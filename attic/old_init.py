import numpy as np
from compass_utils import messages

# def _petrophysics_statistics_on_global_mesh(fractures):
#     for location in ["cell", "fracture"]:
#         # TODO permeability and thermal_conductivity are tensors
#         # for property in ['porosity', 'permeability', 'thermal_conductivity']:
#         for property in ["porosity"]:
#             buffer = np.array(
#                 getattr(kernel, "get_global_%s_%s_buffer" % (location, property))(),
#                 copy=False,
#             )
#             if location == "fracture":
#                 buffer = buffer[fractures]
#             print(buffer.min(), "<=", location, property, "<=", buffer.max())


# FIXME: we need fractures as argument because global_fracture_property_buffer
#        has the size of faces and we need to pick only fracture faces
def _set_property_on_global_mesh(simulation, property, location, value):
    assert location == "cell" or location == "fracture"
    field = getattr(simulation, "%s_%s" % (location, property))
    field[...] = value
    # if location == "fracture":
    #     assert simulation.fractures is not None, "no fractures while setting %s on %s" % (
    #         property,
    #         location,
    #     )
    #     n = simulation.fractures.size()
    #     dim = simulation.mesh.face
    # # tensor only if value is already a tensor
    # # if property in ["permeability", "thermal_conductivity"] and location == "cell":
    # #     value = utils.reshape_as_tensor_array(value, n, dim)
    # # else:
    # if location == "fracture":
    #     buffer[fractures] = value
    # else:
    #     buffer[:] = value


def _set_petrophysics_on_global_mesh(simulation, properties):
    useful_properties = ["porosity", "permeability"]
    # if thermal balance
    useful_properties.append("thermal_conductivity")
    for location in ["cell", "fracture"]:
        for property in useful_properties:
            value = properties[location + "_" + property]()
            if value is not None:
                _set_property_on_global_mesh(simulation, property, location, value)
            else:
                if location == "cell":
                    messages.error("You must define: cell_%s" % property)
                elif simulation.fractures.size() > 0:
                    messages.error("You must define: fracture_%s" % property)
    # print("petrophysics")
    # _petrophysics_statistics_on_global_mesh(simulation)
    # simulation.global_mesh_set_all_rocktypes()
