import numpy as np
from compass_coats.models import Coats
from physics.physical_state import States_with_contexts
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import (
    init_default_rock_physical_properties,
)
from physicalprop.properties import Properties
from compass_coats.init import (
    _set_petrophysics_on_global_mesh,
    _set_property_on_global_mesh,
)
from scheme_lfv import TPFA, VAG
from compass_coats import (
    make_TPFA,
    make_VAG,
    init_transmissivity,
    distribute_over_sites,
    distribute_surface_quantities_over_sites,
)
from icus import ISet, field, intersect, labels
from geom_traits import make_geom


def load_physics(name):
    assert name == "diphasic"

    # load the model
    model = Coats(name)

    # load the default physical properties of the physics
    fluid_properties = init_default_fluid_physical_properties(model.physics)
    rock_properties = init_default_rock_physical_properties(model.physics)
    properties = Properties(fluid_properties, rock_properties)
    return Simulation(model, properties)


class Simulation:
    def __init__(
        self,
        model,
        properties,
    ):
        self.model = model
        self.physics = self.model.physics
        self.properties = properties
        self.dirichlet_faces = ISet()
        self.dirichlet_nodes = ISet()
        self.num_scheme = None
        self.geom_traits = None
        self.fracture = None
        self.gravity = 9.81

    def load_mesh(self, mesh):
        self.geom_traits = make_geom(mesh)

    def init_TPFA(self, *, geom=None, cells=None, dirichlet_faces=None):
        if geom is None:
            geom = self.geom_traits
        if cells is None:
            cells = self.geom_traits.mesh.cells
        if dirichlet_faces is None:
            dirichlet_faces = self.dirichlet_faces
        self.num_scheme = make_TPFA(geom, cells, dirichlet_faces)
        self._init_prop_struct(cells)

    def init_VAG(self, *, geom=None, cells=None):
        if geom is None:
            geom = self.geom_traits
        if cells is None:
            cells = self.geom_traits.mesh.cells
        self.num_scheme = make_VAG(geom, cells)
        self._init_prop_struct(cells)

    def _init_prop_struct(self, cells):
        self.cell_porosity = field(cells, dtype=np.float64)
        self.cell_permeability = field(cells, dtype=np.float64)
        self.cell_thermal_conductivity = field(cells, dtype=np.float64)
        self.cell_heat_source = field(cells, dtype=np.float64)
        # rocktypes and pc at Dirichlet faces ???
        self.rocktypes = labels(self.num_scheme.sites)
        self.rocktypes[...] = 0  # default value
        coats_states = field(self.num_scheme.sites, dtype=self.physics.State)
        coats_contexts = field(self.num_scheme.sites, dtype=self.physics.Context)
        self.coats_solution = States_with_contexts(coats_states, coats_contexts)
        # Neumann source term (molar and energy)
        self.Neumann_source = field(
            self.num_scheme.sites, dtype=self.physics.Accumulation
        )
        self.Neumann_source.as_array().energy[...] = 0.0
        self.Neumann_source.as_array().molar[...] = 0.0

    def init_properties(
        self,
        cell_porosity=lambda: None,
        cell_permeability=lambda: None,
        cell_thermal_conductivity=lambda: None,
        cell_heat_source=None,
        cell_omega_Darcy=lambda: None,
        cell_omega_Fourier=lambda: None,
        fracture_faces=lambda: None,
        fracture_porosity=lambda: None,
        fracture_permeability=lambda: None,
        fracture_thermal_conductivity=lambda: None,
        fracture_omega_Darcy=lambda: None,
        fracture_omega_Fourier=lambda: None,
    ):
        """
        Petrophysical parameters are mandatory depending on the elements that are present (if there are no fractures,
        fracture properties are not mandatory).

        :param cell_permeability: can be a scalar, the diagonal part or the full permeability tensor, or an array
            of any of these three types with as many elements as mesh cells
        :param cell_porosity: can be a scalar or an array with as many elements as mesh cells
        :param cell_thermal_conductivity: can be a scalar, the diagonal part or the full permeability tensor, or an array
            of any of these three types with as many elements as mesh cells
        :param fracture_permeability: can be a scalar or an array with as many elements as fracture cells
            (fracture permeability is isotropic as doing otherwise would require fracture orientation)
        :param fracture_porosity: can be a scalar or an array with as many elements as fracture cells
        :param fracture_thermal_conductivity: can be a scalar

        Some parameters control the numerical scheme:

        :param cell_omega_Darcy: the cell volume proportion that is distributed
            to nodes for the discretisation of the pressure gradient (Darcy law)
        :param cell_omega_Fourier: the fracture volume proportion that is distributed
            to nodes for the discretisation of the pressure gradient (Darcy law)
        :param fracture_omega_Darcy: the cell volume proportion that is distributed
            to nodes for the discretisation of the temperature gradient (Fourier law)
        :param fracture_omega_Fourier: the fracture volume proportion that is distributed
            to nodes for the discretisation of the temperature gradient (Fourier law)

        :param cell_heat_source: volumic thermal source term (will be multiplicated by the cell volume)
            for each cell in W/m^3 = J/m^3/s
        """
        assert self.geom_traits is not None
        assert self.num_scheme is not None
        # here properties will just be used as a namespace
        properties = {}

        def call_if_callable(f):
            if callable(f):
                return f()
            return f

        def make_property_accessor(value):
            return lambda: value

        for location in ["cell", "fracture"]:
            for property in [
                "porosity",
                "permeability",
                "thermal_conductivity",
                "omega_Darcy",
                "omega_Fourier",
            ]:
                name = "%s_%s" % (location, property)
                f = eval(name)
                if callable(f):
                    properties[name] = f
                else:
                    properties[name] = make_property_accessor(f)
        self.fractures = self.geom_traits.mesh.faces.extract(
            call_if_callable(fracture_faces) or []
        )
        _set_petrophysics_on_global_mesh(self, properties)

        # should not be there, nor like that
        # use isinstance ?
        if isinstance(self.num_scheme, TPFA):
            self.Darcy_trans = init_transmissivity(
                self.geom_traits,
                self.num_scheme,
                self.cell_permeability,
            )
            self.Fourier_trans = init_transmissivity(
                self.geom_traits,
                self.num_scheme,
                self.cell_thermal_conductivity,
            )
        elif isinstance(self.num_scheme, VAG):
            self.Darcy_trans = init_transmissivity(
                self.geom_traits,
                self.num_scheme,
                self.cell_permeability,
                self.rocktypes,
                self.dirichlet_nodes,
            )
            self.Fourier_trans = init_transmissivity(
                self.geom_traits,
                self.num_scheme,
                self.cell_thermal_conductivity,
                self.rocktypes,
                self.dirichlet_nodes,
            )
        cell_measures = self.geom_traits.measures(
            self.cell_porosity.support()
        ).as_array()
        poro_vol = field(self.cell_porosity.support(), dtype=np.float64)
        poro_vol.as_array()[...] = self.cell_porosity.as_array() * cell_measures
        poro_1vol = field(self.cell_porosity.support(), dtype=np.float64)
        poro_1vol.as_array()[...] = (
            1.0 - self.cell_porosity.as_array()
        ) * cell_measures
        self.poro_darcy_vol = distribute_over_sites(
            poro_vol,
            self.Darcy_trans,
        )
        self.poro_fourier_vol = distribute_over_sites(
            poro_vol,
            self.Fourier_trans,
        )
        self.rock_fourier_vol = distribute_over_sites(
            poro_1vol,
            self.Fourier_trans,
        )

        if cell_heat_source is not None:
            # init self.cell_heat_source with the given value
            value = call_if_callable(cell_heat_source)
            _set_property_on_global_mesh(self, "heat_source", "cell", value)
            # multiply the value by the volume of the cell
            self.cell_heat_source.as_array()[
                ...
            ] *= self.geom_traits.cell_measures.as_array()
            # distribute over the sites
            self.heat_source = distribute_over_sites(
                self.cell_heat_source,
                self.Fourier_trans,
            )
        else:
            self.heat_source = None

    def set_Neumann_faces(self, faces, Neumann_flux):
        # Neumann_flux is one flux, constant in all faces
        # Neumann flux is surface flux, will be multiplied by faces measures
        # and distributed over concerned sites
        faces_Neumann_flux = field(faces, dtype=self.physics.Accumulation)
        faces_Neumann_flux[...] = Neumann_flux
        self.Neumann_source = distribute_surface_quantities_over_sites(
            self.geom_traits,
            self.num_scheme,
            faces_Neumann_flux,
        )

    def set_states(self, context, state, location=None):
        if location is None:
            self.coats_solution.states[...] = state
            self.coats_solution.contexts[...] = context
        else:
            sites_location = intersect(  # get site indices from location
                location,
                self.num_scheme.sites.location,
                self.num_scheme.sites.location,
            )
            # sites_location = self.num_scheme.sites.from_location(location)
            for i in sites_location.mapping():
                self.coats_solution.states[i] = state
                self.coats_solution.contexts[i] = context
