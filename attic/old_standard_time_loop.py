import copy
from loaf import make_block_linear_system
from compass_coats.assembler import LS_Assembler
import loaf

if loaf.with_petsc:
    from loaf.petsc_solver import PetscSolver, PetscDirectSolver
else:
    from loaf.eigen_solver import EigenSolver
from compass_coats.newton_convergence import Coats_norm
from globalgo import configure_timeloop
from globalgo.core import TimeTick, EventPool
from globalgo.timestep import FixedStep, DynamicStep
from globalgo.config import ConfigHelper
from compass_coats.output_visu import (
    Snapshooter,
    make_sites_dumper,
    OutputDirectoryManager,
)
from compass_coats.output_callbacks import CoatsOutputCallbacks
from icus import join


class Coats_linearized_problem:
    def __init__(self, assembler, *, callbacks=None):
        # TODO: remove copy
        self.__assembler__ = copy.copy(assembler)
        self.__residual_norm__ = copy.copy(assembler.residual_norm)
        self.__events__ = None
        self.__callbacks__ = callbacks or CoatsOutputCallbacks


class Standard_time_loop:
    def __init__(
        self,
        simulation,
        *,
        solver=None,
        direct_solver=False,
        # newton_iteration_callbacks=None,
        Schur_elimination=True,
        fixed_step=None,
        initial_step=1000,
        minimum_step=0.1,
        dumper=None,
        verbosity=3,
    ):
        self.simulation = simulation

        if Schur_elimination:
            Jac, RHS = make_block_linear_system(
                self.simulation.num_scheme.sites,
                self.simulation.physics.nb_dof,
            )
        else:
            Jac, RHS = make_block_linear_system(
                self.simulation.num_scheme.sites,
                self.simulation.physics.nb_natural_variables,
            )

        dirichlet_object = join(
            self.simulation.dirichlet_faces, self.simulation.dirichlet_nodes
        )

        assembler = LS_Assembler(
            self.simulation.physics,
            self.simulation.geom_traits,
            self.simulation.num_scheme,
            self.simulation.properties,
            self.simulation.rocktypes,
            self.simulation.poro_darcy_vol,
            self.simulation.poro_fourier_vol,
            self.simulation.rock_fourier_vol,
            self.simulation.Darcy_trans,
            self.simulation.Fourier_trans,
            dirichlet_object,
            Jac,
            RHS,
            Neumann_source=self.simulation.Neumann_source,
            heat_source=self.simulation.heat_source,
            gravity=self.simulation.gravity,
            Schur_elimination=Schur_elimination,
        )

        assembler.residual_norm = Coats_norm()

        # default solver uses petsc iterative with CPRAMG prec
        # default_solver = EigenSolver()
        assert (
            self.simulation.num_scheme.sites.size(),
            self.simulation.num_scheme.sites.size(),
        ) == Jac.size
        if direct_solver:
            if solver is not None:
                print("PetsC direct solver is ignored, given solver is used")
            else:
                print("PetsC direct solver is used")
            if solver is None:
                if loaf.with_petsc:
                    solver = PetscDirectSolver()
                else:
                    solver = EigenSolver()
        else:
            if solver is None:
                if loaf.with_petsc:
                    solver = PetscSolver()
                else:
                    solver = EigenSolver()
        # outputs of the Newton, linear solver, time loop...
        self.linearized_problem = Coats_linearized_problem(assembler)

        if fixed_step is not None:
            timestepper = FixedStep(fixed_step)
        else:
            timestepper = DynamicStep(
                initial_step,
                minimum_step=minimum_step,
            )

        # helps to write the config dictionnaries
        H = ConfigHelper()
        self.config = {
            "newton": {"tolerance": 1e-5, "maxiter": 8},
            "linear_solver": solver,
            "timesteps": timestepper,
            "events": [],  # factory
            # default is linearized_problem,
            # give argument to the CoatsOutputCallbacks class
            "callbacks": [H.default(verbosity=verbosity)],
        }

        self.loop = configure_timeloop(self.linearized_problem, self.config)

        self.dumper = dumper or make_sites_dumper(
            self.simulation.geom_traits.mesh,
            self.simulation.num_scheme.sites,
            self.simulation.model,
            output_directory=OutputDirectoryManager("output"),  # todo
        )
        self.shooter = Snapshooter(self.dumper)

    def run(
        self,
        solution=None,
        initial_time=0.0,
        final_time=None,
        fixed_step=None,
        initial_step=None,
        nitermax=None,
        output_period=None,
        output_every=None,
        no_output=False,
        events=None,
    ):
        """
        Performs a standard timeloop.

        :param solution: contains context and states
        :param initial_time: Starting time of the simulation. Defaults to 0 if not specified.
        :param final_time: Final time of the simulation (if any).
        :param fixed_step: Set the fixed time step in seconds, can be used only with FixedStep.
                Cannot be specified along with ``initial_step``.
        :param initial_step: Initial time step in seconds, can be used only with DynamicStep.
                (cf. :mod:`ComPASS.timestep_management` module).
                Cannot be specified along with ``fixed_step``.
        :param nitermax: Maximum number of iterations.
        :param output_period: Will dump simulation information every ``output_period`` seconds.
        :param output_every: Will dump simulation information every ``output_every`` iterations.
        :param no_output: Flag that will prevent any output (defaults to False)
        :param events:
        :return: The solution and time tick at the end of the time loop.
        """
        initial_tick = TimeTick(initial_time, 0)
        assert not (final_time is None and nitermax is None)
        final_tick = TimeTick(final_time, nitermax)
        # only one of the fixed_step and initial_step options
        # can be given
        assert initial_step is None or fixed_step is None
        if fixed_step is not None:
            self.loop.timestep_manager = FixedStep(fixed_step)
        elif initial_step is not None:
            # if already DynamicStep, change only the current step
            # (keep other parameters), otherwise create new instance
            if isinstance(self.loop.timestep_manager, DynamicStep):
                self.loop.timestep_manager.current_step = initial_step
            else:
                self.loop.timestep_manager = DynamicStep(initial_step)

        solution = solution or self.simulation.coats_solution
        events = events or EventPool()
        # one or the other output ?
        if output_period is not None and not no_output:
            events.add_recurring_time(
                period=output_period,
                action=self.shooter,
            )
        if output_every is not None and not no_output:
            events.add_recurring_iteration(
                period=output_every,
                action=self.shooter,
            )
        # does not modify solution (self.simulation.coats_solution) !
        return self.loop.run(solution, final_tick, initial_tick, events)
