#include <compass_coats/assembler.h>
#include <compass_coats/closure_laws_with_schur.h>
#include <compass_coats/closure_laws_without_schur.h>
#include <compass_coats/common_type.h>
#include <compass_coats/compass-coats.h>
#include <icus/field-binding.h>
#include <nanobind/nanobind.h>
#include <nanobind_utils/enum.h>
#include <nanobind_utils/numpy-dtype.h>
#include <scheme_lfv/schemes-bindings.h>

namespace nb = nanobind;
namespace nbu = compass::nanobind;
using namespace coats;

NB_MAKE_OPAQUE(BC);

template <typename Mesh_t>
void bind_schemes(nb::module_ &module, auto &mesh_name) {
  auto tpfa_cls_name = std::string("TPFA") + mesh_name;
  auto vag_cls_name = std::string("VAG") + mesh_name;
  numscheme::bindings::bind_tpfa<Mesh_t>(module, tpfa_cls_name.c_str());
  numscheme::bindings::bind_vag_base<Mesh_t>(module, vag_cls_name.c_str());

  if constexpr (Mesh_t::dimension == 3) {
    // keep default name for 3D scheme (in 3D mesh)
    numscheme::bindings::bind_vag<Mesh_t>(module, vag_cls_name.c_str(), "");
    auto vag2D_cls_name = vag_cls_name + std::string("_3_2");
    numscheme::bindings::bind_vag<Mesh_t, 2>(module, vag2D_cls_name.c_str(),
                                             "_codim1");
  }
  if constexpr (Mesh_t::dimension == 2) {
    auto vag2D_cls_name = vag_cls_name + std::string("_2_2");
    numscheme::bindings::bind_vag<Mesh_t>(module, vag_cls_name.c_str(), "");
  }
}

void bind_compass_coats(nb::module_ &module) {
  using namespace icus::binding;

  // bind subtype first
  nbu::bind_enum<BC>(module, "BC",
                     {{BC::dirichlet, "dirichlet"},
                      {BC::neumann, "neumann"},
                      {BC::interior, "interior"}});
  // then bind the enclosing type...
  nb::class_<Site_id> pySite_id{module, "Site_id"};
  pySite_id.def(nb::init<>());
  pySite_id.def(nb::init<const Site_id &>());
  pySite_id.def(nb::init<const BC &, const BC &>());
  pySite_id.def_rw("pressure", &Site_id::pressure);
  pySite_id.def_rw("temperature", &Site_id::temperature);
  // FIXME: add converter C++ type -> numpy string
  // the fact to add a numpy dtype will enable
  // the conversion of the associated field as a numpy recarray
  nbu::add_numpy_dtype(pySite_id,
                       {{"pressure", "int8"}, {"temperature", "int8"}});
  // ... and the associated field
  icus::bind_field(module, pySite_id, "FieldSite_id");
}

void bind_closure_laws(nb::module_ &module) {
  using namespace icus::binding;

  auto cls_clos_laws_no_schur = nb::class_<Closure_laws_without_Schur>(
      module, "Closure_laws_without_Schur",
      "Struct which defines the closure laws and the "
      "contribution to the Jac and RHS with no ",
      "elimination of the secondary variables");
  cls_clos_laws_no_schur.def(nb::init<const Closure_laws_without_Schur &>());
  cls_clos_laws_no_schur.def(
      nb::init<const icus::ISet &, const physics::Rocktype_field &,
               physical_prop::FMP_d &, physical_prop::FMP &,
               physical_prop::RP_d &, physical_prop::RP &>());
  cls_clos_laws_no_schur.def("compute_residuals",
                             &Closure_laws_without_Schur::compute_residuals);
  cls_clos_laws_no_schur.def("get_residuals",
                             &Closure_laws_without_Schur::get_residuals);

  auto cls_clos_laws_with_schur = nb::class_<Closure_laws_with_Schur>(
      module, "Closure_laws",
      "Struct which defines the closure laws and the "
      "elimination of the secondary variables");
  cls_clos_laws_with_schur.def(nb::init<const Closure_laws_with_Schur &>());
  cls_clos_laws_with_schur.def(
      nb::init<const icus::ISet &, const physics::Rocktype_field &,
               physical_prop::FMP_d &, physical_prop::FMP &,
               physical_prop::RP_d &, physical_prop::RP &>());
  cls_clos_laws_with_schur.def("compute_residuals",
                               &Closure_laws_with_Schur::compute_residuals);
  cls_clos_laws_with_schur.def("get_residuals",
                               &Closure_laws_with_Schur::get_residuals);
}

template <typename Coats_assembler, typename Mat_block, typename Nat_block_vect,
          typename Dof_block_vect = Nat_block_vect>
void def_coats_assembler_modules(auto &cls_assembler) {
  cls_assembler.def(nb::init<const Coats_assembler &>());
  cls_assembler.def("residu_reset_history",
                    &Coats_assembler::residu_reset_history);
  cls_assembler.def(
      "balance_equations_residual",
      &Coats_assembler::template balance_equations_residual<Dof_block_vect>);
  cls_assembler.def("discretize_balance_equations",
                    &Coats_assembler::template discretize_balance_equations<
                        Mat_block, Nat_block_vect, Dof_block_vect>);
  cls_assembler.def("update_states",
                    &Coats_assembler::template update_states<Nat_block_vect>);
  cls_assembler.def("get_accumulations", [](Coats_assembler &self) {
    auto &prev_acc = self.previous_acc;
    auto res = loaf::OnePhysicsVector<physics::nb_dof>{prev_acc.support};
    for (auto &&[s, acc] : prev_acc.enumerate()) {
      for (auto &&[i, acc_mol] : std::views::enumerate(acc.molar))
        res(s)(i) = acc_mol;
      res(s)(physics::nb_components) = acc.energy;
    }
    return res;
  });
}

template <typename Scheme>
void bind_assembler(nb::module_ &module, auto &scheme_name,
                    bool without_schur = true) {
  // In the assembler, careful to use the TPFA_Base or VAG_Base schemes
  // no TPFA nor VAG because they contain the builder arrays which are not
  // useful in the flux computation. At this step, only the objects
  // in LFV_scheme are necessary and the copy is much lighter !
  using Driving_force_t =
      std::invoke_result_t<decltype(init_driving_force<Scheme>), Scheme &,
                           Scalar_field &, physical_prop::FMP_d &,
                           typename Scheme::Mesh_t::Vertex>;
  using Therm_diffusion_t = LFVS_flux<Scheme, Phase_temperature_funct_t>;
  using Coats_assembler_no_S = Assembler<Scheme, Closure_laws_without_Schur,
                                         Driving_force_t, Therm_diffusion_t>;
  using Dof_block_vect = loaf::OnePhysicsVector<physics::nb_dof>;
  using Dof_block_mat = loaf::OnePhysicsBlockCOO<physics::nb_dof>;
  using Nat_block_mat = loaf::OnePhysicsBlockCOO<physics::nb_natural_variables>;
  using Nat_block_vect = loaf::OnePhysicsVector<physics::nb_natural_variables>;
  // without schur elim
  if (without_schur) {
    module.def("init_assembler",
               &init_assembler<Scheme, Closure_laws_without_Schur>);
    auto class_name_no_s = scheme_name + std::string("_Assembler_no_Schur");
    auto cls_assembler_no_S = nb::class_<Coats_assembler_no_S>(
        // put name into the scheme description ???
        module, class_name_no_s.c_str(),
        "Structure to assemble the linear system without Schur elimination");
    def_coats_assembler_modules<Coats_assembler_no_S, Nat_block_mat,
                                Nat_block_vect, Dof_block_vect>(
        cls_assembler_no_S);
  }

  // with schur elim
  module.def("init_assembler",
             &init_assembler<Scheme, Closure_laws_with_Schur>);
  using Coats_assembler = Assembler<Scheme, Closure_laws_with_Schur,
                                    Driving_force_t, Therm_diffusion_t>;
  // put name into the scheme description ???
  auto class_name = scheme_name + std::string("_Assembler");
  auto cls_assembler = nb::class_<Coats_assembler>(
      module, class_name.c_str(),
      "Struct to assemble the linear system with Schur elimination");
  def_coats_assembler_modules<Coats_assembler, Dof_block_mat, Dof_block_vect>(
      cls_assembler);
}

NB_MODULE(bindings, module) {
  nb::module_::import_("scheme_lfv"); // to use the binding of Sites

  using Mesh_2s = icmesh::Geomtraits<
      icmesh::Raw_mesh<2, icmesh::structured_connectivity_matrix<2>>>;
  using Mesh_3s = icmesh::Geomtraits<
      icmesh::Raw_mesh<3, icmesh::structured_connectivity_matrix<3>>>;
  using Mesh_3d = icmesh::Geomtraits<
      icmesh::Raw_mesh<3, icmesh::default_connectivity_matrix<3>>>;

  bind_compass_coats(module);
  bind_closure_laws(module);
  bind_schemes<Mesh_3s>(module, "");
  bind_schemes<Mesh_3d>(module, "_default_mesh");
  // In the assembler, careful to use the TPFA_Base or VAG_Base schemes
  // no TPFA nor VAG because they contain the builder arrays which are not
  // useful in the flux computation. At this step, only the objects
  // in LFV_scheme are necessary and the copy is much lighter !
  bind_assembler<numscheme::TPFA_Base<Mesh_3s>>(module, "TPFA");
  bind_assembler<numscheme::VAG_Base<Mesh_3s>>(module, "VAG");
  bind_assembler<numscheme::TPFA_Base<Mesh_3d>>(module, "TPFA_default_mesh");
  bind_assembler<numscheme::VAG_Base<Mesh_3d>>(module, "VAG_default_mesh");
}
