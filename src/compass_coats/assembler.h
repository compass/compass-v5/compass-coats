#pragma once

#include <Eigen/Dense>
#include <coats_variables/coats-variables.h>
#include <compass_coats/closure_laws.h> // remove it
#include <compass_coats/common_type.h>
#include <compass_cxx_utils/array_utils.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <physics/model.h>
#include <pscal/multistate-operators.h>

namespace coats {
namespace cu = compass::utils;
// to allow array operations
using namespace compass::utils::array_operators;
using namespace pscal::multistate::operators; // += in LFVS_driving_force

template <typename LFVS_flux_t, typename LFVS_gravity_t>
// LinearFiniteVolumeScheme_driving_force
// K grad P + rho^alpha g z
struct LFVS_driving_force {
  LFVS_flux_t k_grad_p;
  LFVS_gravity_t rho_alpha_g_z;

  // states is a field carried by sites
  auto operator()(auto flux, const physics::State_field &states,
                  const physics::Context_field &contexts) const {

    auto &&res = k_grad_p(flux, states);
    res += rho_alpha_g_z(flux, states, contexts);
    return res;
  }
};

template <typename Scheme, typename Closure_laws_elim, typename Driving_force,
          typename Therm_diffusion>
struct Assembler {
  const Scheme scheme;
  Closure_laws_elim closure_laws_elim;
  using Cl_block_vect = Closure_laws_elim::Cl_block_vect;
  using Nat_col_block = Closure_laws_elim::Nat_col_block;
  const physics::Scalar_field poro_darcy_vol;
  const physics::Scalar_field poro_fourier_vol;
  const physics::Scalar_field rock_fourier_vol;
  // phase tuple : molar dens * saturation * molar comp
  const Molar_acc_funct_t phase_zeta_i;
  const Therm_acc_funct_t phase_energy;
  const Rock_acc_funct_t rock_energy;
  const Driving_force driving_force; // K grad(P^alpha) (+ gravity term later)
  const Therm_diffusion k_grad_T;    // k grad(T)
  // array<rocktype>(phase tuple) : molar dens * molar comp * kr / visco
  const std::vector<phase_comp_funct_t> phase_mob_i;
  // array<rocktype>(phase tuple) : molar dens * enthalpy * kr / visco
  const std::vector<phase_thermal_funct_t> phase_mob_e;
  const physics::Rocktype_field rocktypes;
  // boundary sites
  icus::ISet dir_boundary_sites;
  const Site_id_field sites_id;
  using Flash =
      physical_prop::Flash<Phase_fugacity_funct_t, Phase_fugacity_coef_funct_t>;
  Flash flash;

  physics::Acc_field previous_acc; // previous time
  // surface molar and thermal source terms:
  physics::Acc_field neumann_sources;
  // volumetric molar and thermal source terms:
  physics::Acc_field volumetric_sources;

  // max increment at each Newton step
  // should be declared in physics !
  physics::State incre_obj;

  using Elem_block =
      loaf::ElementaryBlock<physics::nb_dof, physics::nb_natural_variables>;
  using Col_block = loaf::ElementaryColBlock<physics::nb_dof>;

  Assembler(const Scheme &num_scheme, physical_prop::FMP_d &fluid_properties,
            physical_prop::RP_d &rock_properties, Closure_laws_elim &c_laws,
            const physics::Scalar_field &poro_d_vol,
            const physics::Scalar_field &poro_f_vol,
            const physics::Scalar_field &rock_f_vol,
            const Driving_force &d_force, const Therm_diffusion &therm_diff,
            const physics::Rocktype_field &rkt, const icus::ISet &bound_sites,
            const physics::Acc_field &neum_sources,
            const physics::Acc_field &vol_sources, const Site_id_field &sid)
      : scheme{num_scheme}, closure_laws_elim{c_laws},
        poro_darcy_vol{poro_d_vol}, poro_fourier_vol{poro_f_vol},
        rock_fourier_vol{rock_f_vol},
        phase_zeta_i{init_phase_volumic_acc_functors(fluid_properties)},
        phase_energy{init_phase_thermal_acc_functors(fluid_properties)},
        rock_energy{init_rock_acc_functor(rock_properties)},
        driving_force{d_force}, k_grad_T{therm_diff},
        phase_mob_i{init_phase_comp_mobility_functors(fluid_properties,
                                                      rock_properties)},
        phase_mob_e{init_phase_thermal_mobility_functors(fluid_properties,
                                                         rock_properties)},
        rocktypes{rkt},
        dir_boundary_sites{bound_sites.rebase(scheme.sites.location)},
        neumann_sources{neum_sources}, volumetric_sources{vol_sources},
        sites_id{sid},
        flash{init_phase_fugacity_functors(fluid_properties),
              init_phase_fugacity_coef_functors(fluid_properties)} {
    previous_acc = physics::Acc_field{scheme.sites};

    // should be declared in physics !
    // max increment at each Newton step
    incre_obj.temperature = 20.0;
    for (auto &&alpha : physics::all_phases::array) {
      incre_obj.pressure[alpha] = 5.001e6;
      incre_obj.saturation[alpha] = 0.2;
      for (auto &&comp : physics::all_components::array)
        incre_obj.molar_fractions[alpha][comp] = 1.0;
    }

    // ok only when there is a single physics (no extract to access the jac
    // indices)
    assert(sites_id.support == scheme.sites);
    assert(poro_darcy_vol.support == scheme.sites);
    assert(poro_fourier_vol.support == scheme.sites);
    assert(rock_fourier_vol.support == scheme.sites);
    assert(rocktypes.support == scheme.sites);
    assert(closure_laws_elim.sites == scheme.sites);
    assert(dir_boundary_sites.empty() ||
           dir_boundary_sites.parent() == scheme.sites.location);
    assert(volumetric_sources.support == scheme.sites);
  }

  template <typename LS_RHS>
  auto balance_equations_residual(double dt, LS_RHS &residual,
                                  const physics::State_field &states,
                                  const physics::Context_field &contexts) {
    assert(residual.sites == scheme.sites);
    assert(states.support == scheme.sites);
    assert(contexts.support == scheme.sites);
    residual = 0.0; // necessary ? otherwise remove += from acc

    accumulation_residual(dt, residual, states, contexts);
    add_flux_contributions_residual(residual, states, contexts);

    if (!dir_boundary_sites.empty())
      closure_laws_elim.reset_dirichlet_vect(residual, dir_boundary_sites,
                                             sites_id);

    // add volumetric sources : RHS
    for (auto &&[s, sources] : volumetric_sources.enumerate()) {
      for (auto c_i = 0; c_i < physics::nb_components; ++c_i)
        residual(s)(c_i) -= sources.molar[c_i];
      residual(s)(physics::nb_components) -= sources.energy;
    }

    // add Neumann contributions
    for (auto &&[s, sources] : neumann_sources.enumerate()) {
      for (auto c_i = 0; c_i < physics::nb_components; ++c_i)
        residual(s)(c_i) -= sources.molar[c_i];
      residual(s)(physics::nb_components) -= sources.energy;
    }

    // wells
  }

  template <typename LS_matrix, typename LS_RHS, typename LS_residual>
  auto discretize_balance_equations(
      double dt, LS_matrix &Jac, LS_RHS &RHS,
      const LS_residual &residual, // balance eq residual
      const physics::State_field &states,
      const physics::Context_field &contexts) {
    assert(Jac.row_sites == scheme.sites);
    assert(Jac.col_sites == scheme.sites);
    assert(RHS.sites == scheme.sites);
    assert(residual.sites == scheme.sites);
    assert(states.support == scheme.sites);
    assert(contexts.support == scheme.sites);

    Jac.set_zero();
    RHS = 0.0; // necessary ? otherwise modify init_rhs_with_residual
    // init RHS -= align_matrix * balance_equation_residual
    closure_laws_elim.init_rhs_with_residual(contexts, RHS, residual);

    // if schur elimination, init structures needed to eliminate
    // the secondary unknowns (no contrib to Jac nor RHS)
    // if no schur elimination, add the closure laws into RHS and Jac
    closure_laws_elim.set_up(Jac, RHS, states, contexts);

    // fill Jac and RHS with acc derivatives
    // (RHS is modified by the Schur elimination)
    accumulation_Jac(dt, Jac, RHS, states, contexts);
    add_flux_contributions_Jac(Jac, RHS, states, contexts);

    if (!dir_boundary_sites.empty()) {
      closure_laws_elim.reset_dirichlet_vect(RHS, dir_boundary_sites, sites_id);
      closure_laws_elim.reset_dirichlet_jac(Jac, dir_boundary_sites, sites_id);
    }

    // add_source_term_contributions(Jac, RHS);
    // call source_term(RHS); // careful with elimination and alignment
    // wells
  }

  template <typename LS_RHS>
  void accumulation_residual(double dt, LS_RHS &residual,
                             const physics::State_field &states,
                             const physics::Context_field &contexts) {

    // careful : missing C_tilde !!!
    // where ni is an unknown, not computed with
    // \sum_alpha zeta^\alpha S^\alpha C_i^\alpha ...

    // accumulation: residual
    for (auto &&s : scheme.sites) {
      Col_block acc_res;
      // should not compute the derivatives
      auto &&[mol_acc, energy_acc] =
          site_accumulation(s, states(s), contexts(s));

      // (acc - previous_acc(s)) / dt
      for (auto c_i = 0; c_i < physics::nb_components; ++c_i)
        acc_res(c_i) = (mol_acc.value[c_i] - previous_acc(s).molar[c_i]) / dt;
      acc_res(physics::nb_components) =
          (energy_acc.value - previous_acc(s).energy) / dt;

      residual(s) += acc_res;
    }
  }

  template <typename LS_matrix, typename LS_RHS>
  void accumulation_Jac(double dt, LS_matrix &Jac, LS_RHS &RHS,
                        const physics::State_field &states,
                        const physics::Context_field &contexts) {

    // careful : missing C_tilde !!!
    // where ni is an unknown, not computed with
    // \sum_alpha zeta^\alpha S^\alpha C_i^\alpha ...

    // accumulation: RHS and jac
    for (auto &&s : scheme.sites) {
      // block of size nb_dof (=nb_comp+1) * nb_natural_variable
      Elem_block acc_var;
      auto &&[mol_acc, energy_acc] =
          site_accumulation(s, states(s), contexts(s));

      // molar contributions
      for (auto c_i = 0; c_i < physics::nb_components; ++c_i)
        acc_var.row(c_i) = as_eigen_vector(mol_acc.derivatives[c_i]) / dt;

      // thermal contributions
      acc_var.row(physics::nb_components) =
          as_eigen_vector(energy_acc.derivatives) / dt;

      // secondary unknowns elimination (with or without Schur)
      // without Schur, elimination of P^alpha, remains only Pref
      // now : without Schur : size is nat_variables
      auto &&[prim_acc_var, rhs_prim_acc_var] =
          closure_laws_elim.secondary_elimination(s, contexts(s), acc_var);
      Jac(s, s) += prim_acc_var;
      RHS(s) += rhs_prim_acc_var;
    }
  }

  template <typename LS_RHS>
  void add_flux_contributions_residual(LS_RHS &residual,
                                       const physics::State_field &states,
                                       const physics::Context_field &contexts) {

    // convection and diffusion flux: residual
    for (auto &&[flux, tips] :
         std::views::enumerate(scheme.flux_tips.targets())) {
      auto &&[s0, s1] = tips;
      auto stencil = scheme.stencils.targets_by_source(flux);

      Col_block flux_residual;
      flux_residual.setZero();

      // ---------------------------------------------------
      // convection flux
      for (auto &&alpha :
           physics::union_present_phases(contexts(s0), contexts(s1))) {

        auto p_alpha_flux = driving_force.apply(alpha, flux, states, contexts);
        auto up = p_alpha_flux.value >= 0 ? s0 : s1;
        auto iup = std::find(stencil.begin(), stencil.end(), up);
        assert(iup != stencil.end());
        auto mob_i = phase_mob_i[rocktypes(up)].apply(alpha, states(up));
        auto mob_e = phase_mob_e[rocktypes(up)].apply(alpha, states(up));
        // up to here, should be outside of stencil loop, store phase values !
        // exchange alpha and stencil loop (directly sum the jac contrib)
        // careful with the residual (done only once)

        for (auto &&[c_i, mob_v] : std::views::enumerate(mob_i.value))
          flux_residual(c_i) += mob_v * p_alpha_flux.value;
        flux_residual(physics::nb_components) +=
            mob_e.value * p_alpha_flux.value;
      } // alpha

      // ---------------------------------------------------
      // thermal diffusion : k grad(T)
      auto &&therm_diff = k_grad_T(flux, states);

      flux_residual(physics::nb_components) += therm_diff.value;

      residual(s0) += flux_residual;
      residual(s1) -= flux_residual;
    } // flux
  }

  template <typename LS_matrix, typename LS_RHS>
  void add_flux_contributions_Jac(LS_matrix &Jac, LS_RHS &RHS,
                                  const physics::State_field &states,
                                  const physics::Context_field &contexts) {

    // convection and diffusion flux: jac
    for (auto &&[flux, tips] :
         std::views::enumerate(scheme.flux_tips.targets())) {
      auto &&[s0, s1] = tips;
      auto stencil = scheme.stencils.targets_by_source(flux);

      // ---------------------------------------------------
      // convection flux
      for (auto &&alpha :
           physics::union_present_phases(contexts(s0), contexts(s1))) {

        // auto &&p_alpha_flux = driving_force.apply(alpha, flux, states);
        auto p_alpha_flux = driving_force.apply(alpha, flux, states, contexts);
        auto up = p_alpha_flux.value >= 0 ? s0 : s1;
        auto iup = std::find(stencil.begin(), stencil.end(), up);
        assert(iup != stencil.end());
        auto mob_i = phase_mob_i[rocktypes(up)].apply(alpha, states(up));
        auto mob_e = phase_mob_e[rocktypes(up)].apply(alpha, states(up));
        // up to here, should be outside of stencil loop, store phase values !
        // exchange alpha and stencil loop (directly sum the jac contrib)

        for (auto &&[sk_i, sk] : std::views::enumerate(stencil)) {
          Elem_block var_all_unknowns_sk; // block of size nb_balance_eq x
                                          // nb_natural_variables df_i /dx_j
          // x_j is xcoats(pressure, temperature, molar_fractions, saturation),
          // f_i balance equations
          // var_all_unknowns_sk.setZero();

          // ................................................
          // mob * der(driving_force)
          // molar contributions
          for (auto &&[c_i, mob] : std::views::enumerate(mob_i.value)) {
            var_all_unknowns_sk.row(c_i) = // +=
                mob * as_eigen_vector(p_alpha_flux.derivatives[sk_i]);
          }
          // thermal contributions
          var_all_unknowns_sk.row(physics::nb_components) = // +=
              mob_e.value * as_eigen_vector(p_alpha_flux.derivatives[sk_i]);

          // ................................................
          // der(mob) * driving_force
          if (*iup == sk) {
            // molar contributions
            for (auto c_i = 0; c_i < physics::nb_components; ++c_i)
              var_all_unknowns_sk.row(c_i) +=
                  p_alpha_flux.value * as_eigen_vector(mob_i[c_i].derivatives);

            // thermal contributions
            var_all_unknowns_sk.row(physics::nb_components) +=
                p_alpha_flux.value * as_eigen_vector(mob_e.derivatives);
          }
          // secondary unknowns elimination (With or without Schur)
          // without Schur, elimination of P^alpha, remains only Pref
          // now : without Schur : size is nat_variables
          auto &&[prim_flux_var, rhs_prim_flux_var] =
              closure_laws_elim.secondary_elimination(sk, contexts(sk),
                                                      var_all_unknowns_sk);
          Jac(s0, sk) += prim_flux_var;
          Jac(s1, sk) -= prim_flux_var;
          RHS(s0) += rhs_prim_flux_var;
          RHS(s1) -= rhs_prim_flux_var;
        } // stencil
      } // alpha

      // ---------------------------------------------------
      // thermal diffusion : k grad(T)
      auto &&therm_diff = k_grad_T(flux, states);

      for (auto &&[sk, therm_var] : zip(stencil, therm_diff.derivatives)) {
        Elem_block var_all_unknowns_sk;
        var_all_unknowns_sk.topRows(physics::nb_components).setZero();
        var_all_unknowns_sk.row(physics::nb_components) =
            as_eigen_vector(therm_var);

        // secondary unknowns elimination (With or without Schur)
        // without Schur, elimination of P^alpha, remains only Pref
        // now : without Schur : size is nat_variables
        auto &&[prim_flux_var, rhs_prim_flux_var] =
            closure_laws_elim.secondary_elimination(sk, contexts(sk),
                                                    var_all_unknowns_sk);
        Jac(s0, sk) += prim_flux_var;
        Jac(s1, sk) -= prim_flux_var;
        RHS(s0) += rhs_prim_flux_var;
        RHS(s1) -= rhs_prim_flux_var;
      }

    } // flux
  }

  auto site_accumulation(icus::index_type s, const physics::State &state,
                         const physics::Context &context) {
    // careful : missing C_tilde !!!
    // where ni is an unknown, not computed with
    // \sum_alpha zeta^\alpha S^\alpha C_i^\alpha ...

    // molar accumulation
    auto &&sum_zeta_i = sum_on_present_phases(context, phase_zeta_i, state);
    auto &&molar_acc = sum_zeta_i * poro_darcy_vol(s);
    // energy accumulation
    // rock cp * T * (1-poro) + (sum_phases zeta_i) * poro
    auto fluid_energy_acc =
        rock_energy(state) * rock_fourier_vol(s) +
        sum_on_present_phases(context, phase_energy, state) *
            poro_fourier_vol(s);

    return std::make_tuple(molar_acc, fluid_energy_acc);
  }

  void residu_reset_history(const physics::State_field &states,
                            const physics::Context_field &contexts) {
    for (auto &&s : scheme.sites) {
      // should be without derivatives
      auto &&[molar_acc, energy_acc] =
          site_accumulation(s, states(s), contexts(s));
      // molar accumulation
      previous_acc(s).molar = molar_acc.value;
      // thermal accumulation
      previous_acc(s).energy = energy_acc.value;
    }
  }

  template <typename LS_RHS>
  inline void update_states(physics::State_field &states,
                            physics::Context_field &contexts,
                            const LS_RHS &dx) {
    assert(contexts.support == states.support);
    assert(dx.sites == states.support);

    // set to 0 the Dirichlet solution, must be done before Schur reconstruct
    // we should before check it is already the case (< eps) ?
    if (!dir_boundary_sites.empty())
      closure_laws_elim.reset_dirichlet_vect(dx, dir_boundary_sites, sites_id);

    // 1: reconstruct all natural variables solution using Schur
    auto full_dx = closure_laws_elim.reconstruct(contexts, dx);
    // 2: compute max Newton increment
    auto relax = Newton_compute_relaxation(full_dx);
    // 3: update the state
    for (auto &&[s, state] : std::views::enumerate(states))
      as_eigen_vector(state) += full_dx(s).cwiseProduct(relax);
    // 4: flash
    flash.update(states, contexts);
  }

  // relax = min(1, incre_obj/incre_max)
  // where incre_obj is set by the user
  // and incre_max is the maximum of the Newton increment
  // in current iteration
  template <typename All_dx>
  inline Nat_col_block Newton_compute_relaxation(const All_dx &all_dx) {
    Nat_col_block incre_max;
    incre_max.setZero();
    double eps = 1e-10;

    for (auto &&s : all_dx.sites)
      incre_max = incre_max.cwiseMax(all_dx(s));
    incre_max = incre_max.cwiseMax(eps);
    auto &&frac_array = as_eigen_vector(incre_obj).array() / incre_max.array();

    return frac_array.matrix().cwiseMin(1.0);
  }
};

} // namespace coats
