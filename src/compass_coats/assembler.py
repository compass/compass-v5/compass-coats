import numpy as np
from .bindings import (
    Closure_laws_without_Schur,
    Closure_laws,
    init_assembler,
    BC,
    Site_id,
)
from loaf import make_block_linear_system, make_OnePhysicsVector
from icus.fig import Data
from compass_coats.newton_convergence import Coats_norm

# Build global.interfaces.AssemblerInterface
class Coats_assembler:
    """assembler of the Coats with energy problem"""

    def __init__(
        self,
        mesh,
        model,
        scheme,
        coats_linearized_pb,
    ):
        if scheme.frac_faces and scheme.cells:
            # non empty cells zone
            assert False, "frac in a matrix not implemented yet"
        if scheme.cells:
            poro_loc = "matrix"
            measures = Data(mesh.cell_measures)[scheme.cells]
            poro = coats_linearized_pb.sites_data.matrix.petrophysics.porosity
        else:
            poro_loc = "fractures"
            measures = Data(mesh.face_measures)[scheme.frac_faces]
            poro = coats_linearized_pb.sites_data.fractures.petrophysics.porosity
        poro_vol = poro * measures
        poro_1vol = (1.0 - poro) * measures

        self.poro_darcy_vol = scheme.Darcy_distribute(
            poro_loc,
            # Data carried by mesh objects
            poro_vol,
        )
        self.poro_fourier_vol = scheme.Fourier_distribute(
            poro_loc,
            # Data carried by mesh objects
            poro_vol,
        )
        self.rock_fourier_vol = scheme.Fourier_distribute(
            poro_loc,
            # Data carried by mesh objects
            poro_1vol,
        )

        if scheme.schur_elimination:
            self.closure_laws = Closure_laws(
                scheme.sites.iset,
                coats_linearized_pb.sites_data.rocktypes.field,
                model.properties.fluid.cpp_prop_with_derivatives,
                model.properties.fluid.cpp_prop_without_derivatives,
                model.properties.rock.cpp_prop_with_derivatives,
                model.properties.rock.cpp_prop_without_derivatives,
            )
        else:
            self.closure_laws = Closure_laws_without_Schur(
                scheme.sites.iset,
                coats_linearized_pb.sites_data.rocktypes.field,
                model.properties.fluid.cpp_prop_with_derivatives,
                model.properties.fluid.cpp_prop_without_derivatives,
                model.properties.rock.cpp_prop_with_derivatives,
                model.properties.rock.cpp_prop_without_derivatives,
            )

        # initialize the sites id (interior or Dirichlet)
        # FIXME: pressure stands for "molar balance equationS"
        # FIXME: temperature stands for "energy balance equation"
        self.sites_id = Data.from_zone(scheme.sites, dtype=Site_id)
        # init the field with interior value
        int_id = Site_id(BC(BC.interior), BC(BC.interior))
        self.sites_id[...] = int_id
        # modify the Dirichlet sites
        Dir_id = Site_id(BC(BC.dirichlet), BC(BC.dirichlet))
        self.sites_id[scheme.dirichlet_sites] = Dir_id

        # call a cpp function which returns an Assembler structure
        # initialized driving_force and LFVS_flux before assembler
        self._instance = init_assembler(
            scheme.instance,
            model.properties.fluid.cpp_prop_with_derivatives,
            model.properties.rock.cpp_prop_with_derivatives,
            self.closure_laws,
            coats_linearized_pb.sites_data.rocktypes.field,
            self.poro_darcy_vol.field,
            self.poro_fourier_vol.field,
            self.rock_fourier_vol.field,
            scheme.Darcy_trans,
            scheme.Fourier_trans,
            coats_linearized_pb.mesh.to_vertex(model.gravity),
            # dirichlet must be in sites.location tree
            # FIXME: move it into sites tree
            scheme.dirichlet_mesh_zone.iset,
            coats_linearized_pb.sites_data.boundary_conditions.Neumann_flux.field,
            # sources, defined over sites, already multiplied by the volume
            coats_linearized_pb.sites_data.sources.field,
            self.sites_id.field,
        )
        # create linear system objects with or without schur elimination
        # of the secondary unknowns
        if scheme.schur_elimination:
            self.Jac, self.RHS = make_block_linear_system(
                scheme.sites.iset,  # iset, no Zone
                model.physics.nb_dof,
            )
            # when schur elimination, RHS and residual have same block size:
            # number of balance equations
            self.residual = type(self.RHS)(self.RHS.sites)
        else:
            self.Jac, self.RHS = make_block_linear_system(
                scheme.sites.iset,  # iset, no Zone
                model.physics.nb_natural_variables,
            )
            # when no schur elimination, RHS block size contain all the dof
            # and residual block size contains the number of balance eq
            self.residual = make_OnePhysicsVector(self.RHS.sites, model.physics.nb_dof)
        # assert(are_same_indexed_set(self.Jac.sites, self.RHS.sites))
        self.residual_norm = Coats_norm()

    def ls_description(self):
        """Used yet, was to initialize the linear solver system
        if necessary
        """
        return None

    def set_up(self, solution, dt, time):
        """Initialize the accumulation and store the fixed values of the
        Newton loop
        """
        self._instance.residu_reset_history(solution.states, solution.contexts)
        self.dt = dt

    def evaluate(self, solution):
        """Evaluate residual and jacobian of the Coats energy problem"""
        # compute Jac and RHS
        # RHS is different from residual due to Schur elimination
        self._instance.balance_equations_residual(
            self.dt,
            self.residual,
            solution.states,
            solution.contexts,
        )

        self._instance.discretize_balance_equations(
            self.dt,
            self.Jac,
            self.RHS,
            self.residual,
            solution.states,
            solution.contexts,
        )
        # to save the jac into a file
        # self.Jac.coo.sort_and_add_entries()
        # self.Jac.dump("jacobienne.txt")
        return self.RHS, self.Jac

    def update_solution(self, solution, dx):
        """Update the solution (flash included)"""
        self._instance.update_states(solution.states, solution.contexts, dx)
        return solution

    def restore_solution(self, solution, saved_solution):
        solution.states[...] = saved_solution.states
        solution.contexts[...] = saved_solution.contexts
        return solution
