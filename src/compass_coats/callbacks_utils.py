class LogTimeStepFailure:
    """LogTimeStepFailure(log, residual_transformer)

    log: function
        called when timestepper.try_step() fails
        arguments: a dict
            { 'tick': tick,
              'dt': dt,
              'trace': list of results from residual_transformer(),
            }
    residual_transformer: function
        called after residual_norm.norm(solution, residual, increment)
        arguments: solution, residual, increment, norm

    """

    def __init__(self, log, residual_transformer):
        self.log = log
        self.residual_transformer = residual_transformer
        self.step_solver_trace = []

    def timestepper_try_step(self, iteration, solution, dt, tick):
        self.step_solver_trace = []
        solution, status = yield
        if status.failure:
            self.log(dict(tick=tick, dt=dt, trace=self.step_solver_trace))
        self.step_solver_trace = []

    def residual_norm_norm(self, solution, residual, increment):
        norm = yield
        self.step_solver_trace.append(
            self.residual_transformer(solution, residual, increment, norm)
        )
