#pragma once

#include "icus/Field.h"
#include "icus/ISet.h"
#include "loaf/block_matrix.h"
#include "loaf/vector.h"
#include <Eigen/Dense>
#include <coats_variables/coats-variables.h>
#include <loaf/elementary_block.h>

namespace coats {

inline auto as_eigen_vector(const auto &the_struct) {
  return Eigen::VectorXd::Map(pscal::as_array(the_struct),
                              physics::nb_natural_variables);
}
inline auto as_eigen_vector(auto &the_struct) {
  return Eigen::VectorXd::Map(pscal::as_array(the_struct),
                              physics::nb_natural_variables);
}

struct Closure_laws {
  using Contexts_info = std::array<physics::Context_info, physics::nb_contexts>;
  using Cl_block_vect = loaf::OnePhysicsVector<physics::nb_all_clos_laws>;
  using Cl_col_block = Cl_block_vect::block_type;
  using Cl_block = loaf::ElementaryBlock<physics::nb_all_clos_laws,
                                         physics::nb_natural_variables>;

  icus::ISet sites;
  const physics::Rocktype_field rocktypes;
  const Contexts_info contexts_info;
  const Phase_pressure_funct_t phase_pressure; // not necessary
  const Phase_molar_frac_funct_t phase_mol_frac;
  const Phase_sat_funct_t phase_sat;
  const Phase_sat_funct_no_d_t phase_sat_no_d;
  const Phase_fugacity_funct_t phase_fug;
  const Phase_fugacity_funct_no_d_t phase_fug_no_d;
  const std::vector<phase_pref2palpha_funct_t> phase_pref2palpha;

  // store closure laws residuals for each site
  Cl_block_vect cl_residuals;

  Closure_laws(const icus::ISet &the_sites, const physics::Rocktype_field &rkt,
               physical_prop::FMP_d &fluid_properties,
               physical_prop::FMP &fluid_properties_no_d,
               physical_prop::RP_d &rock_properties,
               physical_prop::RP &rock_properties_no_d)
      : sites{the_sites}, rocktypes{rkt},
        contexts_info{physics::init_contexts_info()},
        phase_pressure{init_phase_pressure_functors()},
        phase_mol_frac{init_phase_molar_fractions_functors()},
        phase_sat{init_phase_saturation_functors()},
        phase_sat_no_d{init_phase_saturation_functors_no_d()},
        phase_fug{init_phase_fugacity_functors(fluid_properties)},
        phase_fug_no_d{init_phase_fugacity_functors(fluid_properties_no_d)},
        phase_pref2palpha{init_phase_pref2palpha_functors(rock_properties)},
        cl_residuals{sites} {
    static_assert(physics::nb_phases < 3 &&
                  "probably wrong _nb_max_clos_laws formula");
  }

  Cl_col_block site_closure_laws_residual(const physics::State &state,
                                          const physics::Context &context,
                                          int rocktype) {
    Cl_col_block residual;

    auto cl_i = 0; // closure laws line index

    // (sum_i c_i^alpha) - 1
    for (auto &&alpha : physics::present_phases(context)) {

      auto &&alpha_mol_frac = state.molar_fractions[alpha];
      residual(cl_i) = sum(alpha_mol_frac) - 1.0;
      ++cl_i;
    }

    // (sum_alpha sat^alpha) - 1
    auto &&sum_sat =
        physics::sum_on_present_phases(context, phase_sat_no_d, state);
    residual(cl_i) = sum_sat - 1.0;
    ++cl_i;

    // thermodynamic equilibrium between phases if any (fugacities equality)
    auto &&ctx_info = contexts_info[physics::context_index(context)];
    for (int eq = 0; eq < ctx_info.nb_equilibrium; ++eq) {
      auto &&[alpha, beta] = ctx_info.phases[eq];
      // fugacity contains all component values
      auto alpha_fugacity = phase_fug_no_d.apply(alpha, state);
      auto beta_fugacity = phase_fug_no_d.apply(beta, state);
      auto comp = physics::component_index(ctx_info.component[eq]);
      residual(cl_i) = alpha_fugacity[comp] - beta_fugacity[comp];
      ++cl_i;
    }
    assert(cl_i == ctx_info.nb_secondary_unknowns);

    // --------------------------------------------------
    // temporary : missing lines
    // Palpha = Pg + Pref2Palpha for alpha not gas
    // Palpha - Pg - Pref2Palpha
    for (auto &&alpha : physics::all_phases::array) {
      if (alpha == physics::Phase::gas)
        continue;
      auto pres = state.pressure[alpha];
      auto pres_g = state.pressure[physics::Phase::gas];
      auto &&pref2palpha = phase_pref2palpha[rocktype].apply(alpha, state);
      residual(cl_i) = pres - pres_g - pref2palpha.value;
      ++cl_i;
    }
    // non meaning variables (C and S of absent phases)
    for (auto &&alpha : physics::absent_phases(context)) {

      // C_i^alpha
      for (size_t comp = 0; comp < physics::nb_components; ++comp) {
        residual(cl_i) = 0.0;
        ++cl_i;
      }

      // s^alpha
      residual(cl_i) = 0.0;
      ++cl_i;
    }

    assert(cl_i == physics::nb_all_clos_laws &&
           "wrong number of additional closure laws");

    return residual;
  }

  Cl_block site_closure_laws_jac(const physics::State &state,
                                 const physics::Context &context,
                                 int rocktype) {

    Cl_block var_all_unknowns;

    auto cl_i = 0; // closure laws line index

    // (sum_i c_i^alpha) - 1
    for (auto &&alpha : physics::present_phases(context)) {

      auto &&alpha_mol_frac = phase_mol_frac.apply(alpha, state);
      var_all_unknowns.row(cl_i).setZero();
      for (size_t comp = 0; comp < physics::nb_components; ++comp) {
        var_all_unknowns.row(cl_i) +=
            as_eigen_vector(alpha_mol_frac.derivatives[comp]);
      }
      ++cl_i;
    }

    // (sum_alpha sat^alpha) - 1
    auto &&sum_sat = physics::sum_on_present_phases(context, phase_sat, state);
    var_all_unknowns.row(cl_i) = as_eigen_vector(sum_sat.derivatives);
    ++cl_i;

    // thermodynamic equilibrium between phases if any (fugacities equality)
    auto &&ctx_info = contexts_info[physics::context_index(context)];
    for (int eq = 0; eq < ctx_info.nb_equilibrium; ++eq) {

      auto &&[alpha, beta] = ctx_info.phases[eq];
      // fugacity contains all component values
      auto alpha_fugacity = phase_fug.apply(alpha, state);
      auto beta_fugacity = phase_fug.apply(beta, state);
      auto comp = physics::component_index(ctx_info.component[eq]);
      var_all_unknowns.row(cl_i) =
          as_eigen_vector(alpha_fugacity.derivatives[comp]) -
          as_eigen_vector(beta_fugacity.derivatives[comp]);
      ++cl_i;
    }
    assert(cl_i == ctx_info.nb_secondary_unknowns);

    // --------------------------------------------------
    // temporary : missing lines
    // Palpha = Pg + Pref2Palpha for alpha not gas
    // Palpha - Pg - Pref2Palpha
    for (auto &&alpha : physics::all_phases::array) {
      if (alpha == physics::Phase::gas)
        continue;
      auto &&pres = phase_pressure.apply(alpha, state);
      auto &&pres_g = phase_pressure.apply(physics::Phase::gas, state);
      auto &&pref2palpha = phase_pref2palpha[rocktype].apply(alpha, state);
      var_all_unknowns.row(cl_i) = as_eigen_vector(pres.derivatives) -
                                   as_eigen_vector(pres_g.derivatives) -
                                   as_eigen_vector(pref2palpha.derivatives);
      ++cl_i;
    }
    // non meaning variables (C and S of absent phases)
    for (auto &&alpha : physics::absent_phases(context)) {

      // C_i^alpha
      auto &&alpha_mol_frac = phase_mol_frac.apply(alpha, state);
      for (size_t comp = 0; comp < physics::nb_components; ++comp) {
        var_all_unknowns.row(cl_i) =
            as_eigen_vector(alpha_mol_frac.derivatives[comp]);
        ++cl_i;
      }

      // s^alpha
      auto &&alpha_sat = phase_sat.apply(alpha, state);
      var_all_unknowns.row(cl_i) = as_eigen_vector(alpha_sat.derivatives);
      ++cl_i;
    }

    assert(cl_i == physics::nb_all_clos_laws &&
           "wrong number of additional closure laws");

    return var_all_unknowns;
  }

  Cl_block_vect &compute_residuals(const physics::State_field &states,
                                   const physics::Context_field &contexts) {
    for (auto &&s : sites) {
      cl_residuals(s) =
          site_closure_laws_residual(states(s), contexts(s), rocktypes(s));
    }
    return cl_residuals;
  }
};
} // namespace coats
