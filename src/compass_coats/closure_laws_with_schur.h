#pragma once
#include <Eigen/Dense>
#include <compass_coats/closure_laws.h>
#include <compass_coats/common_type.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <loaf/block_matrix.h>
#include <loaf/elementary_block.h>
#include <loaf/linear_block_elimination.h>
#include <loaf/vector.h>

#include <chrono>
#include <iostream>

namespace coats {
namespace cu = compass::utils;

struct Closure_laws_with_Schur {
  std::chrono::duration<double> _tspent;
  static inline auto _align(physics::Context context) {
    return Eigen::Map<const Eigen::MatrixXi>(physics::Align::mat[context],
                                             physics::nb_dof, physics::nb_dof)
        .transpose()
        .cast<double>();
  }

  template <std::size_t m, std::size_t n>
  struct Wrapped_eliminator : public loaf::LinearBlockEliminator<m, n> {
    static_assert(m == physics::nb_dof);
    using base = loaf::LinearBlockEliminator<m, n>;
    base::template block_t<m, m>
    mat_incr(physics::Context context,
             const typename base::template block_t<m, n> &J) {
      auto &&permut_J =
          J(Eigen::all, physics::Prim_sec_indices::unk_indices[context]);
      return base::mat_incr(permut_J);
    }
    base::template block_t<m, 1>
    rhs_incr(physics::Context context,
             const typename base::template block_t<m, n> &J) {
      auto &&permut_J =
          J(Eigen::all, physics::Prim_sec_indices::unk_indices[context]);
      return base::rhs_incr(permut_J);
    }
    base::template block_t<n, 1>
    reconstruct(physics::Context context,
                const typename base::template block_t<m, 1> &U) {
      auto &&dx = base::reconstruct(U);
      return dx(physics::Prim_sec_indices::var_indices[context], Eigen::all);
    }
  };
  using eliminator_t =
      Wrapped_eliminator<physics::nb_dof, physics::nb_natural_variables>;

  using P2s_eliminators = std::vector<eliminator_t>;
  // Schur elimination, store mat_factor
  // [            Id_dof           ]
  // [ --------------------------- ]    shape : (nb_natural_variables, nb_dof)
  // [ - J_{K, K}^-1 * J_{K, notK} ]
  //
  // and rhs_factor (inside P2s_eliminators)
  // [        0_dof        ]
  // [ ------------------- ]    shape : (nb_natural_variables, 1)
  // [ - J_{K, K}^-1 * b_k ]

  using Nat_block_vect = loaf::OnePhysicsVector<physics::nb_natural_variables>;
  using Nat_col_block = Nat_block_vect::block_type;
  using Cl_block_vect = Closure_laws::Cl_block_vect;
  using Cl_col_block = Cl_block_vect::block_type;
  // used to fill the closure laws before Schur Mat and vect objects
  // physics::nb_all_clos_laws * physics::nb_natural_variables
  using Cl_block = Closure_laws::Cl_block;
  using Dof_block = loaf::ElementaryBlock<physics::nb_dof, physics::nb_dof>;
  using Dof_col_block = loaf::ElementaryColBlock<physics::nb_dof>;

  icus::ISet sites;
  const physics::Rocktype_field rocktypes;

  Closure_laws closure_laws;

  // stores Schur Mat and vect for each site
  P2s_eliminators prim2sec;
  // stores the alignment matrix for each context
  physics::Context_vector<Dof_block> align;

  Closure_laws_with_Schur(const icus::ISet &the_sites,
                          const physics::Rocktype_field &rkt,
                          physical_prop::FMP_d &fluid_properties,
                          physical_prop::FMP &fluid_properties_no_d,
                          physical_prop::RP_d &rock_properties,
                          physical_prop::RP &rock_properties_no_d)
      : sites{the_sites}, rocktypes{rkt},
        closure_laws{the_sites,        rkt,
                     fluid_properties, fluid_properties_no_d,
                     rock_properties,  rock_properties_no_d} {
    assert(rocktypes.support == sites);
    prim2sec.resize(sites.size());
    for (auto &&ctx : physics::all_contexts::array)
      align[ctx] = _align(ctx);
  }

  template <typename LS_matrix>
  inline LS_matrix permute_variables(const physics::Context &context,
                                     const LS_matrix &mat) {
    assert(mat.cols() == physics::nb_natural_variables);
    return mat(Eigen::all, physics::Prim_sec_indices::unk_indices[context]);
  }

  // with Schur elimination, no contribution is added to jac and RHS
  template <typename LS_matrix, typename LS_RHS>
  void set_up(const LS_matrix &Jac, const LS_RHS &RHS,
              const physics::State_field &states,
              const physics::Context_field &contexts) {
    for (auto &&[s, p2s] : std::views::enumerate(prim2sec)) {
      p2s.mat_factor.template topRows<physics::nb_dof>().setIdentity();
      p2s.rhs_factor.template topRows<physics::nb_dof>().setZero();
      auto &&var_closure_laws = closure_laws.site_closure_laws_jac(
          states(s), contexts(s), rocktypes(s));
      closure_laws.cl_residuals(s) = closure_laws.site_closure_laws_residual(
          states(s), contexts(s), rocktypes(s));
      // physics::split_prim_sec_variables_laws(contexts(s), var_closure_laws);
      // store - J_{K, K}^-1 * J_{K, notK} and - J_{K, K}^-1 * b_k
      // compute J_{K, K}^-1
      auto &&prim_sec_cl = permute_variables(contexts(s), var_closure_laws);
      auto &&sec_cl_inv =
          prim_sec_cl.template rightCols<physics::nb_all_clos_laws>().inverse();
      // - J_{K, K}^-1 * J_{K, notK}
      p2s.mat_factor.template bottomRows<physics::nb_all_clos_laws>() =
          -sec_cl_inv * prim_sec_cl.template leftCols<physics::nb_dof>();
      // b_k = -residual_k
      // - J_{K, K}^-1 * b_k
      // J_{K, K}^-1 * residual_k
      // premutations over columns have no impact on b_k
      p2s.rhs_factor.template bottomRows<physics::nb_all_clos_laws>() =
          sec_cl_inv * closure_laws.cl_residuals(s);
    }
  }

  template <typename LS_matrix>
  std::tuple<Dof_block, Dof_col_block>
  // remove context
  secondary_elimination(icus::index_type s, physics::Context context,
                        const LS_matrix &balance_Jac) {

    return std::make_tuple(
        align[context] * prim2sec[s].mat_incr(context, balance_Jac),
        align[context] * prim2sec[s].rhs_incr(context, balance_Jac));
  }

  template <typename LS_RHS>
  // remove context
  Nat_block_vect reconstruct(const physics::Context_field &contexts,
                             const LS_RHS &solutions) {
    auto &&full_dx = Nat_block_vect{sites};
    for (auto &&s : sites)
      full_dx(s) = prim2sec[s].reconstruct(contexts(s), solutions(s));
    return full_dx;
  }

  Cl_block_vect &compute_residuals(const physics::State_field &states,
                                   const physics::Context_field &contexts) {
    return closure_laws.compute_residuals(states, contexts);
  }

  Cl_block_vect &get_residuals() { return closure_laws.cl_residuals; }

  // modification of the Dirichlet sites is done AFTER the Schur complement
  // then the vect contains only nb_components (+ 1) lines
  // wrong due to alignment
  // put T as the last unknown ?
  template <typename LS_RHS>
  inline void reset_dirichlet_vect(LS_RHS &vect,
                                   const icus::ISet &dir_bound_sites,
                                   const Site_id_field &sites_id) {
    auto tin = std::chrono::high_resolution_clock::now();
    // Dirichlet BC fill vect (0.0)
    for (auto &&sb : dir_bound_sites.mapping()) { // in scheme.sites.location
      if (sites_id(sb).temperature == BC::dirichlet) {
        assert(vect(sb).rows() == physics::nb_components + 1);
        vect(sb).row(physics::nb_components).setZero();
        if (sites_id(sb).pressure == BC::dirichlet)
          vect(sb).topRows(physics::nb_components).setZero();
      } else {
        assert(sites_id(sb).pressure != BC::dirichlet &&
               "Dirichlet condition on P only are not handled!");
      }
    }
    auto tout = std::chrono::high_resolution_clock::now();
    _tspent +=
        std::chrono::duration_cast<std::chrono::duration<double>>(tout - tin);
    // std::cerr << "time spent in Closure_laws_with_Schur: " << _tspent.count()
    //           << std::endl;
  }

  template <typename LS_matrix>
  // modification of the Dirichlet sites is done AFTER the Schur complement
  // then the matrix Jac is square
  // wrong due to alignment
  // put T as the last unknown ?
  // FIXME: this code seems to be a duplicate between
  // closure_laws_(with/without)_schur.h
  inline void reset_dirichlet_jac(LS_matrix &Jac,
                                  const icus::ISet &dir_bound_sites,
                                  const Site_id_field &sites_id) {

    // Dirichlet BC, modify Jac (identity)
    std::vector<icus::index_type> drows_pT; // Dir in p and T
    // Dir in T only (Dir p only has no meaning)
    std::vector<icus::index_type> drows_T;
    drows_pT.reserve(dir_bound_sites.size());     // possibly too much
    drows_T.reserve(dir_bound_sites.size());      // possibly too much
    for (auto &&sb : dir_bound_sites.mapping()) { // in scheme.sites.location
      if (sites_id(sb).temperature == BC::dirichlet) {
        if (sites_id(sb).pressure == BC::dirichlet) {
          drows_pT.push_back(sb);
        } else { // only T is Dirichlet
          drows_T.push_back(sb);
          throw std::runtime_error("only T dirichlet not tested yet.");
          // loop over all non nul blocks in line sb
          // sb_blocks.row(physics::nb_components).setZero();
          // if(i == j) { // block on the diagonal (sb, sb)
          //   sb_blocks(physics::nb_components, physics::nb_components) = 1.0;
          // }
        }
      } else {
        assert(sites_id(sb).pressure != BC::dirichlet &&
               "Dirichlet condition on P only are not handled!");
      }
    }
    Jac.set_rows_to_identity(drows_pT);
    Jac.template set_rows_to_identity<physics::nb_components>(drows_T);
  }

  template <typename LS_RHS, typename LS_residual>
  void init_rhs_with_residual(const physics::Context_field &contexts,
                              LS_RHS &RHS, const LS_residual &residual) {
    for (auto &&s : sites)
      // aligned balance eq residual
      RHS(s) -= align[contexts(s)] * residual(s);
  }
};

} // namespace coats
