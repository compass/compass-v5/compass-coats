#pragma once
#include "icus/Field.h"
#include "icus/ISet.h"
#include "loaf/block_matrix.h"
#include "loaf/vector.h"
#include <Eigen/Dense>
#include <coats_variables/coats-variables.h>
#include <compass_coats/closure_laws.h>
#include <compass_coats/common_type.h>
#include <loaf/elementary_block.h>

#include <chrono>
#include <iostream>

namespace coats {
namespace cu = compass::utils;
using Contexts_info = std::array<physics::Context_info, physics::nb_contexts>;

struct Closure_laws_without_Schur {
  std::chrono::duration<double> _tspent;
  // the following value can be wrong if more than two phases
  // and can be wrong depending on MCP
  // static_assert(physics::nb_phases < 3 && "wrong _nb_max_clos_laws formula");
  // static const std::size_t _nb_max_clos_laws =
  //     physics::nb_phases + 1 + physics::nb_components;
  using Cl_block_vect = Closure_laws::Cl_block_vect;
  using Cl_block = Closure_laws::Cl_block;
  using Nat_block = loaf::ElementaryBlock<physics::nb_natural_variables,
                                          physics::nb_natural_variables>;
  using Nat_col_block = loaf::ElementaryColBlock<physics::nb_natural_variables>;

  icus::ISet sites;
  const physics::Rocktype_field rocktypes;
  Closure_laws closure_laws;

  Closure_laws_without_Schur(const icus::ISet &the_sites,
                             const physics::Rocktype_field &rkt,
                             physical_prop::FMP_d &fluid_properties,
                             physical_prop::FMP &fluid_properties_no_d,
                             physical_prop::RP_d &rock_properties,
                             physical_prop::RP &rock_properties_no_d)
      : sites{the_sites}, rocktypes{rkt},
        closure_laws{the_sites,        rkt,
                     fluid_properties, fluid_properties_no_d,
                     rock_properties,  rock_properties_no_d} {
    // static_assert(physics::nb_phases < 3 &&
    //               "probably wrong _nb_max_clos_laws formula");
  }

  // without Schur elimination, closure laws contribution are added to jac and
  // RHS
  template <typename LS_matrix, typename LS_RHS>
  void set_up(LS_matrix &Jac, LS_RHS &RHS, const physics::State_field &states,
              const physics::Context_field &contexts) {

    for (auto &&s : sites) {
      Nat_block jac_var; // size is nat var * nat var
      jac_var.topRows(physics::nb_dof).setZero();
      // compute closure laws and deriv wrt all natural variables
      jac_var.bottomRows(physics::nb_all_clos_laws) =
          closure_laws.site_closure_laws_jac(states(s), contexts(s),
                                             rocktypes(s));
      closure_laws.cl_residuals(s) = closure_laws.site_closure_laws_residual(
          states(s), contexts(s), rocktypes(s));
      // auto nb_cl = physics::nb_all_clos_laws;
      // store them (optim: only the nb_cl first rows have been filled,
      // depends on the context)
      // auto nb_cl = contexts_info[physics::context_index(contexts(s))]
      //                  .nb_secondary_unknowns;
      // var_closure_laws[s].topRows(nb_cl) = var_all_unknowns.topRows(nb_cl);
      // residual_closure_laws(s).topRows(nb_cl) = residual.topRows(nb_cl);

      // fill closure laws Jac and RHS contrib
      Jac(s, s) += jac_var;
      // residual
      RHS(s).bottomRows(physics::nb_all_clos_laws) -=
          closure_laws.cl_residuals(s);
    }
  }

  // no elimination is done, fill object of good size (optim:
  // do not call this function without Schur)
  template <typename LS_matrix>
  inline std::tuple<Nat_block, Nat_col_block>
  secondary_elimination(icus::index_type, const physics::Context &,
                        const LS_matrix &balance_Jac) {

    Nat_block jac_var;          // size is nat var * nat var
    Nat_col_block nul_residual; // size is nat var
    jac_var.bottomRows(physics::nb_all_clos_laws).setZero();
    nul_residual.setZero();
    // fill balance contrib
    jac_var.topRows(physics::nb_dof) = balance_Jac;

    // linear algebra on lines and columns to eliminate P^alpha and to align
    // the unknowns with the equations

    return std::make_tuple(jac_var, nul_residual);
    // auto block_size = nb_cl + physics::nb_dof;
    // return std::make_tuple(all_jac.topLeftCorner(block_size, block_size),
    //                        all_residual.topRows(block_size));
  }

  template <typename LS_RHS>
  // remove context
  LS_RHS &reconstruct(const physics::Context_field &, LS_RHS &solutions) {
    // no elimination, no reconstruction
    return solutions;
  }

  Cl_block_vect &compute_residuals(const physics::State_field &states,
                                   const physics::Context_field &contexts) {
    return closure_laws.compute_residuals(states, contexts);
  }

  Cl_block_vect &get_residuals() { return closure_laws.cl_residuals; }

  template <typename LS_RHS>
  inline void reset_dirichlet_vect(LS_RHS &vect,
                                   const icus::ISet &dir_bound_sites,
                                   const Site_id_field &sites_id) {
    auto tin = std::chrono::high_resolution_clock::now();
    // Dirichlet BC fill vect (0.0)
    for (auto &&sb :
         dir_bound_sites.mapping()) { // in num_scheme.sites.location
      if (sites_id(sb).temperature == BC::dirichlet) {
        if (sites_id(sb).pressure == BC::dirichlet)
          vect(sb).setZero(); // reset also the closure laws !
        else
          throw std::runtime_error(
              "Not possible to distinguish between pressure and temperature "
              "dir without Schur.");
      } else {
        assert(sites_id(sb).pressure != BC::dirichlet &&
               "Dirichlet condition on P only are not handled!");
      }
    }
    auto tout = std::chrono::high_resolution_clock::now();
    _tspent +=
        std::chrono::duration_cast<std::chrono::duration<double>>(tout - tin);
    // std::cerr << "time spent in Closure_laws_without_Schur: " <<
    // _tspent.count()
    //           << std::endl;
  }

  template <typename LS_matrix>
  // modification of the Dirichlet sites is done AFTER the Schur complement
  // then the matrix Jac is square
  // FIXME: this code seems to be a duplicate between
  // closure_laws_(with/without)_schur.h
  inline void reset_dirichlet_jac(LS_matrix &Jac,
                                  const icus::ISet &dir_bound_sites,
                                  const Site_id_field &sites_id) {

    // Dirichlet BC, modify Jac (identity)
    std::vector<icus::index_type> drows_pT;
    drows_pT.reserve(dir_bound_sites.size()); // possibly too much
    for (auto &&sb :
         dir_bound_sites.mapping()) { // in num_scheme.sites.location
      if (sites_id(sb).temperature == BC::dirichlet) {
        if (sites_id(sb).pressure == BC::dirichlet) {
          drows_pT.push_back(sb);
        } else { // only T is Dirichlet
          throw std::runtime_error(
              "Not possible to distinguish between pressure and temperature "
              "dir without Schur.");
        }
      } else {
        assert(sites_id(sb).pressure != BC::dirichlet &&
               "Dirichlet condition on P only are not handled!");
      }
    }
    Jac.set_rows_to_identity(drows_pT);
  }

  template <typename LS_RHS, typename LS_residual>
  auto init_rhs_with_residual(const physics::Context_field &contexts,
                              LS_RHS &RHS, const LS_residual &residual) {
    for (auto i : sites)
      RHS(i).topRows(physics::nb_dof) -= residual(i); // balance eq residual
  }
};

} // namespace coats
