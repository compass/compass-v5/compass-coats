#pragma once
#include <icus/Field.h>

// add outflow for the temperature ?
enum BC : char { dirichlet, neumann, interior };

// Store data of Site about own/ghost; fractures and boundary conditions
// FIXME: pressure stands for "molar balance equationS"
// FIXME: temperature stands for "energy balance equation"
struct Site_id {
  // char proc;        // "o"/"g": own/ghost
  // char frac;        // "y"/"n": in fracture/not in fracture
  BC pressure;
  BC temperature;
};

using Site_id_field = icus::Field<Site_id>;
