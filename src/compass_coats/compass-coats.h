#pragma once
#include "loaf/block_matrix.h"
#include "loaf/solve.h"
#include "loaf/vector.h"
#include <coats_variables/coats-variables.h>
#include <coats_variables/connection_density.h>
#include <compass_coats/assembler.h>
#include <compass_coats/closure_laws_with_schur.h>
#include <icmesh/Geomtraits.h>
#include <physicalprop/physicalprop.h>
#include <physics/model.h>
#include <scheme_lfv/scheme-lfv.h>
#include <scheme_lfv/tpfa.h>
#include <scheme_lfv/vag.h>

namespace coats {
using namespace icus;
using namespace icmesh;
using namespace numscheme;
using Tensor3D = numscheme::Tensor_d<3>;
using Tensor_field_3D = numscheme::Tensor_field<3>;

template <typename Scheme, typename Pressures_functors, physics::Phase... ph>
inline auto phase_driving_force(Scheme &scheme, Scalar_field &darcy_trans,
                                typename Scheme::Mesh_t::Vertex gravity,
                                Pressures_functors &pressures,
                                Phase_density_funct_t &densities,
                                physics::Phase_set<ph...>) {
  // define the equivalent phase volumetric mass density
  // for one connection (s0 -> s1), what is the density value and deriv
  // depending on the present phase (density function may not be defined
  // when the phase is absent)
  auto rho_eq = physics::phase_functors(
      Connection_density(ph, densities.template get<ph>())...);
  // define driving force K grad P - rho (g scalar position)
  return physics::phase_functors(LFVS_driving_force(
      LFVS_flux(scheme, darcy_trans, pressures.template get<ph>()),
      LFVS_gravity(scheme, darcy_trans, rho_eq.template get<ph>(),
                   gravity))...);
}

template <typename Scheme>
inline auto init_driving_force(Scheme &scheme, Scalar_field &darcy_trans,
                               physical_prop::FMP_d &fluid_properties,
                               typename Scheme::Mesh_t::Vertex gravity) {
  // build flux : lambda grad(P^alpha)
  auto &&pressures_f = init_phase_pressure_functors();
  auto &&densities_f = init_phase_density_functors(fluid_properties);

  return phase_driving_force(scheme, darcy_trans, gravity, pressures_f,
                             densities_f, physics::all_phases{});
}

template <typename Scheme, typename Closure_laws>
inline auto init_assembler(
    const Scheme &scheme, physical_prop::FMP_d &fluid_properties,
    physical_prop::RP_d &rock_properties, Closure_laws &closure_laws,
    physics::Rocktype_field rocktypes, physics::Scalar_field poro_darcy_vol,
    physics::Scalar_field poro_fourier_vol,
    physics::Scalar_field rock_fourier_vol, physics::Scalar_field darcy_trans,
    physics::Scalar_field fourier_trans,
    typename Scheme::Mesh_t::Vertex gravity, ISet bound_faces,
    physics::Acc_field neum_sources, physics::Acc_field vol_sources,
    Site_id_field sites_id) {

  using namespace numscheme;
  using namespace physics;

  // driving force : k grad P - rho (g scalar position)
  auto driving_force =
      init_driving_force(scheme, darcy_trans, fluid_properties, gravity);
  auto lambda_grad_T =
      LFVS_flux(scheme, fourier_trans, init_temperature_functor());

  return Assembler{scheme,           fluid_properties, rock_properties,
                   closure_laws,     poro_darcy_vol,   poro_fourier_vol,
                   rock_fourier_vol, driving_force,    lambda_grad_T,
                   rocktypes,        bound_faces,      neum_sources,
                   vol_sources,      sites_id};
}
} // namespace coats
