import physics as compass_physics
from physicalprop.properties import Properties
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import (
    init_default_rock_physical_properties,
)
from icus import field
from icus.fig import RecordShelf, Shelf, Scalar, Tensor, Zone, Data, Record
from icmesh.dump import write_mesh
import physics.dump as physics_dump
from compass_utils.output_visu import tensor_coordinates
import numpy as np
from compass_coats.assembler import Coats_assembler
from compass_coats.output_callbacks import CoatsOutputCallbacks
from globalgo.config import NewtonInitializer


class Descriptor:
    def __init__(self, instance=None, name=None):
        self.instance = instance
        self.name = name

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        return type(self)(instance, self.name)


class Coats_utils(Descriptor):
    # self.instance contains Coats instance
    def equilibriate_state(
        self,
        *,
        context,
        p,
        T,
        Sg=None,
        Cal=None,
        Cag=None,
        rocktype=None,
    ):
        context_id = self.instance.contexts[
            context
        ]  # should be optional and deduced from other arguments
        # rename build_state into equilibriate_state ?
        return self.instance.properties.build_state(
            context_id,
            p=p,
            T=T,
            Sg=Sg,
            Cal=Cal,
            Cag=Cag,
            rocktype=rocktype,
        )


class Coats_lineariser:
    """
    Implements globalgo.interfaces.LinearizedProblemInterface
    """

    def __init__(self, model, scheme, mesh):
        self.scheme = scheme
        self.mesh = mesh
        self.model = model
        # self.sites_data is a Shelf with all the scheme data,
        # ie data created over the sites (dof) thanks to the model, mesh, scheme
        # and the mesh_data
        # todo: I dislike storing sites_data here, different from the lineariser functor
        self.sites_data = None

    def set_assembler(self, assembler, *, callbacks=None):
        self.__assembler__ = assembler
        self.__residual_norm__ = assembler.residual_norm
        self.__events__ = None
        self.__callbacks__ = callbacks or CoatsOutputCallbacks

    def get_solver_initializer(self, config, namespaces, type_key):
        return NewtonInitializer(self, config, namespaces, type_key)


class Coats:
    # contains equilibriate_state...
    utils = Coats_utils()

    def data_spec(self, physics, dim):
        location_data = lambda diff_type: {
            # icus.Labels is a field of C int which is np.dtype("i")
            "rocktypes": ("i", Scalar),
            "petrophysics": {
                "porosity": ("double", Scalar),
                "permeability": ("double", diff_type),
                "thermal_conductivity": ("double", diff_type),
            },
            "volumetric_sources": (physics.Accumulation, Scalar),
        }
        data_dict = {
            "matrix": location_data(Tensor(dim)),
            "boundary_conditions": {
                "Dirichlet_states": (physics.State, Scalar),
                "Dirichlet_contexts": (physics.Context, Scalar),
                "Neumann_flux": (physics.Accumulation, Scalar),
            },
            "initial_states": (physics.State, Scalar),
            "initial_contexts": (physics.Context, Scalar),
        }
        if self.with_fractures:
            data_dict.update(
                # diffusion coef must be a scalar,
                # how to define anisotropy on objects of co-dimension 1 ?
                {"fractures": location_data(Scalar)}
            )
            data_dict["fractures"].update(
                {
                    "faces": ("bool", Scalar),
                    "thickness": ("double", Scalar),
                }
            )

        return data_dict

    def __init__(
        self,
        physics,
        *,
        thermal=True,
        with_fractures=True,
        phases=None,
        components=None,
        contexts=None,
        gravity=np.array([0.0, 0.0, 9.81]),
    ):
        self.gravity = gravity
        assert physics in ["pure", "diphasic", "water2ph"]
        self.physics_name = physics
        self.physics = compass_physics.load(physics)
        self.thermal = thermal
        self.with_fractures = with_fractures

        if phases is not None:
            assert len(phases) == self.physics.nb_phases
            self.phases = {phase: alpha for alpha, phase in enumerate(phases)}
        else:
            self.phases = {
                phase: alpha
                for alpha, phase in compass_physics.phases_dict(self.physics).items()
            }

        if components is not None:
            assert len(components) == self.physics.nb_components
            self.components = {comp: i for i, comp in enumerate(components)}
        else:
            self.components = {
                comp: i
                for i, comp in compass_physics.components_dict(self.physics).items()
            }

        if contexts is not None:
            assert len(contexts) == self.physics.nb_contexts
            # or give automatically the name depending on the phase names
            self.contexts = {context: i for i, context in enumerate(contexts)}
        else:
            self.contexts = {
                context: i
                for i, context in compass_physics.contexts_dict(self.physics).items()
            }

        assert thermal == True, "isothermal not implemented yet"
        # index of the thermal equation
        self.energy = len(self.components)

        # load the default physical properties of the physics
        fluid_properties = init_default_fluid_physical_properties(self.physics)
        rock_properties = init_default_rock_physical_properties(self.physics)
        self.properties = Properties(fluid_properties, rock_properties)

    @property
    def nb_phases(self):
        return self.physics.nb_phases

    @property
    def nb_components(self):
        return self.physics.nb_components

    def new_data(self, dim):
        """
        Create the structure which will contain all the data carried by the mesh.
        Takes the mesh dimension as argument.

        The fractures are always co-dimension 1 objects.
        """
        data = RecordShelf(self.data_spec(self.physics, dim))
        # init with default value
        # rkt: sites values will be constructed from cells values
        data.matrix.rocktypes[...] = 0
        # init volumetric_sources, source must be defined over all cells
        null_acc = data.matrix.volumetric_sources.dtype()
        # already init with 0
        if self.thermal:
            null_acc.energy = 0.0
        null_acc.molar.fill(0.0)
        data.matrix.volumetric_sources[...] = null_acc
        if self.with_fractures:
            data.fractures.rocktypes[...] = 0
            data.fractures.volumetric_sources[...] = null_acc
        return data

    # mesh_data (over geometry) is distributed into scheme data (over sites)
    # mesh_data is a RecordShelf to facilitate the Data construction,
    # self.data contains directly the Data objects to be able to manipulate
    # them during the simu
    def build_data_over_sites(self, scheme, mesh, mesh_data):
        # factorize the distribution of the data
        def distribute_over_sites(
            loc_name,
            petro,
            scheme_perm,
            mesh_loc,
            loc_measures,
            mesh_vol_sources,
            sites_sources,
            *,
            omega_Darcy=0.075,
            omega_Fourier=0.075,
        ):
            # need to initialize the distribution coefficients
            # used for example to compute the porous volume applied at each site
            # and to distribute the volumetric sources
            if self.thermal:
                scheme_th_cond = petro.thermal_conductivity[mesh_loc]
                scheme.init_distribute(
                    loc_name,
                    scheme_perm,
                    scheme_th_cond,
                    omega_Darcy=omega_Darcy,
                    omega_Fourier=omega_Fourier,
                )
                # To init the sites sources, multiply by the cell volumes
                # and distribute the volume over the sites
                # what concerns the energy is distributed using the Fourier coeffs.
                sites_sources.energy += scheme.Fourier_distribute(
                    loc_name,
                    mesh_vol_sources[mesh_loc].energy * loc_measures,
                )
            else:
                scheme.init_distribute(loc_name, omega_Darcy, scheme_perm)

            # To init the sites sources, multiply by the cell volumes
            # and distribute the volume over the sites
            # what concerns the molar source is distributed using the Darcy coeffs.
            sites_sources.molar += scheme.Darcy_distribute(
                loc_name,
                # loc_measures is 1D array, transform it into 2D array to multiply
                mesh_vol_sources[mesh_loc].molar
                * loc_measures.field.as_array()[:, None],
            )
            return sites_sources

        # create the Record which will contain the sites rocktypes
        # mesh_data.matrix.rocktypes.dtype = mesh_data.fractures.rocktypes.dtype, idem for kind
        sites_rkt = Record(
            mesh_data.matrix.rocktypes.dtype, mesh_data.matrix.rocktypes.kind
        )
        # create the Data which will contain the sources distributed over the sites
        # The sources contributions are summed
        # mesh_data.matrix.volumetric_sources.dtype = mesh_data.fractures.volumetric_sources.dtype
        sites_sources = Data.from_zone(
            scheme.sites,
            dtype=mesh_data.matrix.volumetric_sources.dtype,
        )

        # for the matrix (scheme.cells can be empty):
        if scheme.cells:
            mat_petro = mesh_data.matrix.petrophysics
            cells_perm = mat_petro.permeability[scheme.cells]
            # rocktypes are distributed over sites (cells and VAG nodes if VAG)
            # VAG: the nodes values depend on the permeabilities
            sites_rkt[...] = scheme.build_sites_labels(
                cells_perm,
                mesh_data.matrix.rocktypes[scheme.cells],
            )

            scheme_cells_meas = Data(mesh.cell_measures)[scheme.cells]
            # initialize the volume distribution and update the sites_sources
            sites_sources = distribute_over_sites(
                "matrix",
                mat_petro,
                cells_perm,
                scheme.cells,
                scheme_cells_meas,
                mesh_data.matrix.volumetric_sources,
                sites_sources,
            )

        # for the fractures (keep it after matrix filling):
        if self.with_fractures:
            frac_petro = mesh_data.fractures.petrophysics
            frac_perm = frac_petro.permeability[scheme.frac_faces]
            # scheme.frac_faces can be None, empty Zone or non-empty Zone
            if scheme.frac_faces:
                # rocktypes are distributed over sites (frac faces and VAG nodes if VAG)
                # VAG: the nodes values depend on the permeabilities
                # VAG nodes are overwritten if necessary
                sites_rkt[...] = scheme.build_sites_labels(
                    frac_perm,
                    mesh_data.fractures.rocktypes[scheme.frac_faces],
                )

                scheme_frac_meas = Data(mesh.face_measures)[scheme.frac_faces]
                # initialize the volume distribution and update the sites_sources
                sites_sources = distribute_over_sites(
                    "fractures",
                    frac_petro,
                    frac_perm,
                    scheme.frac_faces,
                    scheme_frac_meas,
                    mesh_data.matrix.volumetric_sources,
                    sites_sources,
                    omega_Darcy=0.15,
                    omega_Fourier=0.15,
                )

        # Boundary conditions
        bc = mesh_data.boundary_conditions
        # Distribute Neumann faces info over sites (including multiplication
        # by the face surface)
        scheme_data_Neumann_flux = scheme.build_sites_neumann_BC(
            # need geometry info and BC initialized over the geometry
            mesh,
            bc.Neumann_flux,
        )

        # complete initial states and contexts with Dirichlet values
        # will be distributed over sites after
        Dirichlet_states_loc = bc.Dirichlet_states[scheme.dirichlet_mesh_zone]
        Dirichlet_contexts_loc = bc.Dirichlet_contexts[scheme.dirichlet_mesh_zone]
        mesh_data.initial_states[...] = Dirichlet_states_loc
        mesh_data.initial_contexts[...] = Dirichlet_contexts_loc
        # distribute initial states and contexts
        sites_location = Zone(scheme.sites.iset.location)
        scheme_data_init_states = scheme.from_location(
            mesh_data.initial_states[sites_location],
        )
        scheme_data_init_contexts = scheme.from_location(
            mesh_data.initial_contexts[sites_location],
        )
        # init self.data.bc.Dir (not really necessary, for user api)
        scheme_data_Dirichlet_states = scheme.from_location(
            Dirichlet_states_loc,
        )
        scheme_data_Dirichlet_contexts = scheme.from_location(
            Dirichlet_contexts_loc,
        )

        sites_data_dict = {
            "rocktypes": sites_rkt[scheme.sites],
            "boundary_conditions": {
                "Dirichlet_states": scheme_data_Dirichlet_states,
                "Dirichlet_contexts": scheme_data_Dirichlet_contexts,
                "Neumann_flux": scheme_data_Neumann_flux,
            },
            "initial_states": scheme_data_init_states,
            "initial_contexts": scheme_data_init_contexts,
            "sources": sites_sources,
        }
        if scheme.cells:
            sites_data_dict.update(
                {
                    "matrix": {
                        "petrophysics": {
                            # careful, carried by the mesh !!!
                            "porosity": mat_petro.porosity[scheme.cells],
                            "permeability": cells_perm,
                            # careful, carried by the mesh !!!
                            "thermal_conductivity": mat_petro.thermal_conductivity[
                                scheme.cells
                            ],
                        },
                    },
                }
            )

        if self.with_fractures:
            sites_data_dict.update(
                {
                    "fractures": {
                        "petrophysics": {
                            # careful, carried by the mesh !!!
                            "porosity": frac_petro.porosity[scheme.frac_faces],
                            "permeability": frac_perm,
                            # careful, carried by the mesh !!!
                            "thermal_conductivity": frac_petro.thermal_conductivity[
                                scheme.frac_faces
                            ],
                        },
                    },
                }
            )

        return Shelf(sites_data_dict)

    def set_up(self, scheme_def, mesh, mesh_data):
        # create the numerical scheme, the cpp class is stored in scheme.instance
        # different in VAG and TPFA
        frac_data = None
        if self.with_fractures:
            frac_data = mesh_data.fractures.faces
        scheme = scheme_def.build(
            mesh, mesh_data.boundary_conditions, frac_data=frac_data
        )

        # contains the functor to build the linearized loop
        # and the data over the sites
        coats_linear = Coats_lineariser(self, scheme, mesh)

        # coats_linear.sites_data is a Shelf with all the scheme data,
        # ie data created over the sites (dof) thanks to the model, mesh, scheme
        # and the mesh_data
        # todo : mesh_data must disappear and coats_linear.sites_data must carry all the data
        coats_linear.sites_data = self.build_data_over_sites(scheme, mesh, mesh_data)

        # init transmissivities (scheme.Darcy_trans and scheme.Fourier_trans)
        # different in VAG and TPFA
        scheme.init_transmissivities(coats_linear.sites_data)

        assembler = Coats_assembler(
            mesh,
            self,  # model
            scheme,
            coats_linear,
        )

        coats_linear.set_assembler(assembler)
        return coats_linear

    def dump_data_over_mesh(
        self,
        basename,
        data,
        mesh,
        *,
        cells=None,
        no_data_float=np.nan,
        no_data_int=-1,
    ):
        """
        Creates vtu file with the data over a specified mesh.
        Tensor can be visualized in paraview with the following procedure
        - create a Cell Centers filter as a child of the whole grid or a filter thereof
        - create a Tensor Glyph filter as child of the Cell Centers
        - in the Tensor Glyph filter's properties, select the property among the Tensors chooser
        - adjust radius
        """
        # fill the non initialized values with nan
        def complete_definition(mesh_data, zone):
            # get the intersection between the mesh zone and the given values zone
            given_data_zone = mesh_data.definition_zone(zone)
            given_data = mesh_data[given_data_zone]
            dtype = mesh_data.get_field_type()
            full_data = Data.from_zone(zone, dtype=dtype)
            # init with NaN or (-1) if integer (there is no int nan)
            try:
                np.asarray(full_data)[...] = no_data_float
            except:
                np.asarray(full_data)[...] = no_data_int
            full_data[:] = given_data
            return full_data

        if cells is None:
            cells = Zone(mesh.cells)
        # get the nodes connected to cells
        cells_nodes = mesh.cells_nodes.extract(
            cells.iset,
            mesh.nodes,
        )
        nodes = Zone(cells_nodes.image())
        petro = data.matrix.petrophysics
        celldata = {
            "cell rocktypes": complete_definition(data.matrix.rocktypes, cells),
            "cell porosity": complete_definition(petro.porosity, cells),
        }
        celldata.update(
            tensor_coordinates(
                complete_definition(petro.permeability, cells),
                "cell permeability",
                diagonal_only=True,
            )
        )
        celldata.update(
            tensor_coordinates(
                complete_definition(petro.thermal_conductivity, cells),
                "cell thermal cond",
                diagonal_only=True,
            )
        )
        # matrix.volumetric_sources
        volsource = complete_definition(data.matrix.volumetric_sources, cells)
        celldata.update(
            physics_dump.accumulation_dict(
                volsource.field,
                self.components,
                prefix="Cell source for ",
            )
        )
        # get the dictionnary filled with initial info on cells
        init_states = complete_definition(data.initial_states, cells)
        init_ctx = complete_definition(data.initial_contexts, cells)
        celldata.update(
            physics_dump.states_dict(
                init_states.field,
                init_ctx.field,
                self.phases,
                self.components,
                prefix="Init cell ",
            )
        )
        # point data
        pointdata = {}
        # get the dictionnary filled with Dirichet info on nodes
        bc = data.boundary_conditions
        dir_states = complete_definition(bc.Dirichlet_states, nodes)
        dir_ctx = complete_definition(bc.Dirichlet_contexts, nodes)
        pointdata.update(
            physics_dump.states_dict(
                dir_states.field,
                dir_ctx.field,
                self.phases,
                self.components,
                prefix="Dir node ",
            )
        )
        # get the dictionnary filled with initial info on nodes
        init_states = complete_definition(data.initial_states, nodes)
        init_ctx = complete_definition(data.initial_contexts, nodes)
        pointdata.update(
            physics_dump.states_dict(
                init_states.field,
                init_ctx.field,
                self.phases,
                self.components,
                prefix="Init node ",
            )
        )

        write_mesh(basename, mesh, celldata=celldata, pointdata=pointdata)
