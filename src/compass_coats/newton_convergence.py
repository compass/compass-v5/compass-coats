import numpy as np
import scipy.linalg


class Coats_norm:
    def __init__(self):
        self.reference_closure = 0

    def set_up(self, solution, assembler):
        self.assembler = assembler  # done each timestep ???
        self.relative_residuals = []
        # get the site accumulation
        accumulations = assembler._instance.get_accumulations()
        # compute the balance equations residual
        assembler._instance.balance_equations_residual(
            assembler.dt,
            assembler.residual,
            solution.states,
            solution.contexts,
        )
        # compute the closure laws residual, not necessary to store it
        cl_residual = assembler.closure_laws.compute_residuals(
            solution.states, solution.contexts
        )
        self.reset_references(
            assembler.dt,
            accumulations.as_matrix(),
            assembler.residual.as_matrix(),
            cl_residual.as_matrix(),
        )
        return self.reference_pv, self.reference_closure

    def pv_norms(self, balance_residual):
        #     # local = np.linalg.norm(residuals.own_nodes, 1, axis=0)
        #     # local += np.linalg.norm(residuals.own_fractures, 1, axis=0)
        #     # local += np.linalg.norm(residuals.own_cells, 1, axis=0)
        #     # local[0] += np.linalg.norm(residuals.own_injectors, 1)
        #     # local[0] += np.linalg.norm(residuals.own_producers, 1)
        #     global_norms = np.zeros(self.npv(), dtype=np.double)
        #     # MPI.COMM_WORLD.Allreduce(local, global_norms, MPI.SUM)
        global_norms = scipy.linalg.norm(balance_residual, axis=0)
        self._global_norms = global_norms
        return global_norms

    @property
    def latest_pv_norms(self):
        return self._global_norms

    def closure_norm(self, closure_residual):
        closure = scipy.linalg.norm(closure_residual)
        # closure = MPI.COMM_WORLD.allreduce(closure, MPI.SUM)
        self._closure = closure
        return closure

    @property
    def latest_closure_norm(self):
        return self._closure

    def reset_conservation_reference(self, dt, balance_accumulation, balance_residual):
        # global accumulation over the whole simulation domain (all procs involved)
        global_accumulation = scipy.linalg.norm(balance_accumulation, axis=0)
        global_reference = global_accumulation / (1000.0 * dt) + 1.0
        self.reference_pv = np.maximum(
            self.pv_norms(balance_residual), global_reference
        )

    def reset_closure_reference(self, closure_residual):
        self.reference_closure = max(
            1.0, self.closure_norm(closure_residual)
        )  # FIXME: ?

    def reset_references(
        self, dt, balance_accumulation, balance_residual, closure_residual
    ):
        self.reset_conservation_reference(dt, balance_accumulation, balance_residual)
        self.reset_closure_reference(closure_residual)

    def relative_norm(self, balance_residual, closure_residual):
        assert self.reference_closure > 0
        assert np.all(
            self.reference_pv > 0
        ), "mininmum of reference value is %g" % np.min(self.reference_pv)
        closure_norm = self.closure_norm(closure_residual)
        return max(
            closure_norm / self.reference_closure,
            np.max(self.pv_norms(balance_residual) / self.reference_pv),
        )

    def norm(self, solution, rhs, dx):
        # get the closure laws residual, not necessary to store it
        closure_residual = self.assembler.closure_laws.get_residuals()
        self.relative_residuals.append(
            self.relative_norm(
                self.assembler.residual.as_matrix(),
                closure_residual.as_matrix(),
            )
        )
        return (
            self.relative_residuals[-1],
            self.latest_pv_norms,
            self.latest_closure_norm,
        )
