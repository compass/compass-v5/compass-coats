import numpy as np
from .bindings import (
    Closure_laws_without_Schur,
    Closure_laws,
    init_assembler,
    BC,
    Site_id,
)
from loaf import OnePhysicsVector3
from icus import field, intersect


class LS_Assembler:
    """Old assembler of the Coats with energy problem"""

    def __init__(
        self,
        the_physics,
        the_mesh,
        num_scheme,
        properties,
        rocktypes,
        poro_darcy_vol,
        poro_fourier_vol,
        rock_fourier_vol,
        Darcy_transmissivities,
        Fourier_transmissivities,
        dirichlet_mesh_objects,  # mesh numerotation (not site), faces or nodes
        Jac,
        RHS,
        Neumann_source=None,  # carried by sites
        volume_sources=None,  # carried by sites
        Schur_elimination=False,
        gravity=np.array([0.0, 0.0, 9.81]),
    ):
        if Schur_elimination:
            self.closure_laws = Closure_laws(
                num_scheme.sites,
                rocktypes,
                properties.fluid.cpp_prop_with_derivatives,
                properties.fluid.cpp_prop_without_derivatives,
                properties.rock.cpp_prop_with_derivatives,
                properties.rock.cpp_prop_without_derivatives,
            )
        else:
            self.closure_laws = Closure_laws_without_Schur(
                num_scheme.sites,
                rocktypes,
                properties.fluid.cpp_prop_with_derivatives,
                properties.fluid.cpp_prop_without_derivatives,
                properties.rock.cpp_prop_with_derivatives,
                properties.rock.cpp_prop_without_derivatives,
            )

        self.dirichlet_sites = intersect(
            dirichlet_mesh_objects, num_scheme.sites.location, num_scheme.sites.location
        )
        self.sites_id = field(num_scheme.sites, dtype=Site_id)
        self.sites_id.as_array().pressure[:] = BC.interior
        self.sites_id.as_array().temperature[:] = BC.interior
        # for s in BC_faces.mapping:
        #     self.sites_id[s].pressure = "n"
        #     self.sites_id[s].temperature = "n"
        for s in self.dirichlet_sites.mapping():
            self.sites_id.as_array().pressure[s] = BC.dirichlet
            self.sites_id.as_array().temperature[s] = BC.dirichlet

        if Neumann_source is None:
            Neumann_source = field(num_scheme.sites, dtype=the_physics.Accumulation)
            Neumann_source.as_array().energy[...] = 0.0
            Neumann_source.as_array().molar[...] = 0.0
        if volume_sources is None:
            volume_sources = field(num_scheme.sites, dtype=the_physics.Accumulation)
            volume_sources.as_array().energy[...] = 0.0
            volume_sources.as_array().molar[...] = 0.0

        # call a cpp function which returns an Assembler structure
        # initialized driving_force and LFVS_flux before assembler
        self._instance = init_assembler(
            num_scheme,
            properties.fluid.cpp_prop_with_derivatives,
            properties.rock.cpp_prop_with_derivatives,
            self.closure_laws,
            rocktypes,
            poro_darcy_vol,
            poro_fourier_vol,
            rock_fourier_vol,
            Darcy_transmissivities,
            Fourier_transmissivities,
            the_mesh.to_vertex(gravity),
            self.dirichlet_sites,
            Neumann_source,
            volume_sources,
            self.sites_id,
        )
        self.Jac = Jac
        self.RHS = RHS
        # assert(are_same_indexed_set(self.Jac.sites, self.RHS.sites))
        self.residual = OnePhysicsVector3(self.RHS.sites)

    def ls_description(self):
        """Used yet, was to initialize the linear solver system
        if necessary
        """
        return None

    def set_up(self, solution, dt, time):
        """Initialize the accumulation and store the fixed values of the
        Newton loop
        """
        self._instance.residu_reset_history(solution.states, solution.contexts)
        self.dt = dt

    def evaluate(self, solution):
        """Evaluate residual and jacobian of the Coats energy problem"""
        # compute Jac and RHS
        # RHS is different from residual due to Schur elimination
        self._instance.balance_equations_residual(
            self.dt,
            self.residual,
            solution.states,
            solution.contexts,
        )

        self._instance.discretize_balance_equations(
            self.dt,
            self.Jac,
            self.RHS,
            self.residual,
            solution.states,
            solution.contexts,
        )
        # to save the jac into a file
        # self.Jac.coo.sort_and_add_entries()
        # self.Jac.dump("jacobienne.txt")
        return self.RHS, self.Jac

    def update_solution(self, solution, dx):
        """Update the solution (flash included)"""
        self._instance.update_states(solution.states, solution.contexts, dx)
        return solution
