import math
import numpy as np
from compass_utils.units import year
from globalgo.core import TimeTick, AllAttemptsFailed
from compass_utils.exceptions import LinearSolverFailure
import time
import yaml
import os


class NewtonCallbacks:
    __exceptions__ = LinearSolverFailure

    def __init__(self, log=print, verbosity=0):
        self.log = log
        self.verbosity = verbosity  # 0 (less verbose) : 3 (most verbose)

    def timeloop_run(self, solution, stop, start=None, events=None):
        self.start = TimeTick.from_time(start, 0)
        self.stop = TimeTick.from_time(stop, math.inf)

    def timestepper_make_step(self, prev_solution, steps, tick):
        if self.verbosity > 0:
            if math.isfinite(self.stop.time):
                progess = (
                    100
                    * (tick.time - self.start.time)
                    / (self.stop.time - self.start.time)
                )
                msg_final_time = (
                    f" -> {self.stop.time:10.4g} s"
                    f" = {(self.stop.time / year):10.4g} y ({progess:.2f}% done)"
                )
            else:
                progess = (
                    100
                    * (tick.iteration - self.start.iteration)
                    / (self.stop.iteration - self.start.iteration)
                )
                msg_final_time = (
                    f" -> iteration {self.stop.iteration:6d} ({progess:.2f}% done)"
                )
            # time step : tick.iteration -> tick.iteration+1,
            # so time step it = tick.iteration+1
            self.log(f"\n{f'** Time Step (iteration): {tick.iteration+1} ':*<78}")
            self.log(
                f"Current time: {tick.time:10.4g} s = {(tick.time / year):10.4g} y"
                + msg_final_time
            )
        try:
            solution, dt, status = yield
        except AllAttemptsFailed as exc:
            residuals_msg = ", ".join(
                f"(it: {f.iteration}, res: {f.residual_norm})"
                for f in exc.attempt_history[-1].failure.args[1]
            )
            self.log(
                "\n------------------ ERROR: ------------------\n"
                f"All timestep attemps failed at iteration "
                f"{exc.attempt_history[-1].iteration}. "
                f"Timestepper is {exc.steps}\n"
                f"Newton residual norm are : {residuals_msg}"
            )
            raise
        if self.verbosity > 0:
            self.log(f"Success with timestep: {dt:10.4g} s = {(dt / year):10.4g} y")
        return solution, dt, status

    def timestepper_try_step(self, iteration, solution, dt, tick):
        solution, status = yield
        if status.failure is not None and self.verbosity > 0:
            self.log(
                f"*** Newton failure with timestep {status.dt:10.4g} s = {(status.dt / year):10.4g} y ***"
            )
            if self.verbosity > 1:
                if self.linear_solver_error is not None:
                    self.log(
                        f"     ==>  due to linear failure : {self.linear_solver_error}"
                    )
                else:
                    self.log(f"     ==>  it reaches its maximum number of iterations\n")
        return solution, status

    def newton_solve(self, first_guess, dt, time):
        if self.verbosity > 0:
            self.log(
                f"trying newton with timestep: {dt:10.4g} s = {(dt / year):10.4g} y"
            )
        solution, status = yield
        if self.verbosity > 0:
            self.log(
                f"Total Newton iteration: {len(status)-1} ; "
                f"linear solver iterations : {[s.ls_status.niterations for s in status[:-1]]}"
            )
        return solution, status

    def assembler_set_up(self, solution, **options):
        set_up_info = yield

    def assembler_evaluate(self, solution):
        residual, jacobian = yield

    def assembler_update_solution(self, prev_solution, dx):
        solution = yield

    def linear_solver_set_up(self, A):
        set_up_info = yield

    def linear_solver_solve(self, b):
        try:
            x, status = yield
        except Exception as err:
            # Here, you could dump linear system on error
            # or just save the exception for later processing
            self.linear_solver_error = err
            raise
        else:
            self.linear_solver_error = None
        return x, status


# Complete outputs with Coats specific callbacks
class CoatsOutputCallbacks(NewtonCallbacks):
    # 0 (less verbose) : 3 (most verbose)
    def __init__(self, log=print, verbosity=0):
        super().__init__(log, verbosity)
        # timeloop_log is modified in Standard_time_loop.run if necessary
        self.timeloop_log = None

    def timestepper_make_step(self, prev_solution, steps, tick):
        solution, dt, status = yield from super().timestepper_make_step(
            prev_solution, steps, tick
        )
        # solution, dt, status = yield
        if self.verbosity > 2:
            array_sol = solution.states.as_array()
            array_prev_sol = prev_solution.states.as_array()
            max_p_var = np.max(np.abs(array_sol.pressure - array_prev_sol.pressure))
            max_T_var = np.max(
                np.abs(array_sol.temperature - array_prev_sol.temperature)
            )
            max_S_var = np.max(np.abs(array_sol.saturation - array_prev_sol.saturation))
            self.log(f"max p variation: {max_p_var:.5g}")
            self.log(f"max T variation: {max_T_var:.5g}")
            self.log(f"max S variation: {max_S_var:.5g}")
        if self.timeloop_log is not None:
            self.timeloop_log.timeloop_callback(tick, solution, dt, status)

    def timestepper_try_step(self, iteration, solution, dt, tick):
        solution, status = yield from super().timestepper_try_step(
            iteration, solution, dt, tick
        )
        # solution, status = yield
        if status.failure is not None and self.timeloop_log is not None:
            self.timeloop_log.ls_iterations.append(
                [s.ls_status.niterations for s in status.failure.args[0]]
            )
            self.timeloop_log.newton_failure_callback()

    def newton_solve(self, first_guess, dt, time):
        solution, status = yield from super().newton_solve(first_guess, dt, time)
        if self.timeloop_log is not None:
            self.timeloop_log.dt = dt
            self.timeloop_log.ls_iterations.append(
                [s.ls_status.niterations for s in status[:-1]]
            )

    def residual_norm_set_up(self, first_guess, assembler):
        reference_pv, reference_closure = yield
        if self.verbosity > 1:
            self.log(
                f"timestep mass residual(s) scale(s): "
                f"{' '.join(f'{x:.5g}' for x in reference_pv[:-1])}\n"
                f"             energy residual scale: {reference_pv[-1]:.5g}\n"
                f"  closure equations residual scale: {reference_closure:.5g}"
            )

    def residual_norm_norm(self, solution, residual, dx):
        if dx is None:
            self.newton_iteration = 0
        norm, latest_pv_norms, latest_closure_norm = yield
        if self.verbosity > 1:
            self.log(
                f"Newton {self.newton_iteration : 3d} residuals: ",
                f"mass {' '.join(f'{x:12.5g}' for x in latest_pv_norms[:-1])} ",
                f"energy {latest_pv_norms[-1]:12.5g}",
                f" closure {latest_closure_norm:12.5g} "
                if latest_closure_norm > 0
                else "",
                f"relative (conv. crit.) {norm:12.5g}",
            )
        if self.timeloop_log is not None and self.newton_iteration > 0:
            self.timeloop_log.newton_iteration_callback(
                self.newton_iteration,
                norm,
            )
        self.newton_iteration += 1


class TimeloopLogCallback:
    """
    A structure that retrieves and stores operating data of the timeloop, newton and lsolver objects.
    Gathered data is stored into a nested dictionary and dumped to the output directory using YAML.
    File timeloop_log.yaml stores compact data for each time step
    (physical time, number of newton attempts, success dt).
    File time_step_log/time_step_<i>_log.yaml stores data for every attempt in time step i,
    detailed newton convergence, linear status and residual history in case of failure.
    """

    def __init__(self, directory):
        self.dict = {}
        now = time.time()
        self.last_newton_start = now
        self.last_timestep_start = now
        self.attempts = [{}]
        self.ls_iterations = []
        self.log_filename = directory / "timeloop_log.yaml"
        self.time_step_dir = directory / "time_step_log"
        os.makedirs(self.time_step_dir, exist_ok=True)
        with open(self.log_filename, "w") as f:
            pass  # Clearing the file if it already exists

    def timeloop_callback(self, tick, solution, dt, status):
        self.attempts[-1]["status"] = "success"
        timestep_dict = {"time": tick.time}
        newton_it = []
        ts_log_filename = (
            self.time_step_dir / f"time_step_{tick.iteration + 1}_log.yaml"
        )
        with open(ts_log_filename, "w") as f:
            pass  # create or clear the file

        for i, attempt_dict in enumerate(self.attempts):
            newton_it.append(len(attempt_dict) - 2)
            with open(ts_log_filename, "a") as f:
                yaml.safe_dump(
                    {f"attempt {i+1}": attempt_dict},
                    f,
                    default_flow_style=False,
                    sort_keys=False,
                )
        now = time.time()
        # rss = self.process.memory_info().rss / 1024**2
        timestep_dict.update(
            {
                "newton_iterations_per_attempt": newton_it,
                "lsolver_iterations_per_newton": self.ls_iterations,
                "success_dt": self.dt,
                # "memory": rss,
                "computation_time": now - self.last_timestep_start,
            }
        )
        self.last_timestep_start = now
        with open(self.log_filename, "a") as f:
            yaml.safe_dump(
                {f"time_step {tick.iteration + 1}": timestep_dict},
                f,
                default_flow_style=False,
                indent=2,
                sort_keys=False,
            )
        # reset self.attempts and ls_iterations for next time step
        self.attempts = [{}]
        self.ls_iterations = []

    def newton_iteration_callback(self, newton_it, norm):
        now = time.time()
        self.attempts[-1]["dt"] = self.dt
        self.attempts[-1][f"newton {newton_it}"] = {
            # ls_status does not exist yet ???
            # "linear_iterations": self.ls_status[].niterations,
            "cpu_time": now - self.last_newton_start,
            "residual": float(norm),
        }
        self.last_newton_start = now

    # def linear_failure_callback(self, newton_tick):
    #     now = time.time()
    #     self.attempts[-1]["dt"] = newton_tick.current_dt
    #     self.attempts[-1][f"newton {newton_tick.iteration}"] = {
    #         "linear_iterations": self.newton.lsolver.nit,
    #         "convergence_history": list(
    #             float(x) for x in self.newton.lsolver.residual_history
    #         ),
    #         "cpu_time": now - self.last_newton_start,
    #     }
    #     self.attempts[-1]["status"] = "linear_failure"
    #     self.last_newton_start = now
    #     self.attempts.append({})

    def newton_failure_callback(self):
        self.attempts[-1]["status"] = "newton_failure"
        self.attempts.append({})
