import numpy as np, icus
from pathlib import Path
from compass_utils import mpi, units
import physics.dump as physics_dump
import icmesh.dump as mesh_dump
from typing import NamedTuple
from compass_utils.filenames import output_directory as default_dir


class OutputDirectoryManager:
    def __init__(self, output_directory):

        self.path = Path(output_directory)
        self.mesh_directory = self.path / "mesh"
        self.states_directory = self.path / "states"
        self.wells_directory = self.path / "wells"
        self.res_directory = self.path / "residuals"
        if mpi.is_on_master_proc:
            for dir in [
                self.path,
                self.mesh_directory,
                self.states_directory,
                self.wells_directory,
                self.res_directory,
            ]:
                if not dir.is_dir():
                    dir.mkdir(parents=True)

    def proc_label(self, proc):
        return f"proc_{proc:06d}"

    def mesh_filename(self, proc):
        return self.mesh_directory / f"mesh_{self.proc_label(proc)}.npz"

    def state_filename(self, proc, tag=""):
        if tag:
            tag += "_"
        return self.states_directory / f"state_{tag}{self.proc_label(proc)}.npz"

    def res_filename(self, time, dt, nit, proc=None):
        if proc is None:
            return self.res_directory / f"res_time{time}_dt{dt}_nit{nit}.npz"
        return (
            self.res_directory
            / f"res_time{time}_dt{dt}_nit{nit}_{self.proc_label(proc)}.npz"
        )


class Location(NamedTuple):
    dim: int = None
    mapping2sites: list = []
    mapping2mesh: list = []


class Dumper:
    def __init__(self, locations, model, output_directory):
        self.outdir_manager = output_directory
        self.locations = locations
        self.model = model

    def set_up(self, mesh):
        # self.dump_own_element_numbers(mesh)
        # self.dump_wellids()
        self.dump_mesh(mesh)

    @mpi.on_master_proc
    def dump_own_element_numbers(self, mesh):
        filename = self.outdir_manager.path / "own_elements"
        communicator = mpi.communicator()
        np_linewidth_backup = np.get_printoptions()["linewidth"]
        np.set_printoptions(
            linewidth=np.inf
        )  # we want all outputs below on a single line
        with open(filename, "w") as f:
            print("# Number of procs", file=f)
            print("nb_procs =", communicator.size, file=f)
            # CHECKME: Parallel How to retrieve all mesh sizes ?
            print("nb_own_cells =", [mesh.cells.size()], file=f)
            print("nb_own_nodes =", [mesh.nodes.size()], file=f)
            print("nb_own_faces =", [mesh.faces.size()], file=f)
            # CHECKME : add fractures
        np.set_printoptions(linewidth=np_linewidth_backup)

    def dump_wellids(self):
        # CHECKME: retrieve well ids
        well_ids = [[]]
        if mpi.is_on_master_proc:
            filename = self.outdir_manager.path / "well_ids"
            with open(filename, "w") as f:
                for ids in well_ids:
                    for i in ids:
                        print(i, file=f)

    def dump_states(self, solution, tag="", dump_fluxes=None):
        physics_dump.dump_states(
            self.outdir_manager.state_filename(mpi.proc_rank, tag),
            solution.states,
            solution.contexts,
            self.model.phases,
            self.model.components,
        )

    # store the mesh connectivities + computation mesh and sites locations
    def dump_mesh(self, mesh_3d):
        self.vertices_dtype = mesh_3d.vertices.dtype
        mesh_dump.dump_vtu_3d(
            mesh_3d,
            self.outdir_manager.mesh_directory
            / f"mesh_{self.outdir_manager.proc_label(mpi.proc_rank)}.vtu",
        )
        # store mesh object dimensions
        locations = {f"{name}_dim": loc.dim for name, loc in self.locations.items()}
        # computation mesh and sites locations
        locations.update(
            {
                name: [loc.mapping2mesh, loc.mapping2sites]
                for name, loc in self.locations.items()
            }
        )
        # dump mesh info + locations into npz file
        mesh_dump.dump_mesh(
            self.outdir_manager.mesh_filename(mpi.proc_rank), mesh_3d, locations
        )

    def dump_residuals(self, time, dt, all_residuals):
        # all_residuals contains several residuals matrix :
        # each residuals matrix contains (nb_comp + 1) values by sites,
        # one matrix by Newton iteration
        for nit, residuals in enumerate(all_residuals):
            res_dict = {}
            for comp, i in self.model.components.items():
                res_dict[f"{comp} molar residual"] = residuals[:, i]
            res_dict["energy residual"] = residuals[:, self.model.energy]
            np.savez(
                self.outdir_manager.res_filename(
                    time,
                    dt,
                    nit,
                    mpi.proc_rank,
                ),
                **res_dict,
            )


def make_sites_dumper(mesh, sites, model, output_directory=None):
    # make_sites_dumper only implemented with dim=3
    assert mesh.dimension == 3
    # intersect does not conserve any order,
    # to do the link between the site index and the mesh object index
    # in postprocess, need the mapping between the intersection
    # and both the mesh and the sites
    cells2sites = icus.intersect(mesh.cells, sites.location, sites.location)
    cells2mesh = cells2sites.rebase(mesh.cells)
    faces2sites = icus.intersect(mesh.faces, sites.location, sites.location)
    faces2mesh = faces2sites.rebase(mesh.faces)
    edges2sites = icus.intersect(mesh.edges, sites.location, sites.location)
    edges2mesh = edges2sites.rebase(mesh.edges)
    nodes2sites = icus.intersect(mesh.nodes, sites.location, sites.location)
    nodes2mesh = nodes2sites.rebase(mesh.nodes)
    # Dimensions depend on the actual dimension of the mesh
    # mapping can be None (empty mapping), transform None into empty list
    f = lambda map: [] if map is None else map
    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=f(cells2sites.mapping_as_array()),
            mapping2mesh=f(cells2mesh.mapping_as_array()),
        ),
        "faces": Location(
            dim=mesh.dimension - 1,
            mapping2sites=f(faces2sites.mapping_as_array()),
            mapping2mesh=f(faces2mesh.mapping_as_array()),
        ),
        "edges": Location(
            dim=1,
            mapping2sites=f(edges2sites.mapping_as_array()),
            mapping2mesh=f(edges2mesh.mapping_as_array()),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=f(nodes2sites.mapping_as_array()),
            mapping2mesh=f(nodes2mesh.mapping_as_array()),
        ),
    }
    output_directory = output_directory or OutputDirectoryManager(default_dir())
    dumper = Dumper(locations, model, output_directory)
    dumper.set_up(mesh)
    return dumper


class Snapshooter:
    def __init__(self, dumper, log=print, verbosity=0):
        self.log = log
        self.verbosity = verbosity  # 0 (less verbose) : 3 (most verbose)
        self.dumper = dumper
        self.nb_snaphots = 0
        self.latest_snapshot_time = None
        self.filename = self.dumper.outdir_manager.path / "snapshots"
        self.res_filename = self.dumper.outdir_manager.path / "residuals_snapshots"
        if mpi.is_on_master_proc:
            with open(self.filename, "w") as f:
                pass
            with open(self.res_filename, "w") as f:
                pass

    def __call__(self, solution, tick):
        return self.shoot(solution, tick.time)

    def new_tag(self):
        tag = f"{self.nb_snaphots:05d}"
        self.nb_snaphots += 1
        return tag

    def shoot(self, solution, t):
        tag = self.new_tag()
        assert self.latest_snapshot_time is None or t >= self.latest_snapshot_time
        if t == self.latest_snapshot_time:
            return
        self.latest_snapshot_time = t
        if mpi.is_on_master_proc:
            if self.verbosity > 0:
                self.log(
                    f"[Snapshooter] Saving output at time: {t} s = {t/units.year} y"
                )
            with open(self.filename, "a") as f:
                print(tag, "%.12g" % t, file=f)

        self.dumper.dump_states(solution, tag=tag)

    def residuals_shoot(self, res_info):
        # res_info is a dict with tick, dt and trace,
        # where trace is the residual matrix : (nb_comp + 1) values by sites
        time = int(res_info["tick"].time)
        dt = int(res_info["dt"])
        all_residuals = res_info["trace"]
        self.dumper.dump_residuals(time, dt, all_residuals)
        if mpi.is_on_master_proc:
            with open(self.res_filename, "a") as f:
                for nit in range(len(all_residuals)):
                    print("%.12g" % time, "%.12g" % dt, nit, file=f)
