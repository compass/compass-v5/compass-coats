import warnings
import numpy as np, vtkwriters as vtkw
from .output_visu import OutputDirectoryManager
from compass_utils import units
from compass_utils.filenames import output_directory as default_dir
from compass_utils import mpi


def get_snapshots(outdir_manager):
    snapshots = []
    with (outdir_manager.path / "snapshots").open() as f:
        for line in f:
            if len(line.strip()) > 0:
                # snap index, snap time
                s0, s1 = line.split()
                snapshots.append((s0, float(s1)))
    return snapshots


def get_res_snapshots(outdir_manager):
    residuals = []
    with (outdir_manager.path / "residuals_snapshots").open() as f:
        for line in f:
            if len(line.strip()) > 0:
                # time, dt, nit
                s0, s1, s2 = line.split()
                residuals.append((s0, s1, s2))
    return residuals


def postprocess_npz(
    snap,
    mesh_data,
    cell_datatypes,
    face_datatypes,
    node_datatypes,
    dump_procid=False,
):
    # fill data with nan or -1, then overwrite with the snapshot data
    def fill_obj_data(to_mesh, to_site, n_obj, snap_data):
        assert to_mesh is not None
        assert to_site is not None
        # init all obj with NaN or (-1) if integer
        all_data = np.empty(
            [n_obj],
            dtype=snap_data.dtype,
        )
        if np.issubdtype(snap_data.dtype, np.integer):
            all_data.fill(-1)
        else:
            all_data.fill(np.nan)
        # fill the values read from the snapshot
        all_data[to_mesh] = snap_data[to_site]
        return all_data

    # not optimal, mesh is processed at each snapshot
    # mesh object dimensions (for example in 3D, cells=3, faces=2, edges=1, nodes=0)
    dimensions = {
        name: int(mesh_data[f"{name}_dim"])
        for name in ["cells", "faces", "edges", "nodes"]
        if f"{name}_dim" in mesh_data.keys()
    }
    n_mesh_cells = len(mesh_data["cells_faces_offsets"])
    n_mesh_nodes = len(mesh_data["vertices"])
    celldata, facedata, nodedata = {}, {}, {}

    # # todo: dump id of the proc where the obj is own
    # if dump_procid:
    #     nb_own_cells = self.distribution.nb_own_cells[proc]
    #     nb_own_fractures = self.distribution.nb_own_fractures[proc]
    #     own_only = True
    #     celldata.update(
    #         "proc": np.array(np.tile(proc, nb_own_cells), dtype=self.proc_id_type)
    #     )
    #     if nb_own_fractures > 0:
    #         facedata.update(
    #             "proc": np.array(
    #                 np.tile(proc, nb_own_fractures), dtype=self.proc_id_type
    #             )
    #         )

    # process the states data
    for data_name, snap_data in snap.items():
        for loc_name, loc_dim in dimensions.items():
            # mesh_data contains loc_mappings, a dict for each mesh object (cell, face, ...)
            # contained the mappings between (ex. with cell obj):
            # - the cell sites indices to the sites
            # - the cell sites indices to the cell mesh indices
            to_mesh, to_site = mesh_data[loc_name]
            assert to_mesh.size == to_site.size
            if to_mesh.size > 0:
                if loc_dim == 3:
                    # fill data with nan or -1, then overwrite with the snapshot data
                    cells_data = fill_obj_data(
                        to_mesh, to_site, n_mesh_cells, snap_data
                    )
                    celldata.update({f"{loc_name} " + data_name: cells_data})
                    cell_datatypes.update(
                        {loc_name + " " + data_name: cells_data.dtype}
                    )
                elif loc_dim == 2:
                    # get the data over the site
                    faces_data = snap_data[to_site]
                    facedata.update({f"{loc_name} " + data_name: faces_data})
                    face_datatypes.update(
                        {loc_name + " " + data_name: faces_data.dtype}
                    )
                elif loc_dim == 1:
                    warnings.warn(f"1D objects not postprocessed yet")
                elif loc_dim == 0:
                    # fill data with nan or -1, then overwrite with the snapshot data
                    nodes_data = fill_obj_data(
                        to_mesh, to_site, n_mesh_nodes, snap_data
                    )
                    nodedata.update({f"{loc_name} " + data_name: nodes_data})
                    node_datatypes.update(
                        {loc_name + " " + data_name: nodes_data.dtype}
                    )

    return celldata, facedata, nodedata


# postprocess files of ONE snapshot, there are as many files as procs,
# then write the vtu files and the pvtu one
def postprocess_pnpz(
    pnpz_files,
    mesh_files,
    state_vtu_dir,
    state_pvtu_filename,
    facestate_pvtp_filename,
    ofmt="binary",
):
    cell_datatypes = {}
    node_datatypes = {}
    face_datatypes = {}
    snapshot_vtu_files = []
    face_snap_vtu_files = []
    for state_file, mesh_file in zip(pnpz_files, mesh_files):
        # load states and mesh data
        state_data = np.load(state_file)
        # not optimal, mesh is loaded at each snapshot
        mesh_data = np.load(mesh_file, allow_pickle=True)

        # postprocess the sites state
        celldata, facedata, nodedata = postprocess_npz(
            state_data,
            mesh_data,
            cell_datatypes,
            face_datatypes,
            node_datatypes,
        )

        # write the vtu files
        # vertices coordinates
        vertices = mesh_data["vertices"]
        # volumic data
        state_vtu_file = state_vtu_dir / (state_file.stem + ".vtu")
        snapshot_vtu_files.append(state_vtu_file)
        coc_to_list = lambda s: np.split(
            mesh_data[f"{s}_values"], mesh_data[f"{s}_offsets"][:-1]
        )
        Fnodes = coc_to_list("faces_nodes")
        cells_faces = coc_to_list("cells_faces")
        # for each cell, the face nodes in consecutive nodes order
        Kfaces_nodes = [[Fnodes[f] for f in faces] for faces in cells_faces]
        vtkw.write_vtu(
            vtkw.polyhedra_vtu_doc(
                vertices,
                Kfaces_nodes,
                pointdata=nodedata,
                celldata=celldata,
                ofmt=ofmt,
            ),
            str(state_vtu_file),
        )

        # faces data
        # careful with the parallelism ??? If empty on some procs ?
        if facedata:
            facestate_vtu_file = state_vtu_dir / ("face" + state_file.stem + ".vtp")
            face_snap_vtu_files.append(facestate_vtu_file)
            # get the location of the visualized faces (part of the mesh faces)
            visu_faces, _ = mesh_data["faces"]
            # build 1D array to deal with unstructured mesh (non constant nb of nodes in each face)
            visuf_nodes = np.array(
                [n for f in visu_faces for n in Fnodes[f]], dtype=int
            )
            nodes_i, visuf_nodes = np.unique(visuf_nodes, return_inverse=True)
            # new numbering of the nodes
            i = 0
            visufaces_nodes = []
            for f in visu_faces:
                visufaces_nodes.append(visuf_nodes[i : i + Fnodes[f].size])
                i += Fnodes[f].size
            # rebuild node data with only the nodes belonging to the visu faces
            fnodedata = {}
            for nname, ndata in nodedata.items():
                fnodedata.update({nname: ndata[nodes_i]})

            vtkw.write_vtp(
                vtkw.vtp_doc(
                    vertices[nodes_i],
                    visufaces_nodes,
                    celldata=facedata,
                    pointdata=fnodedata,
                ),
                str(facestate_vtu_file),
            )

    # pvtu for volumic data
    vtkw.write_pvtu(
        vtkw.pvtu_doc(
            vertices.dtype,
            [file.name for file in snapshot_vtu_files],
            celldata_types=cell_datatypes,
            pointdata_types=node_datatypes,
        ),
        str(state_pvtu_filename),
    )
    # write surface data only if non empty
    if face_snap_vtu_files:
        vtkw.write_pvtp(
            vtkw.pvtp_doc(
                vertices.dtype,
                [file.name for file in face_snap_vtu_files],
                celldata_types=face_datatypes,
                pointdata_types=node_datatypes,
            ),
            str(facestate_pvtp_filename),
        )
        # True: faces are visualized
        return True
    # False: no face to visualize
    return False


def pvd_append(
    time,
    snapshots_pvd,
    faces_pvd,
    state_pvtu_filename,
    facestate_pvtp_filename,
    paraview_directory,
):
    for pvd, state_file in [
        [snapshots_pvd, state_pvtu_filename],
        [faces_pvd, facestate_pvtp_filename],
    ]:
        pvd.append(
            (
                time,
                state_file.relative_to(paraview_directory),
            )
        )


def postprocess(
    output_directory=default_dir(),
    time_unit="year",
    ofmt="binary",
):
    """postprocess a set of directories where output from ComPASS simulations are stored
    (typically something like output-scriptname)

    :param directory: directory to process, "output-scriptname" by default
    :param time_unit: a string among second, minute, hour, day, year, defaults to year

    """
    time_unit_value = {
        "second": 1,
        "minute": units.minute,
        "hour": units.hour,
        "day": units.day,
        "year": units.year,
    }[time_unit]
    outdir_manager = OutputDirectoryManager(output_directory)
    paraview_directory = outdir_manager.path / "paraview"
    state_vtu_directory = paraview_directory / "vtu"
    if not state_vtu_directory.is_dir():
        state_vtu_directory.mkdir(parents=True)
    nprocs = mpi.communicator().size
    assert nprocs == 1, "parallel postprocess not implemented yet"

    snapshots = get_snapshots(outdir_manager)
    snapshots_pvd = []
    snap_faces_pvd = []
    mesh_files = []
    for proc in range(nprocs):
        mesh_files.append(outdir_manager.mesh_filename(proc))

    # are there any faces to visualize ?
    visu_faces = False
    # postprocess states snapshot
    for snap, tsnap in snapshots:
        # postprocess all npz files (as many as nprocs) containing info
        # about this snapshot
        pnpz_files = []
        for proc in range(nprocs):
            pnpz_files.append(outdir_manager.state_filename(proc, snap))
        state_pvtu_filename = state_vtu_directory / f"state_{snap}.pvtu"
        facestate_pvtp_filename = state_vtu_directory / f"facesstate_{snap}.pvtp"
        # update visu_faces, must be true if there are faces in at least one snapshot
        new_visu_faces = postprocess_pnpz(
            pnpz_files,
            mesh_files,
            state_vtu_directory,
            state_pvtu_filename,
            facestate_pvtp_filename,
            ofmt=ofmt,
        )
        visu_faces = visu_faces or new_visu_faces
        # append time to the pvd states and faces states
        pvd_append(
            tsnap / time_unit_value,
            snapshots_pvd,
            snap_faces_pvd,
            state_pvtu_filename,
            facestate_pvtp_filename,
            paraview_directory,
        )

    vtkw.write_pvd(
        vtkw.pvd_doc(snapshots_pvd),
        str(paraview_directory / "states.pvd"),
    )
    if visu_faces:
        vtkw.write_pvd(
            vtkw.pvd_doc(snap_faces_pvd),
            str(paraview_directory / "face_states.pvd"),
        )


def postprocess_residuals(
    output_directory=default_dir(),
    ofmt="binary",
):
    outdir_manager = OutputDirectoryManager(output_directory)
    paraview_directory = outdir_manager.path / "paraview"
    res_vtu_directory = paraview_directory / "residuals"
    if not res_vtu_directory.is_dir():
        res_vtu_directory.mkdir(parents=True)

    res_snapshots = get_res_snapshots(outdir_manager)
    nprocs = mpi.communicator().size
    assert nprocs == 1, "parallel postprocess not implemented yet"
    # mesh files of all procs
    mesh_files = []
    for proc in range(nprocs):
        mesh_files.append(outdir_manager.mesh_filename(proc))

    # postprocess residuals snapshot
    for time, dt, nit in res_snapshots:
        # postprocess all npz files (as many as nprocs) containing info
        # about this residuals
        pnpz_files = []
        for proc in range(nprocs):
            pnpz_files.append(outdir_manager.res_filename(time, dt, nit, proc))
        res_pvtu_filename = res_vtu_directory / (
            outdir_manager.res_filename(time, dt, nit).stem + ".pvtu"
        )
        facesres_pvtp_filename = res_vtu_directory / (
            "face" + outdir_manager.res_filename(time, dt, nit).stem + ".pvtp"
        )

        postprocess_pnpz(
            pnpz_files,
            mesh_files,
            res_vtu_directory,
            res_pvtu_filename,
            facesres_pvtp_filename,
            ofmt=ofmt,
        )
