from compass_coats import (
    make_tpfa,
    make_vag_codim1,
    make_vag,
)
import icus
from icus.fig import Zone, Data, Record
from compass_utils import messages


class Scheme_def:
    #  if the scheme is not applied over the whole mesh, give the concerned cells
    def __init__(self, cells=None, schur_elimination=True, frac_faces=None):
        self.cells = cells  # if cells is None, will be set to all mesh cells
        # if frac_faces is None, will be modified if fractures
        self.frac_faces = frac_faces
        # elimination of the secondary unknowns
        self.schur_elimination = schur_elimination


class Scheme(Scheme_def):
    def __init__(self, scheme_def, cpp_instance, dirichlet_mesh_zone):
        super().__init__(
            scheme_def.cells, scheme_def.schur_elimination, scheme_def.frac_faces
        )
        # self.scheme_def = scheme_def
        assert self.cells is not None or self.frac_faces is not None
        # cpp class of the num scheme
        self.instance = cpp_instance
        self.sites = Zone(self.instance.sites)
        # todo: remove dirichlet_mesh_zone, use only dirichlet_sites
        # used only by TPFA ?
        self.dirichlet_mesh_zone = dirichlet_mesh_zone
        self.dirichlet_sites = self.from_location(self.dirichlet_mesh_zone)

    def from_location(self, obj, *, loc_iset=None):
        if isinstance(obj, Data):
            loc_iset = loc_iset or obj.field.support()
            # loc_iset is in location tree, get the corresponding iset in sites tree
            sites_iset = self.sites.iset.from_location(loc_iset)
            return Data(obj.field.share(sites_iset))
        if isinstance(obj, Zone):
            return Zone(self.sites.iset.from_location(obj.iset))
        messages.error("from_location is implemented with Data or Zone only")

    # commun function to distribute the Neumann info defined over the mesh faces
    # into info over the scheme sites (including multiplication
    # by the face surface)
    def build_sites_neumann_BC(self, mesh, faces_Neumann_flux):
        # first get intersection between Neumann faces and mesh faces
        initialized_faces = faces_Neumann_flux.definition_zone(Zone(mesh.faces))
        # then distribute the surface flux into the sites (and multiply by
        # the surface)
        return Data(
            self.instance.distribute_surfaces(
                faces_Neumann_flux[initialized_faces].field,
            )
        )

    def init_transmissivities(self, sites_data):
        if self.frac_faces and self.cells:
            # non empty cells zone
            assert False, "frac in a matrix not implemented yet"
        if self.cells:
            self.Darcy_trans = self.instance.build_transmissivities(
                sites_data.matrix.petrophysics.permeability[self.cells].field,
            )
            self.Fourier_trans = self.instance.build_transmissivities(
                sites_data.matrix.petrophysics.thermal_conductivity[self.cells].field,
            )
        elif self.frac_faces:
            self.Darcy_trans = self.instance.build_transmissivities(
                sites_data.fractures.petrophysics.permeability[self.frac_faces].field,
            )
            self.Fourier_trans = self.instance.build_transmissivities(
                sites_data.fractures.petrophysics.thermal_conductivity[
                    self.frac_faces
                ].field,
            )


class TPFA(Scheme_def):
    def build(self, mesh, bc_data, frac_data=None):
        if frac_data is not None:
            # get the frac faces zone
            def_zone = frac_data.definition_zone(Zone(mesh.faces))
            self.frac_faces = frac_data[def_zone] == True
            assert len(self.frac_faces) == 0, "TPFA fractures not implemented yet"
        # cells where the scheme is used, overload self.cells if init with None
        self.cells = self.cells or Zone(mesh.cells)
        # in TPFA, need first the Dirichlet objects, are the objects
        # given in bc_data.Dir which are faces amoung
        # the faces connecting the tpfa cells
        scheme_cells_faces = mesh.cells_faces.extract(
            self.cells.iset,
            mesh.faces,
        )
        possible_BC_zone = Zone(scheme_cells_faces.image())
        # dirichlet mesh objects
        # get intersection between the possible BC obj and the given values zone
        dirichlet_mesh_zone = bc_data.Dirichlet_contexts.definition_zone(
            possible_BC_zone
        )
        cpp_instance = make_tpfa(mesh, self.cells.iset, dirichlet_mesh_zone.iset)
        return TPFA_scheme(self, cpp_instance, dirichlet_mesh_zone)


class TPFA_scheme(Scheme):
    def init_distribute(self, *args, **kwargs):
        pass

    def Darcy_distribute(self, loc_name, quantities):
        return Data(self.instance.distribute_volumes(quantities.field))

    def Fourier_distribute(self, loc_name, quantities):
        return Data(self.instance.distribute_volumes(quantities.field))

    def build_sites_labels(self, mesh_perm, mesh_rkt):
        # bound face has the same rkt as its neighbouring cell
        # get the connectivity between bound face and neigh cells
        boundf_c = self.instance.bound_faces_cells
        assert icus.are_same_indexed_set(
            self.dirichlet_mesh_zone.iset, boundf_c.source()
        )
        # get array of neighbouring cells index
        boundf_c_arr = boundf_c.as_array()
        boundf_c_arr.resize(len(boundf_c.source()))
        # init with cell values, need to switch from mesh to sites
        sites_rkt_record = Record(mesh_rkt.dtype)
        sites_rkt_record[...] = self.from_location(mesh_rkt)
        # create field with rocktypes of bound faces
        boundf_rkt = icus.field(boundf_c.source(), "i")
        boundf_rkt[...] = mesh_rkt.field.as_array()[boundf_c_arr]
        # fill bc values, need to switch from faces to sites
        sites_rkt_record[...] = self.from_location(Data(boundf_rkt))
        return sites_rkt_record[self.sites]


class VAG(Scheme_def):
    def build(self, mesh, bc_data, frac_data=None):
        # cells where the scheme is used, overload self.cells if init with None
        # self.cells can be empty Zone, in this case wrong to have self.cells = self.cells or Zone(mesh.cells)
        if self.cells is None:
            self.cells = Zone(mesh.cells)
        # init the Dirichlet objects: are the objects
        # initialized in bc_data.Dir which are nodes connecting
        # the vag cells
        scheme_cells_nodes = mesh.cells_nodes.extract(
            self.cells.iset,
            mesh.nodes,
        )
        possible_BC_zone = Zone(scheme_cells_nodes.image())
        # dirichlet mesh objects, add fractures dirichlet nodes after
        dirichlet_mesh_zone = bc_data.Dirichlet_contexts.definition_zone(
            possible_BC_zone
        )
        if frac_data is not None:
            # get the frac faces zone, if len > 0, give to the good scheme
            def_zone = frac_data.definition_zone(Zone(mesh.faces))
            self.frac_faces = frac_data[def_zone] == True
            # add fractures dirichlet nodes
            scheme_frac_nodes = mesh.faces_nodes.extract(
                self.frac_faces.iset,
                mesh.nodes,
            )
            possible_frac_BC_zone = Zone(scheme_frac_nodes.image())
            dirichlet_mesh_zone |= bc_data.Dirichlet_contexts.definition_zone(
                possible_frac_BC_zone
            )
            if self.frac_faces and self.cells:
                # non empty cells zone
                assert False, "frac in a matrix not implemented yet"
            elif self.frac_faces:
                return VAG_scheme(
                    self,
                    make_vag_codim1(mesh, self.frac_faces.iset),
                    dirichlet_mesh_zone,
                )

        assert self.cells, "empty cells zone"
        return VAG_scheme(self, make_vag(mesh, self.cells.iset), dirichlet_mesh_zone)


class VAG_scheme(Scheme):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Darcy_distrib_param = {}
        self.Fourier_distrib_param = {}

    class Distrib_parameters:
        def __init__(
            self,
            label,
            *,
            omega,
            is_volume_n=None,
        ):
            self.sites_label = label
            self.is_volume_nodes = is_volume_n
            self.omega = omega

        @staticmethod
        def is_vol_nodes(nodes, dir_obj, no_volume=Zone()):
            is_volume_nodes = Record("bool")
            is_volume_nodes[...] = True
            is_volume_nodes[dir_obj | no_volume] = False
            return is_volume_nodes[nodes]

        @staticmethod
        def build_cells_labels(scheme, diffusion):
            return Data(scheme.instance.build_cells_labels(diffusion.field))

    # loc_name contains "matrix" or "fractures"
    def Darcy_distribute(self, loc_name, quantities):
        return Data(
            self.instance.distribute_volumes(
                quantities.field,
                # *Darcy_distrib_param)
                self.Darcy_distrib_param[loc_name].sites_label.field,
                self.Darcy_distrib_param[loc_name].is_volume_nodes.field,
                self.Darcy_distrib_param[loc_name].omega,
            )
        )

    # loc_name contains "matrix" or "fractures"
    def Fourier_distribute(self, loc_name, quantities):
        return Data(
            self.instance.distribute_volumes(
                quantities.field,
                # *Fourier_distrib_param)
                self.Fourier_distrib_param[loc_name].sites_label.field,
                self.Fourier_distrib_param[loc_name].is_volume_nodes.field,
                self.Fourier_distrib_param[loc_name].omega,
            )
        )

    # loc_name contains "matrix" or "fractures"
    def init_distribute(
        self,
        loc_name,
        permeability,
        thermal_cond=None,
        *,
        omega_Darcy=0.075,
        omega_Fourier=0.075,
    ):
        assert loc_name == "matrix" or loc_name == "fractures"
        # determine sites_label with the diffusion factor
        # give the same label for cells which have same diffusion values (same max value if tensor),
        # determine the nodes label as the label of the neighbouring cell
        # with the highest diffusion value (drain)
        cells_labels = self.Distrib_parameters.build_cells_labels(self, permeability)
        # build sites label concerns only the cells labels
        # (fractures are not taken into account)
        Darcy_sites_labels = self.build_sites_labels(permeability, cells_labels)
        # build is_volume_nodes, boolean to know if the node gets volume from the cell
        # carried by mesh.nodes, true if not Dirichlet, nor FF atm BC
        if loc_name == "matrix" and self.frac_faces:
            # no volume at fracture nodes
            frac_nodes = self.instance.mesh.faces_nodes.extract(
                self.frac_faces,
                self.instance.mesh.nodes,
            )
            no_volume = Zone(frac_nodes.image())
        else:
            no_volume = Zone()
        m_nodes = self.instance.mesh.nodes
        Darcy_vol_nodes = self.Distrib_parameters.is_vol_nodes(
            m_nodes,
            self.dirichlet_mesh_zone,
            no_volume=no_volume,
        )
        self.Darcy_distrib_param.update(
            {
                loc_name: self.Distrib_parameters(
                    Darcy_sites_labels,
                    omega=omega_Darcy,
                    is_volume_n=Darcy_vol_nodes,
                )
            }
        )
        if thermal_cond is not None:
            cells_labels = self.Distrib_parameters.build_cells_labels(
                self, thermal_cond
            )
            # build sites label concerns only the cells labels
            # (fractures are not taken into account)
            Fourier_sites_labels = self.build_sites_labels(thermal_cond, cells_labels)
            # build is_volume_nodes, boolean to know if the node gets volume from the cell
            # carried by mesh.nodes, true if not Dirichlet, nor FF atm BC
            if loc_name == "matrix" and self.frac_faces is not None:
                # no volume at fracture nodes ???
                no_volume = Zone()
            else:
                no_volume = Zone()
            # todo: distinguish Darcy and Fourier Dirichlet nodes !!!
            Fourier_vol_nodes = self.Distrib_parameters.is_vol_nodes(
                m_nodes,
                self.dirichlet_mesh_zone,
                no_volume=no_volume,
            )
            self.Fourier_distrib_param.update(
                {
                    loc_name: self.Distrib_parameters(
                        Fourier_sites_labels,
                        omega=omega_Fourier,
                        is_volume_n=Fourier_vol_nodes,
                    )
                }
            )

    def build_sites_labels(self, mesh_perm, mesh_rkt):
        # todo : not possible to iterate over connectivities in python,
        # use cpp function, better in python ???
        # Determine the nodes rocktype depending on the neighbouring
        # cells rocktypes and permeabilities
        return Data(
            self.instance.build_sites_labels(
                mesh_perm.field,
                mesh_rkt.field,
            )
        )
