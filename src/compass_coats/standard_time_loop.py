import loaf
import warnings

if loaf.with_petsc:
    from loaf.petsc_solver import PetscSolver, PetscDirectSolver
else:
    from loaf.eigen_solver import EigenSolver
import globalgo
from compass_coats.output_visu import (
    Snapshooter,
    make_sites_dumper,
    OutputDirectoryManager,
)
from compass_coats.output_callbacks import TimeloopLogCallback
from compass_coats.callbacks_utils import LogTimeStepFailure
from physics.physical_state import States_with_contexts
from compass_utils.filenames import output_directory as default_dir


class Standard_time_loop:
    def __init__(
        self,
        *,
        linearized_problem=None,
        scheme=None,
        mesh=None,
        model=None,
        data=None,
        solver=None,
        direct_solver=False,
        fixed_step=None,
        initial_step=1000,
        minimum_step=0.1,
        verbosity=3,
        output_dir=default_dir(),
        newton_res_output=False,
        config=None,
    ):
        """
        Init a standard timeloop.

        Or the linearized problem is given, or it is constructed using
        scheme, mesh, model and data.

        :param fixed_step: Set the fixed time step in seconds, can be used only with FixedStep.
                        fixed_step has the priority over dynamic step
        :param initial_step: Initial time step in seconds, can be used only with DynamicStep.
        """
        if linearized_problem is not None:
            self.linearized_problem = linearized_problem
        else:
            assert scheme is not None, "scheme is missing in standard_time_loop"
            assert mesh is not None, "mesh is missing in standard_time_loop"
            assert model is not None, "model is missing in standard_time_loop"
            assert data is not None, "data is missing in standard_time_loop"
            self.linearized_problem = model.set_up(scheme, mesh, data)

        if direct_solver:
            if solver is not None:
                warnings.warn("Direct solver is ignored, given solver is used")
            else:
                if loaf.with_petsc:
                    # print("PetsC direct solver is used")
                    solver = PetscDirectSolver()
                else:
                    # print("Eigen solver is used")
                    solver = EigenSolver()
        else:
            if solver is None:
                if loaf.with_petsc:
                    solver = PetscSolver()
                else:
                    solver = EigenSolver()

        if fixed_step is not None:
            timestepper = globalgo.timestep.FixedStep(fixed_step)
        else:
            timestepper = globalgo.timestep.DynamicStep(
                initial_step,
                minimum_step=minimum_step,
            )

        # visualization objects
        dumper = make_sites_dumper(
            self.linearized_problem.mesh,
            self.linearized_problem.scheme.sites.iset,
            self.linearized_problem.model,
            output_directory=OutputDirectoryManager(output_dir),
        )
        self.shooter = Snapshooter(dumper)

        if config is None:
            # helps to write the config dictionnaries
            _ = globalgo.config.ConfigHelper()
            self.config = dict(
                newton=_(tolerance=1e-5, maxiter=8),
                linear_solver=solver,
                timesteps=timestepper,
                events=[],  # factory
                callbacks=[_.default(verbosity=verbosity)],
            )
            # if Newton residuals storage if failure timestep
            if newton_res_output:
                # add the additional callback
                # at each time step failure, store a dict with tick, dt
                # and trace, where trace is the residual matrix :
                # (nb_comp + 1) values by sites
                self.config["callbacks"].append(
                    LogTimeStepFailure(
                        # save to files in output-.../residuals directory
                        self.shooter.residuals_shoot,
                        lambda s, r, i, n: r.as_matrix().copy(),
                    ),
                )
        else:
            self.config = config

        self.loop = globalgo.configure_timeloop(self.linearized_problem, self.config)

    def run(
        self,
        *,
        solution=None,
        initial_time=0.0,
        final_time=None,
        fixed_step=None,
        initial_step=None,
        nitermax=None,
        output_period=None,
        nb_output=None,
        output_every=None,
        no_output=False,
        timeloop_file=False,
        events=None,
    ):
        """
        Performs a standard timeloop.

        :param solution: contains context and states
        :param initial_time: Starting time of the simulation. Defaults to 0 if not specified.
        :param final_time: Final time of the simulation (if any).
        :param fixed_step: Set the fixed time step in seconds, can be used only with FixedStep.
                Cannot be specified along with ``initial_step``.
        :param initial_step: Initial time step in seconds, can be used only with DynamicStep.
                (cf. :mod:`ComPASS.timestep_management` module).
                Cannot be specified along with ``fixed_step``.
        :param nitermax: Maximum number of iterations.
        :param output_period: Will dump simulation information every ``output_period`` seconds.
        :param nb_output: Will compute ``output_period`` from ``final_time`` so that there are
            ``nb_output`` dumps of simulation info. This parameter will not have effect if
            ``output_period`` is defined.
        :param output_every: Will dump simulation information every ``output_every`` iterations.
        :param no_output: Flag that will prevent any output (defaults to False)
        :param timeloop_file: Flag to generate time loop files (defaults to False)
                with convergence information.
        :param events:
        :return: The solution and time tick at the end of the time loop.
        """
        initial_tick = globalgo.core.TimeTick(initial_time, 0)
        assert not (
            final_time is None and nitermax is None
        ), "end loop is missing in standard_time_loop"
        final_tick = globalgo.core.TimeTick(final_time, nitermax)
        # only one of the fixed_step and initial_step options
        # can be given
        assert (
            initial_step is None or fixed_step is None
        ), "initial_step or fixed_step missing in standard_time_loop"
        if fixed_step is not None:
            self.loop.timestep_manager = globalgo.timestep.FixedStep(fixed_step)
        elif initial_step is not None:
            # if already DynamicStep, change only the current step
            # (keep other parameters), otherwise create new instance
            if isinstance(self.loop.timestep_manager, globalgo.timestep.DynamicStep):
                self.loop.timestep_manager.step = initial_step
            else:
                self.loop.timestep_manager = globalgo.timestep.DynamicStep(initial_step)

        if solution is None:
            sites = self.linearized_problem.scheme.sites
            # contains states and contexts
            solution = States_with_contexts(
                # states
                self.linearized_problem.sites_data.initial_states[sites].field,
                # contexts
                self.linearized_problem.sites_data.initial_contexts[sites].field,
            )
        # process events
        events = events or globalgo.core.EventPool()
        if output_period is None and nb_output is not None:
            # init output_period with nb_output
            nb_output = max(2, nb_output)
            if final_time is not None:
                output_period = final_time / (nb_output - 1)
            else:
                warnings.warn(
                    "nb_output has no impact because final_time is not set in Standard_time_loop.run."
                )
        # one or the other output ?
        if output_period is not None and not no_output:
            events.add_recurring_time(
                period=output_period,
                action=self.shooter,
            )
        if output_every is not None and not no_output:
            events.add_recurring_iteration(
                period=output_every,
                action=self.shooter,
            )
        # reset if run is called several times
        if no_output | (not timeloop_file):
            self.linearized_problem.__callbacks__.timeloop_log = None
        else:
            main_dir = self.shooter.dumper.outdir_manager.path
            self.linearized_problem.__callbacks__.timeloop_log = TimeloopLogCallback(
                main_dir
            )
        # does not modify solution (self.simulation.coats_solution) !
        return self.loop.run(solution, final_tick, initial_tick, events)
