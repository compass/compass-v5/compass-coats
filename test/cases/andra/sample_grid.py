import numpy as np
from icmesh import regular_mesh
from icmesh.dump import dump_vtu_3d
from icus import field, labels
from icus.fig import Data
import vtkwriters as vtkw


class BigMesh:
    def __init__(self, filepath):
        data = np.load(filepath)
        self.x = data["x"]
        self.y = data["y"]
        self.z = data["z"]
        self.xc = 0.5 * (self.x[:-1] + self.x[1:])
        self.yc = 0.5 * (self.y[:-1] + self.y[1:])
        self.zc = 0.5 * (self.z[:-1] + self.z[1:])
        self.nx = self.xc.shape[0]
        self.ny = self.yc.shape[0]
        self.nz = self.zc.shape[0]
        self.domain = data["domain"]

    def nearest_domain(self, P):
        x, y, z = P
        i = np.abs(self.xc - x).argmin()
        j = np.abs(self.yc - y).argmin()
        k = np.abs(self.zc - z).argmin()
        return self.domain[i, j, k]

    def dump(self, filename):
        vtkw.write_vtr(
            vtkw.vtr_doc(
                (self.x, self.y, self.z),
                celldata={
                    "domain": self.domain.ravel(order="F"),
                },
            ),
            filename,
        )


def load_small_mesh(dump_meshes=False):
    bigmesh = BigMesh("mesh/small_cigeo.npz")

    Ox, Oy, Oz = (714, 394, 75)
    dx, dy, dz = steps = (3.0, 1.5, 1.5)
    nx, ny, nz = shape = (10, 20, 20)
    origin = (Ox - nx * dx, Oy - 0.5 * ny * dy, Oz - 0.5 * dz * nz)
    mesh = regular_mesh(shape, steps=steps, origin=origin)
    centers = mesh.cell_isobarycenters.as_array()

    rocktypes = labels(mesh.cells)
    for i, C in enumerate(centers):
        rocktypes[i] = bigmesh.nearest_domain(C)

    if dump_meshes:
        print(f"Mesh contains {centers.shape[0]} cells")
        bigmesh.dump("bigmesh")
        dump_vtu_3d(
            mesh, "small-domain.vtu", celldata={"rocktype": rocktypes.as_array()}
        )

    return mesh, Data(rocktypes)


if __name__ == "__main__":
    load_small_mesh(dump_meshes=True)
