#%% Documentation : https://compass.gitlab.io/compass-v5/training/
#%% Mesh is in https://brgm365.sharepoint.com/:u:/r/sites/DGRANDRACOMPASS2021-Formationdc2023/Documents%20partages/scripts/small_cigeo/mesh/small_cigeo.npz?csf=1&web=1&e=BRsmhh
#%% Import the ComPASS library and some utilities
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from compass_utils.filenames import output_directory
from icmesh import top_boundary, bottom_boundary
from compass_utils.units import *
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Zone
from sample_grid import load_small_mesh
from utils import (
    set_van_Genuchten_kr_pc,
    get_nodes,
)

#%% Choose the numerical scheme (TPFA or VAG)
scheme_def = TPFA()

#%% Create the geometry and the cells rocktypes
mesh, cell_rkt = load_small_mesh()

#%% Load the diphasic physics : water/air component in liquid/gas phase
model = Coats("diphasic")
# change some physical properties (kr and pc)
set_van_Genuchten_kr_pc(model)

#%% Initialize the simulation data
data = model.new_data(mesh.dimension)
# init with cells rocktypes values
data.rocktypes[...] = cell_rkt
gallery_rkt = 4
gallery_cells = data.rocktypes[Zone(mesh.cells)] == gallery_rkt

data.matrix.petrophysics.porosity[...] = 0.15
data.matrix.petrophysics.permeability[...] = 1e-20  # m^2
data.matrix.petrophysics.permeability[gallery_cells] = 5e-18  # m^2
data.matrix.petrophysics.thermal_conductivity[...] = 2.0  # W/m/K

#%% Initialize all states with reference values
ptop = 6.0 * MPa
pgallery = 1 * bar
T = 20 * degC
ctx0, X0 = model.utils.equilibriate_state(context="liquid", T=T, p=ptop)
data.initial_contexts[...] = ctx0
data.initial_states[...] = X0

# modify gallery state (identified by its rocktype)
gal_ctx, Xgal = model.utils.equilibriate_state(
    context="diphasic",
    p=pgallery,  # gaz pressure
    T=T,
    Sg=0.5,
    # rkt is necessary to know which pc to equilibrate the pressures
    rocktype=gallery_rkt,
)
data.initial_states[gallery_cells] = Xgal
data.initial_contexts[gallery_cells] = gal_ctx

#%% To use VAG, you must also modify the nodes values of gallery
gallery_nodes = get_nodes(mesh, gallery_cells)
data.initial_states[gallery_nodes] = Xgal
data.initial_contexts[gallery_nodes] = gal_ctx

#%% Set Dirichlet Boundary condition
top = top_boundary(mesh)  # get top mesh objects (faces, edges, nodes)
bottom = bottom_boundary(mesh)  # get bottom mesh objects (faces, edges, nodes)
data.boundary_conditions.Dirichlet_contexts[top | bottom] = ctx0
data.boundary_conditions.Dirichlet_states[top] = X0
# gravity is a vector, by default [0, 0, 9.81]
rho_g = 1000 * model.gravity[-1]
Xbottom = X0
z_bottom = mesh.origin[-1]
z_top = z_bottom + mesh.extent[-1]
Xbottom.pressure[:] = ptop + rho_g * (z_top - z_bottom)
data.boundary_conditions.Dirichlet_states[bottom] = Xbottom

#%% Add heat source, will be multiplied by the cell measure
data.cell_heat_source[gallery_cells] = 1.0  # in W/m^3 = J/m^3/s

#%% Create the standard time loop
visu_dir = output_directory(__file__)
time_loop = Standard_time_loop(
    mesh=mesh,
    model=model,
    scheme=scheme_def,
    data=data,
    output_dir=visu_dir,
)
#%% Change some Newton and time stepper coefficients
time_loop.loop.timestepper.step_solver.maxiter = 20
time_loop.loop.timestep_manager.increase_factor = 5

#%% Run the standard time loop
dt = 30 * day
final_time = 50 * year
snapshot_period = final_time / 20
solution, tick = time_loop.run(
    initial_step=dt,
    final_time=final_time,
    output_period=snapshot_period,
)

#%% Some postprocesses, it allows to visualize with Paraview
postprocess(visu_dir, time_unit="day")
