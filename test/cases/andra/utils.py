import numpy as np
from physicalprop.set_van_Genuchten_rock_properties import (
    add_van_Genuchten_rock_properties,
)
from icus.fig import Zone, Data, Record
from icmesh import (
    top_boundary,
    bottom_boundary,
)


def set_van_Genuchten_kr_pc(model):
    # Cigeo argilite (natural medium)
    add_van_Genuchten_rock_properties(model.properties.rock, 7, 1.5e7, 0, 0, 1.5)
    # Cigeo EDZ
    add_van_Genuchten_rock_properties(model.properties.rock, 6, 1.5e6, 0, 0, 1.5)
    # Cigeo Bentonite
    add_van_Genuchten_rock_properties(model.properties.rock, 5, 1.6e7, 0, 0, 1.6)
    # Cigeo backfill in access drifts
    add_van_Genuchten_rock_properties(model.properties.rock, 4, 2.0e6, 0, 0, 1.5)
    # jeux
    add_van_Genuchten_rock_properties(model.properties.rock, 3, 2.0e6, 0, 0, 1.5)
    add_van_Genuchten_rock_properties(model.properties.rock, 2, 2.0e6, 0.01, 0, 1.54)
    add_van_Genuchten_rock_properties(model.properties.rock, 1, 15.0e6, 0.4, 0, 1.49)


def get_nodes(mesh, cells):
    neighbouring_nodes = mesh.cells_nodes.extract(cells.iset, mesh.nodes)
    nodes_list = np.unique(neighbouring_nodes.as_array())
    return Zone(mesh.nodes.extract(nodes_list))
