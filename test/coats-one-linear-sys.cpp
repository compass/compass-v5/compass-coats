#include <catch2/catch_test_macros.hpp>
#include <compass_coats/closure_laws_with_schur.h>
#include <compass_coats/compass-coats.h>
#include <icmesh/Connected_components.h>
#include <icmesh/Geomtraits.h>
#include <icmesh/Read_fvca6.h>
#include <loaf/factories.h>
#include <physicalprop/physicalprop.h>
#include <physics/model.h>
#include <scheme_lfv/tpfa.h>
#include <scheme_lfv/vag.h>
#include <string>

// creates a 3D grid with, at each direction: N segments and N+1 nodes from 0
// to 1
static auto build_mesh(icus::index_type N) {
  // Constant_axis_offset{origin, Nb_elements, dx}
  auto step = icmesh::Constant_axis_offset{0., N, 1. / N};
  return icmesh::mesh(icmesh::lattice(step, step, step));
}

template <std::size_t elem_i>
static auto get_close_to(auto &mesh, auto dir, auto x, double eps = 1.e-6) {
  auto centers = mesh.template get_isobarycenters<elem_i>();
  std::vector<icus::index_type> top_indices;
  for (auto &&[i, v] : centers.enumerate())
    if (abs(v[dir] - x) < eps)
      top_indices.push_back(i);

  return mesh.elements[elem_i].extract(top_indices);
}

static void assert_hydrostatic_pressure(auto &mesh, auto sites, auto states,
                                        auto p0, const auto &gravity,
                                        double eps = 1.e-10) {
  auto sites_position = mesh.isobarycenters(sites.location);
  auto rho = 1000; // TODO should be obtained from fluid properties instead
  CHECK(mesh.dimension == 3);
  std::array<double, mesh.dimension> top{1.0, 1.0, 1.0};
  for (auto &&[pos, state] : std::views::zip(sites_position, states)) {
    auto depth = top - pos;
    // dot(depth, g)
    auto g_z = 0.;
    for (std::size_t i = 0; i < mesh.dimension; ++i)
      g_z += depth[i] * gravity[i];
    auto anal_p = p0 + rho * g_z;
    CHECK(std::abs(anal_p - state.pressure[0]) / anal_p < eps);
    CHECK(std::abs(1. - state.saturation[physics::Phase::liquid]) < eps);
    CHECK(std::abs(state.saturation[physics::Phase::gas]) < eps);
  }
}

static auto init_states_contexts(auto sites, auto p0, auto T0) {
  auto states = physics::State_field(sites);
  physics::State s0{};
  s0.temperature = T0;
  s0.pressure.fill(p0);
  s0.saturation[physics::Phase::gas] = 0.;
  s0.saturation[physics::Phase::liquid] = 1.;
  s0.molar_fractions[physics::Phase::liquid].fill(0.);
  s0.molar_fractions[physics::Phase::gas].fill(0.);
  s0.molar_fractions[physics::Phase::liquid][physics::Component::water] = 1.;
  s0.molar_fractions[physics::Phase::gas][physics::Component::air] = 1.;
  states.fill(s0);
  auto contexts = physics::Context_field(sites);
  contexts.fill(physics::Context::liquid);

  return std::make_tuple(states, contexts);
}

static auto is_vol_nodes(auto nodes, auto dirichlet) {
  auto is_volume_nodes = make_field(nodes, true);
  is_volume_nodes.view(dirichlet).fill(false);
  return is_volume_nodes;
}

template <std::size_t mesh_dim = 3>
static auto build_diff_tensor(auto cells, auto perm_val, auto thermcond_val) {
  using Tensor = numscheme::Tensor_d<mesh_dim>;
  using Tensor_field = numscheme::Tensor_field<mesh_dim>;
  // careful, carried by the cells, Tensor
  auto permeability = Tensor_field(cells);
  auto perm = Tensor({0.0});
  for (std::size_t i = 0; i < mesh_dim; ++i)
    perm(i, i) = perm_val;
  permeability.fill(perm);
  auto thermal_conductivity = Tensor_field(cells);
  auto th_cond = Tensor({0.0});
  for (std::size_t i = 0; i < mesh_dim; ++i)
    th_cond(i, i) = thermcond_val; // W/m/K
  thermal_conductivity.fill(th_cond);

  return std::make_tuple(permeability, thermal_conductivity);
}

static auto build_assembler(auto num_scheme, auto rkt, auto permeability,
                            auto thermal_conductivity, auto poro_darcy_vol,
                            auto poro_fourier_vol, auto rock_fourier_vol,
                            const auto &gravity, auto dirichlet) {

  // init Site_id_field
  auto sites_id = Site_id_field(num_scheme.sites);
  Site_id int_id({BC::interior, BC::interior});
  sites_id.fill(int_id);
  // modify the Dirichlet sites
  Site_id dir_id({BC::dirichlet, BC::dirichlet});
  auto dir_sites = num_scheme.sites.from_location(dirichlet);
  sites_id.view(dir_sites).fill(dir_id);

  // init the model ie the physical properties
  auto fluid_properties_wd = physical_prop::FMP_d();
  auto fluid_properties = physical_prop::FMP();
  auto rock_properties_wd = physical_prop::RP_d();
  auto rock_properties = physical_prop::RP();

  // init closure_laws
  auto closure_laws = coats::Closure_laws_with_Schur(
      num_scheme.sites, rkt, fluid_properties_wd, fluid_properties,
      rock_properties_wd, rock_properties);

  // init Darcy and Fourier Transmissivities
  auto Darcy_trans = num_scheme.build_transmissivities(permeability);
  auto Fourier_trans = num_scheme.build_transmissivities(thermal_conductivity);

  // init null volumetric and surfacic sources
  auto vol_sources = physics::Acc_field(num_scheme.sites);
  auto neum_sources = physics::Acc_field(num_scheme.sites);
  physics::Accumulation vol0{}, neum0{};
  vol0.energy = 0.0;
  vol0.molar.fill(0.0);
  neum0.energy = 0.0;
  neum0.molar.fill(0.0);
  vol_sources.fill(vol0);
  neum_sources.fill(neum0);

  return init_assembler(num_scheme, fluid_properties_wd, rock_properties_wd,
                        closure_laws, rkt, poro_darcy_vol, poro_fourier_vol,
                        rock_fourier_vol, Darcy_trans, Fourier_trans, gravity,
                        dirichlet, neum_sources, vol_sources, sites_id);
}

static auto fill_linear_system(auto &assembler, auto &states, auto &contexts,
                               auto &Jac, auto &RHS, auto &residual) {
  // compute accumulation
  assembler.residu_reset_history(states, contexts);
  // Evaluate residual and jacobian
  auto dt = 1e5;
  assembler.balance_equations_residual(dt, residual, states, contexts);
  assembler.discretize_balance_equations(dt, Jac, RHS, residual, states,
                                         contexts);
}

auto eigen_solve(auto &Jac, auto &RHS) {
  loaf::Solver eigen_solver{};
  eigen_solver.compute(Jac);
  auto &&[success, message] = eigen_solver.info();
  if (!success)
    throw std::runtime_error(message);
  return eigen_solver.solve(RHS);
}

template <std::size_t scheme_dim>
// vag_cells is the set of objet where the vag scheme is applied,
// if VAG2D in a 3D scheme, vag_cells is a set of faces
static auto vag_linear_sys(auto &mesh, auto vag_cells, auto permeability,
                           auto thermal_conductivity) {
  // init the numerical scheme
  auto num_scheme =
      numscheme::VAG<std::decay_t<decltype(mesh)>, scheme_dim>(mesh, vag_cells);
  // Dirichlet : top nodes (VAG)
  auto top_nodes = get_close_to<mesh.node>(mesh, mesh.dimension - 1, 1.e0);
  auto dirichlet =
      icus::intersect(top_nodes, num_scheme.build_info._vag_nodes, mesh.nodes);

  // init the rocktypes
  auto rkt = physics::Rocktype_field(num_scheme.sites);
  rkt.fill(0);
  // init the petrophysics
  // careful, carried by the cells
  auto porosity = icus::Field(vag_cells);
  porosity.fill(0.15);

  // init the states and contexts
  auto p0 = 1.e8;
  auto T0 = 300.;
  auto &&[states, contexts] = init_states_contexts(num_scheme.sites, p0, T0);

  // init porous Darcy/Fourier volumes
  auto cell_volumes = mesh.measures(vag_cells);
  auto porous_vol = icus::Field(vag_cells);
  auto porous_1vol = icus::Field(vag_cells);
  for (auto &&[poro, vol, poro_vol, poro_1vol] :
       std::views::zip(porosity, cell_volumes, porous_vol, porous_1vol)) {
    poro_vol = poro * vol;
    poro_1vol = (1.0 - poro) * vol;
  }
  auto sites_label = rkt;
  auto is_volume_nodes = is_vol_nodes(mesh.nodes, dirichlet);
  auto omega = 0.075;
  auto poro_darcy_vol = num_scheme.distribute_volumes(porous_vol, sites_label,
                                                      is_volume_nodes, omega);
  auto poro_fourier_vol = num_scheme.distribute_volumes(porous_vol, sites_label,
                                                        is_volume_nodes, omega);
  auto rock_fourier_vol = num_scheme.distribute_volumes(
      porous_1vol, sites_label, is_volume_nodes, omega);

  auto gravity = std::array<double, 3>({0.0, 0.0, 9.81});

  auto assembler = build_assembler(
      num_scheme, rkt, permeability, thermal_conductivity, poro_darcy_vol,
      poro_fourier_vol, rock_fourier_vol, gravity, dirichlet);

  // create linear system objects
  auto [Jac, RHS] =
      loaf::make_block_linear_system<physics::nb_dof>(num_scheme.sites);
  // RHS is different from residual due to Schur elimination
  auto residual = loaf::OnePhysicsVector<physics::nb_dof>(num_scheme.sites);

  fill_linear_system(assembler, states, contexts, Jac, RHS, residual);

  auto dx = eigen_solve(Jac, RHS);
  assembler.update_states(states, contexts, dx);
  // test the pressure and saturation values
  assert_hydrostatic_pressure(mesh, num_scheme.sites, states, p0, gravity);
}

static auto tpfa_linear_sys(auto &mesh, auto permeability,
                            auto thermal_conductivity) {
  // Dirichlet : top faces (TPFA)
  auto dirichlet = get_close_to<mesh.face>(mesh, mesh.dimension - 1, 1.e0);
  // init the numerical scheme
  auto num_scheme = numscheme::TPFA(mesh, mesh.cells, dirichlet);

  // init the rocktypes
  auto rkt = physics::Rocktype_field(num_scheme.sites);
  rkt.fill(0);
  // init the petrophysics
  // careful, carried by the cells
  auto porosity = icus::Field(mesh.cells);
  porosity.fill(0.15);

  // init the states and contexts
  auto p0 = 1.e8;
  auto T0 = 300.;
  auto &&[states, contexts] = init_states_contexts(num_scheme.sites, p0, T0);

  // init porous Darcy/Fourier volumes
  auto cell_volumes = mesh.get_cell_measures();
  auto porous_vol = icus::Field(mesh.cells);
  auto porous_1vol = icus::Field(mesh.cells);
  for (auto &&[poro, vol, poro_vol, poro_1vol] :
       std::views::zip(porosity, cell_volumes, porous_vol, porous_1vol)) {
    poro_vol = poro * vol;
    poro_1vol = (1.0 - poro) * vol;
  }
  auto poro_darcy_vol = num_scheme.distribute_volumes(porous_vol);
  auto poro_fourier_vol = num_scheme.distribute_volumes(porous_vol);
  auto rock_fourier_vol = num_scheme.distribute_volumes(porous_1vol);

  auto gravity = std::array<double, 3>({0.0, 0.0, 9.81});

  auto assembler = build_assembler(
      num_scheme, rkt, permeability, thermal_conductivity, poro_darcy_vol,
      poro_fourier_vol, rock_fourier_vol, gravity, dirichlet);

  // create linear system objects
  auto [Jac, RHS] =
      loaf::make_block_linear_system<physics::nb_dof>(num_scheme.sites);
  // RHS is different from residual due to Schur elimination
  auto residual = loaf::OnePhysicsVector<physics::nb_dof>(num_scheme.sites);

  fill_linear_system(assembler, states, contexts, Jac, RHS, residual);

  auto dx = eigen_solve(Jac, RHS);
  assembler.update_states(states, contexts, dx);
  // test the pressure and saturation values
  assert_hydrostatic_pressure(mesh, num_scheme.sites, states, p0, gravity);
}

TEST_CASE("one coats linear system") {
  // creates a 3D grid with, at each direction: N segments and N+1 nodes from 0
  // to 1
  auto &&mesh = build_mesh(6);
  auto middle_faces = get_close_to<mesh.face>(mesh, 0, 5.0e-1);
  // Gamma = middle faces in the x direction
  auto c_comp1 = icmesh::Connected_components(mesh, middle_faces);
  auto &&[permeability, thermal_conductivity] =
      build_diff_tensor(mesh.cells, 1.e-11, 2.5);

  // SECTION("VAG and TPFA 3D cartesian mesh") {
  tpfa_linear_sys(mesh, permeability, thermal_conductivity);

  // VAG scheme applied on the whole 3D mesh
  vag_linear_sys<3>(mesh, mesh.cells, permeability, thermal_conductivity);
  // decompose the mesh into connected components wrt different set of faces
  // (Gamma) and apply VAG on the 3D mesh. Gamma = faces such that x = 0
  auto x0_faces = get_close_to<mesh.face>(mesh, 0, 0);
  auto c_comp0 = icmesh::Connected_components(mesh, x0_faces);
  auto &&c_mesh0 = c_comp0.mesh;
  auto &&[c0_perm, c0_th_cond] = build_diff_tensor(c_mesh0.cells, 1.e-13, 3.5);
  vag_linear_sys<3>(c_mesh0, c_mesh0.cells, c0_perm, c0_th_cond);
  // SECTION("VAG and TPFA 3D connected component mesh") {
  auto &&c_mesh1 = c_comp1.mesh;
  auto &&[c1_perm, c1_th_cond] = build_diff_tensor(c_mesh1.cells, 1.e-0, 1.5);
  vag_linear_sys<3>(c_mesh1, c_mesh1.cells, c1_perm, c1_th_cond);
  tpfa_linear_sys(mesh, permeability, thermal_conductivity);

  // SECTION("VAG2D in a 3D mesh") {
  // VAG scheme applied on a selection of faces (2D scheme in a 3D mesh)
  // only scalar diffusion coef is possible in VAG 2D
  auto scalar_perm = numscheme::Scalar_field(middle_faces);
  scalar_perm.fill(1.e-11);
  auto &&scalar_thermcond = numscheme::Scalar_field(middle_faces);
  scalar_thermcond.fill(2.5);
  vag_linear_sys<2>(mesh, middle_faces, scalar_perm, scalar_thermcond);
  // applied VAG2D over gamma_mesh from the connected component
  // (same set of faces than middle_faces, other mesh object)
  auto gamma = c_comp1.gamma;
  auto g_perm = numscheme::Scalar_field(gamma);
  g_perm.fill(1.e-11);
  auto &&g_thermcond = numscheme::Scalar_field(gamma);
  g_thermcond.fill(2.5);
  vag_linear_sys<2>(c_mesh1, gamma, g_perm, g_thermcond);

  // SECTION("VAG and TPFA 3D voronoi mesh") {
  // non cartesian mesh
  std::string file{__FILE__};
  auto &&directory = file.substr(0, file.rfind("/"));
  auto &&voro_file = directory + "/mesh_data/vmesh_1.msh";
  auto &&vor_mesh = icmesh::read_fvca6(voro_file);
  auto &&[vor_perm, vor_thcond] =
      build_diff_tensor(vor_mesh.cells, 1.e-11, 2.5);
  tpfa_linear_sys(vor_mesh, vor_perm, vor_thcond);
  vag_linear_sys<3>(vor_mesh, vor_mesh.cells, vor_perm, vor_thcond);
}
