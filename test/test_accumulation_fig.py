from icus.fig import Zone, Data
from compass_coats.models import Coats
import numpy as np


def test_accumulation_fig():
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    sites = Zone.from_size(20)

    Data_acc = Data.from_zone(sites, dtype=model.physics.Accumulation)
    null_acc = Data_acc.dtype()
    null_acc.energy = 0.0
    null_acc.molar.fill(0.0)
    Data_acc[...] = null_acc
    assert np.all(np.abs(Data_acc.molar.field.as_array()) < 1e-9)
    other = Data.from_zone(sites, dtype=Data_acc.dtype)
