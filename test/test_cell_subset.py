import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import sys
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import (
    regular_mesh,
    top_boundary,
)
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data, Zone  # remove Zone


@pytest.mark.parametrize("numerical_scheme", ["TPFA", "VAG"])
def test_separated_subsets_fast(numerical_scheme):
    test_separated_subsets(numerical_scheme, nitermax=1)


@pytest.mark.parametrize("numerical_scheme", ["TPFA", "VAG"])
@pytest.mark.slow
def test_separated_subsets(numerical_scheme, nitermax=None):
    # init the mesh and the geometry utilities
    Nx = 20
    Ny = 1
    Nz = 30  # number of cells
    Lx = 100.0
    Ly = 4.0
    Lz = 200.0  # domain lengths (in m)
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # define the subset of cells where to solve
    cell_centers = Data(mesh.cell_isobarycenters)
    half_top_cells = cell_centers[:, -1] > Lz / 2.0

    # definition of the scheme over the subset of cells
    if numerical_scheme == "TPFA":
        scheme_def = TPFA(cells=half_top_cells)
    elif numerical_scheme == "VAG":
        scheme_def = VAG(cells=half_top_cells)
    else:
        raise "num scheme not recognized"

    # init petrophysics and rocktypes
    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # cells values

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
    )
    # if necessary, adapt the Newton or timestepper coefficients
    time_loop.loop.timestepper.step_solver.maxiter = 25
    time_loop.loop.timestep_manager.increase_factor = 3

    solution, tick = time_loop.run(
        initial_step=1e4,
        final_time=500 * day,
        output_every=3,
        nitermax=nitermax,
    )
    # postprocess to prepare for visualization
    # error in visu, all the mesh is plotted, but some values are missing
    # because the unknowns are not computed everywhere
    postprocess(time_unit="day")


if __name__ == "__main__":
    # by default, all num schemes
    num_scheme = None
    # read num scheme from command line, otherwise both num schemes are used
    if len(sys.argv) > 1:
        num_scheme = sys.argv[1]

    test_separated_subsets(num_scheme)
