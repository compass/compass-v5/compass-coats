import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
import sys
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from compass_utils.filenames import output_directory
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary,
    bottom_boundary,
)
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def solve_Darcy(
    num_scheme,
    model,
    mesh,
    Schur_elimination,
    direct_solver=False,
    nitermax=None,
):
    if num_scheme == "TPFA":
        scheme_def = TPFA(schur_elimination=Schur_elimination)
    elif num_scheme == "VAG":
        scheme_def = VAG(schur_elimination=Schur_elimination)
    else:
        raise "num scheme not recognized"

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # already default value

    # initial state and BC
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",  # should be removed
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",  # should be removed
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    # bottom = bottom_boundary(mesh)

    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    linearized_problem = model.set_up(scheme_def, mesh, data)
    visu_dir = output_directory(__file__)
    time_loop = Standard_time_loop(
        linearized_problem=linearized_problem,
        output_dir=visu_dir,
        verbosity=3,
        direct_solver=direct_solver,  # à comprendre
    )
    # if necessary, adapt the Newton or timestepper coefficients
    # time_loop.loop.timestepper.step_solver.tolerance = 1e-9
    # time_loop.loop.timestepper.step_solver.maxiter = 25
    # time_loop.loop.timestep_manager.increase_factor = 1.5

    dt = 15 * day

    final_time = 50 * day
    snapshot_period = final_time / 3
    solution, tick = time_loop.run(
        initial_step=dt,
        # fixed_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
        # output_every=10,
        nitermax=nitermax,
    )
    postprocess(visu_dir, time_unit="day")


def build_regular_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))


@pytest.mark.parametrize(
    "num_scheme,mesh_builder",
    [
        ("TPFA", build_regular_mesh),
        ("TPFA", build_3D_scottish_mesh),
        ("VAG", build_regular_mesh),
        ("VAG", build_3D_scottish_mesh),
    ],
)
def test_coats_energy_fast(num_scheme, mesh_builder):
    Nx = 1
    Ny = 1
    Nz = 100
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    mesh = mesh_builder(Nx, Ny, Nz, Lx, Ly, Lz)
    solve_Darcy(num_scheme, model, mesh, True, nitermax=1)


@pytest.mark.slow
def test_coats_energy(numerical_scheme=None):

    Nx = 1
    Ny = 1
    Nz = 100
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)

    num_schemes = numerical_scheme or ["TPFA", "VAG"]
    for num_scheme in num_schemes:
        assert num_scheme in ["TPFA", "VAG"]

        print("-----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D regular grid")
        mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        Schur_elimination = False
        # the default iterative petsc solver (and cpramg precond) does not converge
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"--------------------{num_scheme} WITH SCHUR ELIMINATION---------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)
        print("----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D scottish grid")
        mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        Schur_elimination = False
        # the default iterative petsc solver (and cpramg precond) does not converge
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"--------------------{num_scheme} WITH SCHUR ELIMINATION---------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)

    print("end of test")


if __name__ == "__main__":
    # by default, all num schemes
    num_scheme = None
    # read num scheme from command line, otherwise both num schemes are used
    if len(sys.argv) > 1:
        num_scheme = [sys.argv[1]]

    test_coats_energy(numerical_scheme=num_scheme)
