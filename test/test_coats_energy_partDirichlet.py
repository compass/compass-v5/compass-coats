import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
import sys
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import (
    regular_mesh,
    top_boundary,
)
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Zone, Data


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def solve_Darcy(
    num_scheme,
    model,
    mesh,
    x_dir,
    direct_solver=False,
    nitermax=None,
):
    if num_scheme == "TPFA":
        scheme_def = TPFA()
    elif num_scheme == "VAG":
        scheme_def = VAG()
    else:
        raise "num scheme not recognized"

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # already default value

    # initial state and BC
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",  # should be removed
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",  # should be removed
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use set of faces for TPFA
    # and set of nodes for VAG
    union_faces_nodes = Zone() | mesh.faces | mesh.nodes
    fn_pos = Data(mesh.isobarycenters(union_faces_nodes.iset))

    # Identify the sites (faces and nodes) on the ztop boundary
    # and such that x > x_dir
    dir_sites = top_boundary(mesh) & (fn_pos[:, 0] > x_dir)
    assert len(dir_sites) > 0

    data.boundary_conditions.Dirichlet_states[dir_sites] = Xtop
    data.boundary_conditions.Dirichlet_contexts[dir_sites] = top_ctx

    linearized_problem = model.set_up(scheme_def, mesh, data)
    time_loop = Standard_time_loop(
        linearized_problem=linearized_problem,
        verbosity=3,
        direct_solver=direct_solver,  # à comprendre
    )
    # if necessary, adapt the Newton or timestepper coefficients
    # time_loop.loop.timestepper.step_solver.tolerance = 1e-9
    # time_loop.loop.timestepper.step_solver.maxiter = 25
    # time_loop.loop.timestep_manager.increase_factor = 1.5

    dt = 15 * day

    final_time = 50 * day
    snapshot_period = final_time / 3
    solution, tick = time_loop.run(
        initial_step=dt,
        # fixed_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
        # output_every=10,
        nitermax=nitermax,
    )
    postprocess(time_unit="day")


def build_regular_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))


@pytest.mark.parametrize(
    "num_scheme",
    [
        "TPFA",
        "VAG",
    ],
)
def test_coats_energy_fast(num_scheme):
    Nx = 1
    Ny = 1
    Nz = 100
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    mesh = build_regular_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
    solve_Darcy(num_scheme, model, mesh, Lx / 2.0, True, nitermax=1)


@pytest.mark.slow
def test_coats_energy(numerical_scheme=None):

    Nx = 8
    Ny = 1
    Nz = 20
    Lx = 10.0
    Ly = 1.0
    Lz = 15.0

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)

    num_schemes = numerical_scheme or ["TPFA", "VAG"]
    for num_scheme in num_schemes:
        assert num_scheme in ["TPFA", "VAG"]

        print("-----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D regular grid")
        mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
        solve_Darcy(num_scheme, model, mesh, Lx / 2.0)

    print("end of test")


if __name__ == "__main__":
    # by default, all num schemes
    num_scheme = None
    # read num scheme from command line, otherwise both num schemes are used
    if len(sys.argv) > 1:
        num_scheme = [sys.argv[1]]

    test_coats_energy(numerical_scheme=num_scheme)
