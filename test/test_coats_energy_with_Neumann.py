import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
import sys
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from compass_utils.filenames import output_directory
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary,
    bottom_boundary_faces,
)
from compass_utils.units import bar, degC, day, year
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def solve_Darcy(
    num_scheme,
    model,
    mesh,
    Schur_elimination,
    direct_solver=False,
    nitermax=None,
):
    if num_scheme == "TPFA":
        scheme_def = TPFA(schur_elimination=Schur_elimination)
    elif num_scheme == "VAG":
        scheme_def = VAG(schur_elimination=Schur_elimination)
    else:
        raise "num scheme not recognized"

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0

    # initial state and BC
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",  # should be removed
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",  # should be removed
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    # set Dirichlet values
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    # impose non homogeneous Neumann at the bottom faces
    bottom_faces = bottom_boundary_faces(mesh)
    qmol = 1e-3
    hbottom = 1e5  # estimate of liquid_molar_enthalpy, should be computed
    # init Neumann flux, always imposed at faces
    # Neumann flux is surface flux, will be multiplied by faces measures
    Neumann_flux = data.boundary_conditions.Neumann_flux.dtype()
    Neumann_flux.energy = qmol * hbottom  # in W/m^2 = J/m^2/s
    Neumann_flux.molar[model.components["air"]] = 0  # in mol/m^2/s
    Neumann_flux.molar[model.components["water"]] = qmol  # in mol/m^2/s
    # init all the bottom faces with the same Neumann flux
    data.boundary_conditions.Neumann_flux[bottom_faces] = Neumann_flux

    visu_dir = output_directory(__file__)
    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        output_dir=visu_dir,
        direct_solver=direct_solver,  # à comprendre
    )

    dt = 1 * day
    final_time = 50 * day
    snapshot_period = final_time / 5

    solution, tick = time_loop.run(
        initial_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
        nitermax=nitermax,
    )

    # use now FixedStep, WARNING:
    # the steps can be smaller due to events or to stop exactly at final time

    # first implementation :
    #        time_loop.loop.timestep_manager = FixedStep(6 * day)
    # or in run using fixed_step

    # increase the max number of Newton iterations
    # because cannot reduce the time step if convergence failure
    time_loop.loop.timestepper.step_solver.maxiter = 20
    final_time += 0.1 * year
    snapshot_period = 0.05 * year
    last_solution, last_tick = time_loop.run(
        solution=solution,
        initial_time=tick.time,
        final_time=final_time,
        fixed_step=6 * day,
        output_period=snapshot_period,
        output_every=3,
        nitermax=nitermax,
    )

    postprocess(visu_dir, time_unit="day")


def build_regular_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))


@pytest.mark.parametrize(
    "num_scheme,mesh_builder",
    [
        ("TPFA", build_regular_mesh),
        ("TPFA", build_3D_scottish_mesh),
        ("VAG", build_regular_mesh),
        ("VAG", build_3D_scottish_mesh),
    ],
)
def test_coats_energy_fast(num_scheme, mesh_builder):
    Nx = 2
    Ny = 2
    Nz = 10
    Lx = 3.0
    Ly = 4.0
    Lz = 20.0
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    solve_Darcy(num_scheme, model, mesh, True, nitermax=1)


@pytest.mark.slow
def test_coats_energy(numerical_scheme=None):

    Nx = 2
    Ny = 2
    Nz = 10
    Lx = 3.0
    Ly = 4.0
    Lz = 20.0

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)

    num_schemes = numerical_scheme or ["TPFA", "VAG"]
    for num_scheme in num_schemes:
        assert num_scheme in ["TPFA", "VAG"]

        print("-----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D regular grid")
        mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        Schur_elimination = False
        # the default iterative petsc solver (and cpramg precond) does not converge
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"--------------------{num_scheme} WITH SCHUR ELIMINATION---------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)
        print("----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D scottish grid")
        mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        Schur_elimination = False
        # the default iterative petsc solver (and cpramg precond) does not converge
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"--------------------{num_scheme} WITH SCHUR ELIMINATION---------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)

    print("end of test")


if __name__ == "__main__":
    # by default, all num schemes
    num_scheme = None
    # read num scheme from command line, otherwise both num schemes are used
    if len(sys.argv) > 1:
        num_scheme = [sys.argv[1]]

    test_coats_energy(numerical_scheme=num_scheme)
