import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
import sys
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary,
    bottom_boundary,
)
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def solve_Darcy(
    num_scheme,
    model,
    mesh,
    Schur_elimination,
    direct_solver=False,
    nitermax=None,
):
    if num_scheme == "TPFA":
        scheme_def = TPFA(schur_elimination=Schur_elimination)
    elif num_scheme == "VAG":
        scheme_def = VAG(schur_elimination=Schur_elimination)
    else:
        raise "num scheme not recognized"

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0
    data.matrix.rocktypes[...] = 0

    # initial state and BC
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",  # should be removed
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    bot_ctx, Xbottom = model.utils.equilibriate_state(
        context="liquid",  # should be removed
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with bottom values
    data.initial_states[...] = Xbottom
    data.initial_contexts[...] = bot_ctx
    # Dirichlet is applied at top and bottom
    # get all mesh obj at the top and bottom, no distinction between VAG and TPFA
    # to optimize, you can use *_boundary_faces for TPFA
    # and *_boundary_nodes for VAG
    top = top_boundary(mesh)
    bottom = bottom_boundary(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx
    data.boundary_conditions.Dirichlet_states[bottom] = Xbottom
    data.boundary_conditions.Dirichlet_contexts[bottom] = bot_ctx

    # define a heat flux at the bottom part
    heating_below = mesh.origin[-1] + mesh.extent[-1] / 4
    z_cell = Data(mesh.cell_isobarycenters)[:, -1]
    heating_cells = z_cell < heating_below
    vol_sources = data.matrix.volumetric_sources.dtype()
    vol_sources.molar[...] = 0.0
    vol_sources.energy = 5.0  # in W/m^3 = J/m^3/s
    data.matrix.volumetric_sources[heating_cells] = vol_sources

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        direct_solver=direct_solver,
        verbosity=3,
    )
    # if necessary, adapt the Newton or timestepper coefficients
    # time_loop.loop.timestepper.step_solver.tolerance = 1e-9
    # time_loop.loop.timestepper.step_solver.maxiter = 25
    # time_loop.loop.timestep_manager.increase_factor = 1.5

    dt = 1e4
    final_time = 10 * day
    snapshot_period = final_time / 3

    solution, tick = time_loop.run(
        initial_step=dt,
        # fixed_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
        # output_every=10,
        nitermax=nitermax,
    )
    postprocess(time_unit="day")


def build_regular_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))


@pytest.mark.parametrize(
    "num_scheme,mesh_builder",
    [
        ("TPFA", build_regular_mesh),
        ("TPFA", build_3D_scottish_mesh),
        ("VAG", build_regular_mesh),
        ("VAG", build_3D_scottish_mesh),
    ],
)
def test_coats_energy_fast(num_scheme, mesh_builder):
    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    mesh = mesh_builder(Nx, Ny, Nz, Lx, Ly, Lz)
    solve_Darcy(num_scheme, model, mesh, True, nitermax=1)


@pytest.mark.slow
def test_coats_energy(numerical_scheme=None):

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)

    num_schemes = numerical_scheme or ["TPFA", "VAG"]
    for num_scheme in num_schemes:
        assert num_scheme in ["TPFA", "VAG"]

        print("-----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D regular grid")
        mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        Schur_elimination = False
        # the default iterative petsc solver (and cpramg precond) does not converge
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"-------------------{num_scheme} WITH SCHUR ELIMINATION------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)
        print("----------------------------------------------------------------")
        print("Solve coats - energy with Xcoats structure over a 3D scottish grid")
        mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
        print(
            f"---------{num_scheme} WITHOUT SCHUR ELIMINATION (direct solver)---------"
        )
        # the default iterative petsc solver (and cpramg precond) does not converge
        Schur_elimination = False
        solve_Darcy(num_scheme, model, mesh, Schur_elimination, direct_solver=True)
        print(
            f"-------------------{num_scheme} WITH SCHUR ELIMINATION------------------"
        )
        Schur_elimination = True
        solve_Darcy(num_scheme, model, mesh, Schur_elimination)

    print("end of test")


if __name__ == "__main__":
    # by default, all num schemes
    num_scheme = None
    # read num scheme from command line, otherwise both num schemes are used
    if len(sys.argv) > 1:
        num_scheme = [sys.argv[1]]

    test_coats_energy(numerical_scheme=num_scheme)
