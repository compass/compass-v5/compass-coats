import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.old_assembler import LS_Assembler

# FIXME: the following fails on windows because EigeinSolver cannot be pickled (copy.copy in globalgo)
if platform.system() == "Linux":
    from loaf.petsc_solver import PetscSolver as LinearSolver
else:
    from loaf.eigen_solver import EigenSolver as LinearSolver
from compass_coats.newton_convergence import Coats_norm
from compass_coats.output_visu import Snapshooter, make_sites_dumper
from compass_coats.output_callbacks import CoatsOutputCallbacks
from compass_coats.postprocess import postprocess
from compass_coats import (
    TPFA,
)
from icus import field, join, intersect, labels, tensor
from compass_coats.models import Coats
from physics.physical_state import States_with_contexts
from physicalprop.properties import Properties
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import (
    init_default_rock_physical_properties,
)
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary_faces,
    bottom_boundary_faces,
)
from loaf import make_block_linear_system

from globalgo import configure_timeloop
from globalgo.core import EventPool, AllAttemptsFailed
from globalgo.timestep import DynamicStep
from globalgo.config import ConfigHelper, NewtonInitializer

from compass_utils.units import *


class Coats_linearized_problem:
    def __init__(self, assembler, *, callbacks=None):
        self.__assembler__ = assembler
        self.__residual_norm__ = assembler.residual_norm
        self.__events__ = None
        self.__callbacks__ = callbacks or CoatsOutputCallbacks

    def get_solver_initializer(self, config, namespaces, type_key):
        return NewtonInitializer(self, config, namespaces, type_key)


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def assemble_Darcy(
    mesh,
    the_physics,
    coats_properties,
    N,
    L,
    p0,
    lambda_0,
    Schur_elimination,
):
    the_mesh = mesh
    top_faces = top_boundary_faces(mesh)
    bottom_faces = bottom_boundary_faces(mesh)
    dirichlet_faces = join(top_faces.iset, bottom_faces.iset)
    tpfa = TPFA(mesh, the_mesh.cells, dirichlet_faces)

    rocktypes = labels(tpfa.sites)
    rocktypes[...] = 0
    porosity = field(the_mesh.cells, dtype=np.float64)
    porosity[...] = 0.15
    # compute porosity * cell volume
    cell_measures = mesh.cell_measures.as_array()
    poro_vol = field(the_mesh.cells, dtype=np.float64)
    poro_vol.as_array()[...] = porosity.as_array() * cell_measures
    # compute (1 - porosity) * cell volume (for Fourier eq)
    poro_1vol = field(the_mesh.cells, dtype=np.float64)
    poro_1vol.as_array()[...] = (1.0 - porosity.as_array()) * cell_measures

    K = field(the_mesh.cells, dtype=tensor((3, 3), "d"))
    K[...] = np.diag((1e-12, 1e-12, 1e-12))
    Darcy_trans = tpfa.build_transmissivities(K)
    # reservoir thermal conductivity
    thermal_conductivity = field(the_mesh.cells, dtype=tensor((3, 3), "d"))
    thermal_conductivity[...] = np.diag((3.0, 3.0, 3.0))
    Fourier_trans = tpfa.build_transmissivities(thermal_conductivity)

    # init porous volumes: distribute the volume to the sites, just a mapping
    # in tpfa
    poro_darcy_vol = tpfa.distribute_volumes(poro_vol)
    poro_fourier_vol = tpfa.distribute_volumes(poro_vol)
    rock_fourier_vol = tpfa.distribute_volumes(poro_1vol)

    coats_states = field(tpfa.sites, dtype=the_physics.State)
    coats_contexts = field(tpfa.sites, dtype=the_physics.Context)
    coats_solution = States_with_contexts(coats_states, coats_contexts)

    # BC
    top_ctx, Xtop = coats_properties.build_state(
        the_physics.Context.diphasic,
        p=1.1e6,
        T=310,
        Sg=0.1,
    )
    bot_ctx, Xbottom = coats_properties.build_state(
        the_physics.Context.liquid,
        p=1.0e6,
        T=300,
        Cal=1.0e6 / 7.14e9 / 2.0,
    )

    # init states
    coats_solution.states[...] = Xbottom
    coats_solution.contexts[...] = bot_ctx
    # modify BC states
    # get top faces (in sites.location tree) in sites tree
    top_sites = tpfa.sites.from_location(top_faces.iset)
    coats_solution.states[top_sites] = Xtop
    coats_solution.contexts[top_sites] = top_ctx

    if Schur_elimination:
        Jac, RHS = make_block_linear_system(tpfa.sites, the_physics.nb_dof)
    else:
        Jac, RHS = make_block_linear_system(
            tpfa.sites, the_physics.nb_natural_variables
        )

    assembler = LS_Assembler(
        the_physics,
        mesh,
        tpfa,
        coats_properties,
        rocktypes,
        poro_darcy_vol,
        poro_fourier_vol,
        rock_fourier_vol,
        Darcy_trans,
        Fourier_trans,
        dirichlet_faces,
        Jac,
        RHS,
        Schur_elimination=Schur_elimination,
        gravity=np.array([0.0, 0.0, 0.0]),
    )
    assembler.residual_norm = Coats_norm()
    return assembler, coats_solution, tpfa.sites


def test_coats_energy():

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    N = [Nx, Ny, Nz]
    L = [Lx, Ly, Lz]
    p0 = 1.0e5
    # pL = 1.0e6
    lambda_0 = 2.3
    # lambda_L = 0.6

    final_time = 10 * day

    start_dt = 1e3

    snapshot_period = final_time / 10

    Schur_elimination = True

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    diphasic_physics = model.physics
    # load the default physical properties of the diphasic physics
    fluid_properties = init_default_fluid_physical_properties(diphasic_physics)
    rock_properties = init_default_rock_physical_properties(diphasic_physics)
    coats_prop = Properties(fluid_properties, rock_properties)

    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    #
    assembler, initial_solution, sites = assemble_Darcy(
        mesh,
        diphasic_physics,
        coats_prop,
        N,
        L,
        p0,
        lambda_0,
        Schur_elimination,
    )

    linearized_problem = Coats_linearized_problem(
        assembler,
        callbacks=CoatsOutputCallbacks,
    )
    # helps to write the config dictionnaries
    H = ConfigHelper()

    config = {
        "newton": {"tolerance": 1e-8, "maxiter": 10},
        "linear_solver": LinearSolver(),
        "timesteps": DynamicStep(start_dt),
        "events": [],  # factory
        # default is linearized_problem,
        # give argument to the CoatsOutputCallbacks class
        "callbacks": [H.default(verbosity=3)],
    }

    time_loop = configure_timeloop(linearized_problem, config)

    time_loop.run(
        solution=initial_solution,
        stop=final_time,
        events=EventPool().add_recurring_time(
            period=snapshot_period,
            action=Snapshooter(make_sites_dumper(mesh, sites, model)),
        ),
    )
    #
    postprocess()


def test_coats_energy_allattemptsfailed():

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    N = [Nx, Ny, Nz]
    L = [Lx, Ly, Lz]
    p0 = 1.0e5
    # pL = 1.0e6
    lambda_0 = 2.3
    # lambda_L = 0.6

    final_time = 10 * day

    # start_dt is smaller than the minimum timestep
    # to raise error
    start_dt = 1
    minimum_step = 1e3

    snapshot_period = final_time / 10

    Schur_elimination = True

    # load the physics
    model = Coats("diphasic")
    diphasic_physics = model.physics
    # load the default physical properties of the diphasic physics
    fluid_properties = init_default_fluid_physical_properties(diphasic_physics)
    rock_properties = init_default_rock_physical_properties(diphasic_physics)
    coats_prop = Properties(fluid_properties, rock_properties)

    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    #
    assembler, initial_solution, sites = assemble_Darcy(
        mesh,
        diphasic_physics,
        coats_prop,
        N,
        L,
        p0,
        lambda_0,
        Schur_elimination,
    )

    linearized_problem = Coats_linearized_problem(
        assembler,
        callbacks=CoatsOutputCallbacks,
    )
    # helps to write the config dictionnaries
    H = ConfigHelper()

    config = {
        # tolerance & maxiter to trigger AllAttemptsFailed
        "newton": {"tolerance": 1e-18, "maxiter": 1},
        "linear_solver": LinearSolver(),
        "timesteps": DynamicStep(start_dt, minimum_step=minimum_step),
        "events": [],  # factory
        # default is linearized_problem,
        # give argument to the CoatsOutputCallbacks class
        "callbacks": [H.default(verbosity=3)],
    }

    time_loop = configure_timeloop(linearized_problem, config)

    with pytest.raises(AllAttemptsFailed):
        time_loop.run(
            solution=initial_solution,
            stop=final_time,
            events=EventPool().add_recurring_time(
                period=snapshot_period,
                action=Snapshooter(make_sites_dumper(mesh, sites, model)),
            ),
        )


if __name__ == "__main__":
    test_coats_energy()
