import numpy as np
from compass_coats.old_assembler import LS_Assembler
from loaf.eigen_solver import EigenSolver
from compass_coats.newton_convergence import Coats_norm
from compass_coats.output_visu import Snapshooter, make_sites_dumper
from compass_coats.postprocess import postprocess
from compass_coats import (
    TPFA,
)
from icus import field, join, intersect, labels, tensor
from physics.physical_state import States_with_contexts
from physicalprop.properties import Properties
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import (
    init_default_rock_physical_properties,
)
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary_faces,
    bottom_boundary_faces,
)
from loaf import make_block_linear_system

from globalgo.core import Timeloop, Timestepper, EventPool, Newton
from globalgo.timestep import DynamicStep
from globalgo.callbacks import with_callbacks
from compass_coats.models import Coats
from compass_coats.output_callbacks import CoatsOutputCallbacks
from globalgo.core.utils import F

from compass_utils.units import *


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def assemble_Darcy(
    mesh,
    the_physics,
    coats_properties,
    N,
    L,
    p0,
    lambda_0,
    Schur_elimination,
):
    the_mesh = mesh
    top_faces_among_faces = top_boundary_faces(mesh).iset
    bottom_faces_among_faces = bottom_boundary_faces(mesh).iset
    dirichlet_faces = join(top_faces_among_faces, bottom_faces_among_faces)
    tpfa = TPFA(mesh, the_mesh.cells, dirichlet_faces)

    rocktypes = labels(tpfa.sites)
    rocktypes[...] = 0
    porosity = field(the_mesh.cells, dtype=np.float64)
    porosity[...] = 0.15
    # compute porosity * cell volume
    cell_measures = mesh.cell_measures.as_array()
    poro_vol = field(the_mesh.cells, dtype=np.float64)
    poro_vol.as_array()[...] = porosity.as_array() * cell_measures
    # compute (1 - porosity) * cell volume (for Fourier eq)
    poro_1vol = field(the_mesh.cells, dtype=np.float64)
    poro_1vol.as_array()[...] = (1.0 - porosity.as_array()) * cell_measures

    K = field(the_mesh.cells, dtype=tensor((3, 3), "d"))
    K[...] = np.diag((1e-12, 1e-12, 1e-12))
    Darcy_trans = tpfa.build_transmissivities(K)
    # reservoir thermal conductivity
    thermal_conductivity = field(the_mesh.cells, dtype=tensor((3, 3), "d"))
    thermal_conductivity[...] = np.diag((3.0, 3.0, 3.0))
    Fourier_trans = tpfa.build_transmissivities(thermal_conductivity)

    # init porous volumes: distribute the volume to the sites, just a mapping
    # in tpfa
    poro_darcy_vol = tpfa.distribute_volumes(poro_vol)
    poro_fourier_vol = tpfa.distribute_volumes(poro_vol)
    rock_fourier_vol = tpfa.distribute_volumes(poro_1vol)

    coats_states = field(tpfa.sites, dtype=the_physics.State)
    coats_contexts = field(tpfa.sites, dtype=the_physics.Context)
    coats_solution = States_with_contexts(coats_states, coats_contexts)

    # BC
    top_faces = intersect(
        top_faces_among_faces, tpfa.sites.location, tpfa.sites.location
    )
    top_ctx, Xtop = coats_properties.build_state(
        the_physics.Context.diphasic,
        p=1.1e6,
        T=310,
        Sg=0.1,
    )
    bot_ctx, Xbottom = coats_properties.build_state(
        the_physics.Context.liquid,
        p=1.0e6,
        T=300,
        Cal=1.0e6 / 7.14e9 / 2.0,
    )

    # init states
    coats_solution.states[...] = Xbottom
    coats_solution.contexts[...] = bot_ctx
    # modify BC states
    for i in top_faces.mapping():
        coats_solution.states[i] = Xtop
        coats_solution.contexts[i] = top_ctx
    if Schur_elimination:
        Jac, RHS = make_block_linear_system(tpfa.sites, the_physics.nb_dof)
    else:
        Jac, RHS = make_block_linear_system(
            tpfa.sites, the_physics.nb_natural_variables
        )

    assembler = LS_Assembler(
        the_physics,
        mesh,
        tpfa,
        coats_properties,
        rocktypes,
        poro_darcy_vol,
        poro_fourier_vol,
        rock_fourier_vol,
        Darcy_trans,
        Fourier_trans,
        dirichlet_faces,
        Jac,
        RHS,
        Schur_elimination=Schur_elimination,
        gravity=np.array([0.0, 0.0, 0.0]),
    )
    assembler.residual_norm = Coats_norm()
    return assembler, coats_solution, tpfa.sites


def test_coats_energy():

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0
    N = [Nx, Ny, Nz]
    L = [Lx, Ly, Lz]
    p0 = 1.0e5
    # pL = 1.0e6
    lambda_0 = 2.3
    # lambda_L = 0.6

    final_time = 10 * day

    start_dt = 1e3

    snapshot_period = final_time / 10

    Schur_elimination = True

    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    diphasic_physics = model.physics
    # load the default physical properties of the diphasic physics
    fluid_properties = init_default_fluid_physical_properties(diphasic_physics)
    rock_properties = init_default_rock_physical_properties(diphasic_physics)
    coats_prop = Properties(fluid_properties, rock_properties)

    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    #
    cb_log = CoatsOutputCallbacks()
    #
    assembler, initial_solution, sites = assemble_Darcy(
        mesh,
        diphasic_physics,
        coats_prop,
        N,
        L,
        p0,
        lambda_0,
        Schur_elimination,
    )

    with_callbacks(cb_log.timeloop_run)("run")(
        Timeloop(
            F(
                with_callbacks(cb_log.timestepper_make_step)("make_step"),
                with_callbacks(cb_log.timestepper_try_step)("try_step"),
            )(
                Timestepper(
                    with_callbacks(cb_log.newton_solve)("solve")(
                        Newton(
                            tolerance=1e-8,
                            maxiter=10,
                            assembler=F(
                                with_callbacks(cb_log.assembler_evaluate)("evaluate"),
                                with_callbacks(cb_log.assembler_set_up)("set_up"),
                                with_callbacks(cb_log.assembler_update_solution)(
                                    "update_solution"
                                ),
                            )(assembler),
                            residual_norm=F(
                                with_callbacks(cb_log.residual_norm_norm)("norm"),
                                with_callbacks(cb_log.residual_norm_set_up)("set_up"),
                            )(assembler.residual_norm),
                            linear_solver=F(
                                with_callbacks(cb_log.linear_solver_solve)("solve"),
                                with_callbacks(cb_log.linear_solver_set_up)("set_up"),
                            )(EigenSolver()),
                        ),
                    ),
                ),
            ),
            DynamicStep(start_dt),
        ),
    ).run(
        solution=initial_solution,
        stop=final_time,
        events=EventPool().add_recurring_time(
            period=snapshot_period,
            action=Snapshooter(make_sites_dumper(mesh, sites, model)),
        ),
    )
    #
    postprocess()


if __name__ == "__main__":
    test_coats_energy()
