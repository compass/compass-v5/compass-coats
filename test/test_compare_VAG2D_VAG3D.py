import platform
import pytest
import numpy as np

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

# 3D diphasic test with TPFA or VAG
# comparison of a 3D VAG test with nx = 1 (2D extruded)
# and a 2D VAG test with the same thickness and petrophysics data
# to compare the results

from compass_coats.models import Coats
from compass_coats.schemes import VAG
from icmesh import regular_mesh, top_boundary_nodes
from compass_utils.units import bar, degC, day
from compass_coats.standard_time_loop import Standard_time_loop
import icus
from icus.fig import Data, Zone


poro = 0.15
perm = 1e-10  # m^2
thermal_cond = 3.0  # W/m/K
thick = 2.0  # m
Tinit = 15 * degC
# the pressure should not be too high so rho * g * z should be
# non negligible (to have a representative relative norm)
pinit = 3 * bar


def sol_pos_at_nodes(mesh, time_loop, solution):
    sites = time_loop.linearized_problem.scheme.sites.iset
    # mesh_sites = sites.to_location(sites)
    # compute intersection with the nodes
    nodes = icus.intersect(mesh.nodes, sites.location, mesh.nodes)
    if nodes.size() == 0:
        return None, None
    # comprendre invalid call to mapping
    # keep only the nodes with x = 0
    xyz = Data(mesh.isobarycenters(nodes))
    frac_nodes = abs(xyz[0]) < 1e-3
    sites_nodes = Zone(sites.from_location(frac_nodes.iset))

    return Data(solution.states)[sites_nodes], xyz[frac_nodes]


def simu_def(
    Nz,
    gravity,
    context,
    ptop,
    Ttop,
    Sgtop,
    Cal_init,
    cells=None,
):
    # init the mesh and the geometry utilities
    Lx, Ly, Lz = thick, 5.0, 30.0  # cell lengths (in m)
    Nx, Ny = 1, 2
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # init the scheme
    scheme_def = VAG(cells=cells)
    # init the model with the chosen physics
    model = Coats("diphasic")
    if not gravity:
        model.gravity = np.array([0.0, 0.0, 0.0])

    # init matrix or fractures petrophysics and rocktypes
    data = model.new_data(mesh.dimension)

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=pinit,
        T=Tinit,
        Cal=Cal_init,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # fix top Dirichlet BC
    if context == "diphasic":
        top_ctx, Xtop = model.utils.equilibriate_state(
            context=context,
            p=ptop,
            T=Ttop,
            Sg=Sgtop,
        )
    elif context == "liquid":
        top_ctx, Xtop = model.utils.equilibriate_state(
            context=context,
            p=ptop,
            T=Ttop,
        )
    top = top_boundary_nodes(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    return model, scheme_def, mesh, data


def diphasic_3D(Nz, gravity, context, ptop, Ttop, Sgtop, Cal_init, final_time):
    model, scheme_def, mesh, data = simu_def(
        Nz=Nz,
        gravity=gravity,
        context=context,
        ptop=ptop,
        Ttop=Ttop,
        Sgtop=Sgtop,
        Cal_init=Cal_init,
    )

    data.matrix.petrophysics.porosity[...] = poro
    data.matrix.petrophysics.permeability[...] = perm
    data.matrix.petrophysics.thermal_conductivity[...] = thermal_cond
    data.matrix.rocktypes[...] = 0  # cells values

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        verbosity=0,
    )
    # if necessary, adapt the Newton or timestepper coefficients
    time_loop.loop.timestepper.step_solver.maxiter = 10
    time_loop.loop.timestep_manager.increase_factor = 1.5

    solution, tick = time_loop.run(
        initial_step=1e4,
        final_time=final_time,
    )

    # will compare the result over the NODES with x = 0
    return sol_pos_at_nodes(mesh, time_loop, solution)


# run VAG 2D over faces where x=0 with same petrophysics values as VAG3D
def diphasic_2D(Nz, gravity, context, ptop, Ttop, Sgtop, Cal_init, final_time):
    # cells define a restriction of the cells where the simulation is executed
    # In this test, run only on fractures, so define cells as empty zone
    model, scheme_def, mesh, data = simu_def(
        cells=Zone(),
        Nz=Nz,
        gravity=gravity,
        context=context,
        ptop=ptop,
        Ttop=Ttop,
        Sgtop=Sgtop,
        Cal_init=Cal_init,
    )
    # define the simulated 2D plane
    # get the faces belonging to the plane x = 0
    xyz = Data(mesh.face_isobarycenters)
    frac_faces = abs(xyz[0]) < 1e-3
    thickness = Data.from_zone(frac_faces, dtype=np.float64)
    thickness[...] = thick
    # boolean Data carried by faces
    frac = Data.from_zone(frac_faces, dtype="bool")
    frac[...] = True
    data.fractures.faces[...] = frac
    # thickness: Data carried by frac_faces zone
    data.fractures.thickness[...] = thickness
    data.fractures.petrophysics.porosity[...] = poro
    data.fractures.petrophysics.permeability[...] = perm
    data.fractures.petrophysics.thermal_conductivity[...] = thermal_cond

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        verbosity=0,
    )
    # if necessary, adapt the timestepper coefficients
    time_loop.loop.timestep_manager.increase_factor = 1.5

    solution, tick = time_loop.run(
        initial_step=1e4,
        final_time=final_time,
    )

    # will compare the result over the NODES with x = 0
    # to compare the data, return also the sites position
    return sol_pos_at_nodes(mesh, time_loop, solution)


def compute_diff(sol3D, sites_pos3D, sol2D, sites_pos2D, eps=2e-4):
    # by chance the sites are in the same order, check it
    assert (
        np.abs(sites_pos3D.field.as_array() - sites_pos2D.field.as_array()).max() < eps
    )

    arr2D = sol2D.field.as_array()
    arr3D = sol3D.field.as_array()

    pnorm = np.abs((arr2D.pressure - arr3D.pressure) / arr2D.pressure).max()
    Tnorm = np.abs((arr2D.temperature - arr3D.temperature) / arr2D.temperature).max()
    # values of order 1, not necessary to divide (and not possible to divide by 0)
    Cnorm = np.abs(arr2D.molar_fractions - arr3D.molar_fractions).max()
    Snorm = np.abs(arr2D.saturation - arr3D.saturation).max()
    print(f"norm press={pnorm}, temp={Tnorm}, mol frac={Cnorm}, sat={Snorm}")
    assert pnorm < eps, f"pnorm={pnorm} >= eps ({eps})"
    assert Tnorm < eps, f"Tnorm={Tnorm} >= eps ({eps})"
    assert Cnorm < eps, f"Cnorm={Cnorm} >= eps ({eps})"
    assert Snorm < eps, f"Snorm={Snorm} >= eps ({eps})"


@pytest.mark.slow
def test_compare_2D_3D():
    ptop = 2 * bar
    Sgtop = 0.5  # if diphasic top boundary, use this saturation
    for context in ["liquid", "diphasic"]:
        for Ttop, Tstr in zip([Tinit, 35 * degC], ["Tinit", "Tinit+20C"]):
            for gravity in [False, True]:
                for Cal_init in [0, 1.0e-4]:
                    for Nz in [10, 20]:
                        for final_time, tstr in zip(
                            [50 * day, 100 * day], ["50d", "100d"]
                        ):
                            print("---------------------------------------------")
                            print(
                                f"top {context} BC with T={Tstr}, gravity is {gravity}, init Cal={Cal_init}, Nz={Nz}, tf={tstr}"
                            )
                            sol3D, sites_pos3D = diphasic_3D(
                                Nz=Nz,
                                gravity=gravity,
                                context=context,
                                ptop=ptop,
                                Ttop=Ttop,
                                Sgtop=Sgtop,
                                Cal_init=Cal_init,
                                final_time=final_time,
                            )
                            sol2D, sites_pos2D = diphasic_2D(
                                Nz=Nz,
                                gravity=gravity,
                                context=context,
                                ptop=ptop,
                                Ttop=Ttop,
                                Sgtop=Sgtop,
                                Cal_init=Cal_init,
                                final_time=final_time,
                            )
                            compute_diff(sol3D, sites_pos3D, sol2D, sites_pos2D)


def test_compare_2D_3D_fast():
    sol3D, sites_pos3D = diphasic_3D(
        Nz=15,
        gravity=True,
        context="liquid",
        ptop=2 * bar,
        Ttop=Tinit,
        Sgtop=0,
        Cal_init=0,
        final_time=50 * day,
    )
    sol2D, sites_pos2D = diphasic_2D(
        Nz=15,
        gravity=True,
        context="liquid",
        ptop=2 * bar,
        Ttop=Tinit,
        Sgtop=0,
        Cal_init=0,
        final_time=50 * day,
    )
    compute_diff(sol3D, sites_pos3D, sol2D, sites_pos2D, eps=2e-8)
