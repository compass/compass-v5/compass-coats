import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.models import Coats
from compass_coats.schemes import VAG
from icmesh import (
    regular_mesh,
    top_boundary_nodes,
    xmax_boundary_nodes,
)
from compass_utils.units import bar, degC, day, year
from compass_coats.standard_time_loop import Standard_time_loop
from compass_coats.postprocess import postprocess


def test_solve_Darcy():
    Nx, Ny, Nz = [15, 3, 10]
    Lx, Ly, Lz = [300, 50, 200]
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    scheme_def = VAG()
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    # gravity is a vector, by default [0, 0, 9.81]
    model.gravity = np.array([10.0, 0.0, 5.0])

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.2
    data.matrix.petrophysics.permeability[...] = 1e-10
    data.matrix.petrophysics.thermal_conductivity[...] = 2.0

    # BC and initial states
    ctx0, X0 = model.utils.equilibriate_state(
        context="liquid",
        p=11 * bar,
        T=10 * degC,
    )
    # init all states with same values
    data.initial_states[...] = X0
    data.initial_contexts[...] = ctx0

    # init the dirichlet node (top left corner) with init values
    dirichlet_node = top_boundary_nodes(mesh) & xmax_boundary_nodes(mesh)
    assert len(dirichlet_node) == Ny + 1
    data.boundary_conditions.Dirichlet_states[dirichlet_node] = X0
    data.boundary_conditions.Dirichlet_contexts[dirichlet_node] = ctx0

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
    )

    dt = 100 * day
    final_time = 10 * year

    sol, tick = time_loop.run(
        initial_step=dt,
        final_time=final_time,
        output_every=1,
        nitermax=1,
    )

    #%% Some postprocesses, it allows to visualize with Paraview
    postprocess(time_unit="day")
