import platform
import pytest
import numpy as np

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import regular_mesh, top_boundary
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data, Zone
from compass_utils.filenames import output_directory


def simu_def(num_name, cells=None, Nx=1, Ny=4, Nz=20):
    # init the mesh and the geometry utilities
    # Nx, Ny, Nz = 1, 4, 20  # number of cells
    Lx, Ly, Lz = 1.0, 5.0, 50.0  # cell lengths (in m)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # init the scheme
    if num_name == "VAG":
        scheme_def = VAG(cells=cells)
    elif num_name == "TPFA":
        scheme_def = TPFA(cells=cells)  # with no fracture yet
    else:
        raise f"scheme {num_name} not implemented, use TPFA or VAG"
    # init the model with the chosen physics
    model = Coats("diphasic")

    # init matrix or fractures petrophysics and rocktypes
    data = model.new_data(mesh.dimension)

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",
        p=5 * bar,
        T=35 * degC,
        Sg=0.5,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    return model, scheme_def, mesh, data


def test_diphasic_Dirichlet(num_name="TPFA"):
    model, scheme_def, mesh, data = simu_def(num_name)

    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # cells values

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
    )
    # if necessary, adapt the Newton or timestepper coefficients
    time_loop.loop.timestepper.step_solver.maxiter = 25
    time_loop.loop.timestep_manager.increase_factor = 1.5

    final_time = 100 * day
    solution, tick = time_loop.run(
        initial_step=1e4,
        final_time=final_time,
        output_period=final_time / 4,
    )
    # postprocess to prepare for visualization
    postprocess(time_unit="day")


@pytest.mark.slow
def test_diphasic_Dirichlet_slow():
    test_diphasic_Dirichlet(num_name="VAG")


# VAG 2D in a 3D mesh, implemented only with VAG yet
@pytest.mark.slow
def test_diphasic_Dirichlet_2D():
    # cells define a restriction of the cells where the simulation is executed
    # In this test, run only on fractures, so define cells as empty zone
    model, scheme_def, mesh, data = simu_def("VAG", cells=Zone(), Nz=30)
    visu_dir = output_directory("diphasic2DVAG")
    # get the faces belonging to the plane y = 0
    xyz = Data(mesh.face_isobarycenters)
    frac_faces = abs(xyz[1]) < 1e-3
    thickness = Data.from_zone(frac_faces, dtype=np.float64)
    thickness[...] = 0.01
    # boolean Data carried by faces
    frac = Data.from_zone(frac_faces, dtype="bool")
    frac[...] = True
    data.fractures.faces[...] = frac
    # thickness: Data carried by frac_faces zone
    data.fractures.thickness[...] = thickness
    data.fractures.petrophysics.porosity[...] = 0.2
    data.fractures.petrophysics.permeability[...] = 1e-8
    data.fractures.petrophysics.thermal_conductivity[...] = 4.0

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        output_dir=visu_dir,
    )
    # if necessary, adapt the timestepper coefficients
    time_loop.loop.timestep_manager.increase_factor = 1.1

    final_time = 100 * day
    solution, tick = time_loop.run(
        initial_step=1e4,
        final_time=final_time,
        output_period=final_time / 4,
    )
    # postprocess to prepare for visualization
    postprocess(visu_dir, time_unit="day")
