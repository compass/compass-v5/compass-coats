from icus import field, join
from icus.fig import Data, Zone
import numpy as np
from compass_coats import TPFA
from compass_coats.output_visu import (
    Dumper,
    Snapshooter,
    Location,
    make_sites_dumper,
    OutputDirectoryManager,
)
from compass_coats.postprocess import postprocess
from icmesh import regular_mesh, top_boundary_faces
from physics.physical_state import States_with_contexts
from compass_coats.models import Coats


def init_fake_simulation(phases=None, components=None):
    model = Coats("diphasic", phases=phases, components=components)
    the_physics = model.physics
    mesh = regular_mesh((8, 5, 3))
    sites = join(mesh.cells, mesh.nodes)
    Xcoats = field(sites, dtype=the_physics.State)
    for xcoats in Xcoats.as_array():
        xcoats.temperature = 300.0
        xcoats.saturation[the_physics.Phase.gas] = 0.4
        xcoats.saturation[the_physics.Phase.liquid] = (
            1.0 - xcoats.saturation[the_physics.Phase.gas]
        )
        xcoats.molar_fractions[the_physics.Phase.gas][the_physics.Component.water] = 0.1
        xcoats.molar_fractions[the_physics.Phase.liquid][
            the_physics.Component.water
        ] = 0.8
        for alpha in range(the_physics.nb_phases):
            xcoats.molar_fractions[alpha][the_physics.Component.air] = (
                1.0 - xcoats.molar_fractions[alpha][the_physics.Component.water]
            )
            xcoats.pressure[alpha] = 1.0e5
    contexts = field(sites, dtype=the_physics.Context)
    for i in range(int((sites.size() + 1) / 2)):
        contexts[2 * i] = the_physics.Context.gas
        if 2 * i + 1 < sites.size():
            contexts[2 * i + 1] = the_physics.Context.liquid
    sstates = States_with_contexts(Xcoats, contexts)
    return model, mesh, sstates


def run_fake_timeloop(
    model, mesh, sstates, locations, output_dir=OutputDirectoryManager("output-dump")
):
    states = sstates.states
    dumper = Dumper(locations, model, output_dir)
    dumper.set_up(mesh)
    shooter = Snapshooter(dumper)
    for fake_time_iteration in range(5):
        states.as_array().temperature[:] += 10.0
        shooter.shoot(
            sstates,
            fake_time_iteration,
        )

    assert (dumper.outdir_manager.path / "mesh").is_dir()
    assert (dumper.outdir_manager.path / "states").is_dir()
    assert (dumper.outdir_manager.path / "wells").is_dir()


def test_random_cells_nodes():
    model, mesh, sstates = init_fake_simulation()
    states = sstates.states
    sites = states.support()
    ncells = mesh.cells.size()
    # shuffle the cells and nodes
    unordered = np.arange(sites.size())
    np.random.shuffle(unordered)
    cells = sites.extract(list(unordered[:ncells]))
    nodes = sites.extract(list(unordered[ncells:]))
    # give non constant values for nodes and cells
    states.as_array().temperature[cells.mapping_as_array()] = 100
    states.as_array().temperature[nodes.mapping_as_array()] = 300
    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells.mapping_as_array(),
            mapping2mesh=np.arange(cells.size(), dtype=np.uint64),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=nodes.mapping_as_array(),
            mapping2mesh=np.arange(nodes.size(), dtype=np.uint64),
        ),
    }

    # temperature varies in time, T += 10.0
    run_fake_timeloop(model, mesh, sstates, locations)

    postprocess("output-dump", time_unit="second")


def test_random_sub_cells_nodes():
    model, mesh, sstates = init_fake_simulation()
    mesh = mesh
    states = sstates.states
    sites = states.support()
    # simulate over a subset of the cells, get the connecting nodes
    cells = mesh.cells.extract(np.arange(int(mesh.cells.size() / 2)))
    nodes = mesh.cells_nodes.extract(cells, mesh.nodes).image()
    ncells = cells.size()
    nnodes = nodes.size()
    # shuffle the cells and nodes
    unordered = np.arange(sites.size())
    np.random.shuffle(unordered)
    cells_sites = sites.extract(list(unordered[:ncells]))
    nodes_sites = sites.extract(list(unordered[ncells : ncells + nnodes]))
    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells_sites.mapping_as_array(),
            mapping2mesh=cells.mapping_as_array(),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=nodes_sites.mapping_as_array(),
            mapping2mesh=nodes.mapping_as_array(),
        ),
    }

    # temperature varies in time, T += 10.0, T0 = 300
    run_fake_timeloop(model, mesh, sstates, locations)

    postprocess("output-dump", time_unit="second")


def test_other_phases_comp_names():
    phases = ["phase 1", "phase 2"]
    comp = ["comp 1", "comp 2"]
    model, mesh, sstates = init_fake_simulation(phases, comp)
    mesh = mesh
    states = sstates.states
    sites = states.support()
    ncells = mesh.cells.size()
    nnodes = mesh.nodes.size()
    cells2sites = mesh.cells.rebase(sites)
    nodes2sites = mesh.nodes.rebase(sites)
    # give non constant values in space
    cells_centers = mesh.cell_isobarycenters.as_array()
    nodes_centers = mesh.vertices.as_array()
    states.as_array().temperature[cells2sites.mapping_as_array()] = (
        100 + 10 * cells_centers[:, 0] + 50 * cells_centers[:, 1]
    )
    states.as_array().temperature[nodes2sites.mapping_as_array()] = (
        300 + 10 * nodes_centers[:, 1] + 50 * nodes_centers[:, 2]
    )

    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells2sites.mapping_as_array(),
            mapping2mesh=np.arange(ncells, dtype=np.uint64),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=nodes2sites.mapping_as_array(),
            mapping2mesh=np.arange(nnodes, dtype=np.uint64),
        ),
    }

    # temperature varies in time, T += 10.0
    run_fake_timeloop(
        model,
        mesh,
        sstates,
        locations,
        output_dir=OutputDirectoryManager("output-dump"),
    )

    postprocess("output-dump", time_unit="second")


def test_random_space_variable():
    model, mesh, sstates = init_fake_simulation()
    mesh = mesh
    states = sstates.states
    sites = states.support()
    ncells = mesh.cells.size()
    unordered = np.arange(sites.size())
    np.random.shuffle(unordered)
    cells = sites.extract(list(unordered[:ncells]))
    nodes = sites.extract(list(unordered[ncells:]))
    # give non constant values in space
    # warning, cells != mesh.cells; idem for nodes but the order of the
    # plotting cells respect the order of the mesh cells, the sites only are shuffled,
    # thus use mesh location to have space dependent sites values
    cells_centers = mesh.cell_isobarycenters.as_array()
    nodes_centers = mesh.vertices.as_array()
    states.as_array().temperature[cells.mapping_as_array()] = (
        100 + 10 * cells_centers[:, 0] + 50 * cells_centers[:, 2]
    )
    states.as_array().temperature[nodes.mapping_as_array()] = (
        300 + 10 * nodes_centers[:, 1] + 50 * nodes_centers[:, 2]
    )

    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells.mapping_as_array(),
            mapping2mesh=np.arange(cells.size(), dtype=np.uint64),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=nodes.mapping_as_array(),
            mapping2mesh=np.arange(nodes.size(), dtype=np.uint64),
        ),
    }

    # temperature varies in time, T += 10.0
    run_fake_timeloop(
        model,
        mesh,
        sstates,
        locations,
        output_dir=OutputDirectoryManager("output-dump"),
    )

    postprocess("output-dump", time_unit="second")


def test_random_cells_empty_nodes():
    model, mesh, sstates = init_fake_simulation()
    mesh = mesh
    states = sstates.states
    sites = states.support()
    ncells = mesh.cells.size()
    unordered = np.arange(sites.size())
    np.random.shuffle(unordered)
    cells = sites.extract(list(unordered[:ncells]))
    nodes = sites.extract(list())
    # mapping can be None (empty mapping), transform None into empty list
    convert = lambda map: [] if map is None else map
    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells.mapping_as_array(),
            mapping2mesh=np.arange(cells.size(), dtype=np.uint64),
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=convert(nodes.mapping_as_array()),
            mapping2mesh=np.arange(0, dtype=np.uint64),
        ),
    }

    # temperature varies in time, T += 10.0, T0 = 300
    run_fake_timeloop(model, mesh, sstates, locations)

    postprocess("output-dump", time_unit="second")


def test_random_cells_nodes_2D_faces():
    model, mesh, sstates = init_fake_simulation()
    mesh = mesh
    states = sstates.states
    sites = states.support()
    ncells = mesh.cells.size()
    unordered = np.arange(sites.size())
    np.random.shuffle(unordered)
    cells = sites.extract(list(unordered[:ncells]))
    nodes = sites.extract(list(unordered[ncells:]))
    locations = {
        "cells": Location(
            dim=mesh.dimension,
            mapping2sites=cells.mapping_as_array(),
            mapping2mesh=np.arange(cells.size(), dtype=np.uint64),
        ),
        # 2D objects not postprocessed yet
        "faces": Location(
            dim=mesh.dimension - 1,
            mapping2sites=[1, 2, 3],
            mapping2mesh=[0, 1, 2],
        ),
        "nodes": Location(
            dim=0,
            mapping2sites=nodes.mapping_as_array(),
            mapping2mesh=np.arange(nodes.size(), dtype=np.uint64),
        ),
    }

    # temperature varies in time, T += 10.0, T0 = 300
    run_fake_timeloop(model, mesh, sstates, locations)

    postprocess("output-dump", time_unit="second")


def test_dumper_from_scheme():
    model = Coats("diphasic")
    mesh = regular_mesh((2, 2, 2))
    tpfa = TPFA(mesh, mesh.cells, top_boundary_faces(mesh).iset)

    make_sites_dumper(mesh, tpfa.sites, model)


def test_dump_data():
    mesh = regular_mesh((8, 4, 9))
    cell_pos = Data(mesh.cell_isobarycenters)
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    # init petrophysics and rocktypes
    data = model.new_data(mesh.dimension)
    poro_cells = Zone(mesh.cells.extract(np.arange(3, 12)))
    poro = Data.from_zone(poro_cells)
    poro[...] = 0.2
    data.matrix.petrophysics.porosity[...] = poro
    data.matrix.petrophysics.porosity[cell_pos[:, 0] > 4] = 0.4
    data.matrix.petrophysics.permeability[cell_pos[:, 2] < 5] = 1e-10  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 2.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # cells values

    model.dump_data_over_mesh("data_mesh.vtu", data, mesh)


if __name__ == "__main__":
    test_dump_data()
    test_random_cells_nodes()
    test_random_cells_empty_nodes()
    test_random_cells_nodes_2D_faces()
    test_random_space_variable()
    test_other_phases_comp_names()
    test_dumper_from_scheme()
