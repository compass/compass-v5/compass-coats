from pathlib import Path
import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import (
    regular_mesh,
    scottish_mesh,
    read_fvca6,
)
from compass_utils.units import bar, degC, day
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Zone, Data


def max_boundary(axis):
    def boundary_functor(mesh, eps=1e-3):
        # get faces and nodes close to max of axis,
        # no distinction between VAG and TPFA
        domain = Zone() | mesh.faces | mesh.nodes
        # get the coordinates of the domain
        xyz = Data(mesh.isobarycenters(domain.iset))
        # Identify the nodes and faces on max boundary
        max_coord = np.max(xyz[axis])
        return abs(xyz[axis] - max_coord) < eps

    return boundary_functor


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def assert_hydro_results(mesh, model, scheme, sol, PDir, eps):
    liquid_phase = model.phases["liquid"]
    final_liq_p = sol.states.as_array().pressure[:, liquid_phase]
    rho = 1000  # TODO should be obtained from ComPASS instead
    top = np.max(mesh.vertices.as_array(), axis=0)
    sites_position = mesh.isobarycenters(scheme.sites.iset.location).as_array()
    sites_depth = top - sites_position
    hydro_p = PDir[liquid_phase] + rho * np.dot(sites_depth, model.gravity)
    diff = []
    for hydro, pl in zip(hydro_p, final_liq_p):
        diff.append(abs(pl - hydro) / pl)
        assert abs(pl - hydro) / pl < eps

    print(" *** Norm of the error: ", np.linalg.norm(diff), " ***")


def solve_Darcy(
    scheme_name,
    model,
    mesh,
    Schur_elimination,
    dirichlet,
):
    if scheme_name == "TPFA":
        scheme_def = TPFA(schur_elimination=Schur_elimination)
    elif scheme_name == "VAG":
        scheme_def = VAG(schur_elimination=Schur_elimination)
    else:
        raise "num scheme not recognized"

    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.2
    data.matrix.petrophysics.permeability[...] = 1e-10
    data.matrix.petrophysics.thermal_conductivity[...] = 2.0

    # BC and initial states
    ctx0, X0 = model.utils.equilibriate_state(
        context="liquid",
        p=11 * bar,
        T=10 * degC,
    )
    # init all states with same values
    data.initial_states[...] = X0
    data.initial_contexts[...] = ctx0

    # init dirichlet faces/nodes with init values
    data.boundary_conditions.Dirichlet_states[dirichlet] = X0
    data.boundary_conditions.Dirichlet_contexts[dirichlet] = ctx0

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        direct_solver=True,
        verbosity=1,
    )

    dt = 100 * day

    sol, tick = time_loop.run(
        initial_step=dt,
        nitermax=1,
    )

    scheme = time_loop.linearized_problem.scheme
    eps = 2e-10 if scheme_name == "TPFA" else 1e-8
    assert_hydro_results(mesh, model, scheme, sol, X0.pressure, eps=eps)


def all_solve_Darcy(
    N,
    L,
    model,
    scheme_name,
    dirichlet_functor,
):
    assert scheme_name in ["TPFA", "VAG"]
    Nx, Ny, Nz = N
    Lx, Ly, Lz = L
    print("-----------------------------------------------------------------")
    print(
        "Test hydrostatic pressure over a 3D regular grid with g=",
        model.gravity,
    )
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    print(f"{scheme_name} with and without SCHUR elimination")
    Schur_elimination = False
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))
    Schur_elimination = True
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))
    print("----------------------------------------------------------------")
    print(
        "Test hydrostatic pressure over a 3D scottish grid with g=",
        model.gravity,
    )
    print(f"{scheme_name} with and without SCHUR elimination")
    mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
    Schur_elimination = False
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))
    Schur_elimination = True
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))
    print("----------------------------------------------------------------")
    print(
        "Test hydrostatic pressure over a 3D voronoi mesh with g=",
        model.gravity,
    )
    print(f"{scheme_name} with and without SCHUR elimination")
    current_path = Path(__file__).parent.absolute()
    voro_file = (current_path / Path("mesh_data")).as_posix() + "/vmesh_1.msh"
    mesh = read_fvca6(voro_file)
    Schur_elimination = False
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))
    Schur_elimination = True
    solve_Darcy(scheme_name, model, mesh, Schur_elimination, dirichlet_functor(mesh))


def test_hydro_pressure_fast():
    Nx, Ny, Nz = 20, 4, 4
    Lx, Ly, Lz = 1000, 150, 200
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    g = 9
    # gravity is a vector, by default [0, 0, 9.81]
    model.gravity = np.array([g, 0.0, 0.0])
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    current_path = Path(__file__).parent.absolute()
    voro_file = (current_path / Path("mesh_data")).as_posix() + "/vmesh_1.msh"
    voro_mesh = read_fvca6(voro_file)
    for scheme_name in ["TPFA", "VAG"]:
        # apply Dirichlet on xmax
        solve_Darcy(scheme_name, model, mesh, True, max_boundary(axis=0)(mesh))
        solve_Darcy(
            scheme_name, model, voro_mesh, True, max_boundary(axis=0)(voro_mesh)
        )


@pytest.mark.slow
def test_x_hydro_pressure():

    N = [20, 4, 4]
    L = [1000, 150, 200]
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    for g in range(0, 10, 3):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([g, 0.0, 0.0])
        for scheme_name in ["TPFA", "VAG"]:
            # apply Dirichlet on xmax
            all_solve_Darcy(N, L, model, scheme_name, max_boundary(axis=0))


@pytest.mark.slow
def test_y_hydro_pressure():

    N = [5, 21, 4]
    L = [140, 1100, 190]
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    for g in range(0, 3):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([0.0, g, 0.0])
        for scheme_name in ["TPFA", "VAG"]:
            # apply Dirichlet on ymax
            all_solve_Darcy(N, L, model, scheme_name, max_boundary(axis=1))


@pytest.mark.slow
def test_z_hydro_pressure():

    N = [3, 4, 19]
    L = [150, 200, 900]
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    for g in range(0, 11, 2):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([0.0, 0.0, g])
        for scheme_name in ["TPFA", "VAG"]:
            # apply Dirichlet on zmax
            all_solve_Darcy(N, L, model, scheme_name, max_boundary(axis=2))
