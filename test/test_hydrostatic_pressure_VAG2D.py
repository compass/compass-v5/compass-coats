import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.models import Coats
from compass_coats.schemes import VAG
from icmesh import (
    regular_mesh,
    scottish_mesh,
    top_boundary_nodes,
    xmax_boundary_nodes,
    ymax_boundary_nodes,
)
from compass_utils.units import bar, degC, day, year
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data, Zone
from compass_coats.postprocess import postprocess


def scottish_step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(
        scottish_step(Nx, Lx), scottish_step(Ny, Ly), scottish_step(Nz, Lz)
    )


def solve_Darcy(
    model,
    mesh,
    frac_faces,
    thickness,
    Schur_elimination,
    dirichlet_obj,
    test_results_functor,
):
    data = model.new_data(mesh.dimension)
    # boolean Data carried by faces
    frac = Data.from_zone(frac_faces, dtype="bool")
    frac[...] = True
    data.fractures.faces[...] = frac
    # thickness: Data carried by frac_faces zone
    data.fractures.thickness[...] = thickness
    data.fractures.petrophysics.porosity[...] = 0.2
    data.fractures.petrophysics.permeability[...] = 1e-8
    data.fractures.petrophysics.thermal_conductivity[...] = 4.0

    # cells define a restriction of the cells where the simulation is executed
    # In this test, run only on fractures, so define cells as empty zone
    scheme_def = VAG(cells=Zone())

    # BC and initial states
    ctx0, X0 = model.utils.equilibriate_state(
        context="liquid",
        p=9 * bar,
        T=10 * degC,
    )
    # BC and initial states
    ctxDir, XDir = model.utils.equilibriate_state(
        context="liquid",
        p=11 * bar,
        T=10 * degC,
    )
    # init all states with same values
    data.initial_states[...] = X0
    data.initial_contexts[...] = ctx0

    # init dirichlet nodes with init values
    data.boundary_conditions.Dirichlet_states[dirichlet_obj] = XDir
    data.boundary_conditions.Dirichlet_contexts[dirichlet_obj] = ctxDir

    scheme_def.schur_elimination = Schur_elimination

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        direct_solver=True,
    )

    dt = 100 * day
    # not used, does 1 step only
    final_time = 10 * year

    sol, tick = time_loop.run(
        initial_step=dt,
        final_time=final_time,
        nitermax=1,
        output_every=1,
    )

    scheme = time_loop.linearized_problem.scheme
    diff = test_results_functor(
        mesh,
        model,
        scheme,
        sol,
        XDir.pressure,
        X0.pressure,
    )
    postprocess()


def all_solve_Darcy(
    N,
    L,
    model,
    dirichlet_functor,
    test_results_functor,
):
    Nx, Ny, Nz = N
    Lx, Ly, Lz = L
    print("-----------------------------------------------------------------")
    print(
        "Test hydrostatic pressure over faces of the 3D regular grid with g=",
        model.gravity,
    )
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # get the faces belonging to the plane y = 0
    # mesh.face_isobarycenters should be a Data
    xyz = Data(mesh.face_isobarycenters)
    frac_faces = abs(xyz[1]) < 1e-3
    thickness = Data.from_zone(frac_faces, dtype=np.float64)
    thickness[...] = 0.01
    print("with and without SCHUR elimination")
    Schur_elimination = False
    solve_Darcy(
        model,
        mesh,
        frac_faces,
        thickness,
        Schur_elimination,
        dirichlet_functor(mesh),
        test_results_functor,
    )
    Schur_elimination = True
    solve_Darcy(
        model,
        mesh,
        frac_faces,
        thickness,
        Schur_elimination,
        dirichlet_functor(mesh),
        test_results_functor,
    )
    print("----------------------------------------------------------------")
    print(
        "Test hydrostatic pressure over a 3D scottish grid with g=",
        model.gravity,
    )
    print("with and without SCHUR elimination")
    mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
    xyz = Data(mesh.face_isobarycenters)
    frac_faces = abs(xyz[1]) < 1e-3
    thickness = Data.from_zone(frac_faces)
    thickness[...] = 0.01
    Schur_elimination = False
    solve_Darcy(
        model,
        mesh,
        frac_faces,
        thickness,
        Schur_elimination,
        dirichlet_functor(mesh),
        test_results_functor,
    )
    Schur_elimination = True
    solve_Darcy(
        model,
        mesh,
        frac_faces,
        thickness,
        Schur_elimination,
        dirichlet_functor(mesh),
        test_results_functor,
    )


def rho_g_pos(mesh, scheme, gravity):
    rho = 1000  # TODO should be obtained from ComPASS instead
    top = np.array(mesh.origin) + np.array(mesh.extent)
    sites_position = mesh.isobarycenters(scheme.sites.iset.location).as_array()
    sites_depth = top - sites_position
    return rho * np.dot(sites_depth, gravity)


def assert_hydro_results(mesh, model, scheme, sol, PDir, P0, eps=1e-8):
    liquid_phase = model.phases["liquid"]
    rho_g_z = rho_g_pos(mesh, scheme, model.gravity)
    final_liq_p = sol.states.as_array().pressure[:, liquid_phase]
    diff = []
    for hydro, pl in zip(rho_g_z, final_liq_p):
        hydro_p = PDir[liquid_phase] + hydro
        diff.append(abs(pl - hydro_p) / pl)
        assert abs(pl - hydro_p) / pl < eps

    print(" *** Norm of the error: ", np.linalg.norm(diff), " ***")
    return diff


def assert_ctx_results(mesh, model, scheme, sol, PDir, P0, eps=1e-8):
    liquid_phase = model.phases["liquid"]
    final_liq_p = sol.states.as_array().pressure[:, liquid_phase]
    Pinit = P0[liquid_phase]
    diff = []
    for pl in final_liq_p:
        diff.append(abs(pl - Pinit) / pl)
        assert abs(pl - Pinit) / pl < eps

    print(" *** Norm of the error: ", np.linalg.norm(diff), " ***")
    return diff


def test_hydro_pressure_fast():
    Nx, Ny, Nz = 20, 4, 4
    Lx, Ly, Lz = 1000, 150, 200
    model = Coats("diphasic")
    g = 9
    # gravity is a vector, by default [0, 0, 9.81]
    model.gravity = np.array([g, 0.0, 0.0])
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # get the faces belonging to the plane y = 0
    # mesh.face_isobarycenters should be a Data
    xyz = Data(mesh.face_isobarycenters)
    frac_faces = abs(xyz[1]) < 1e-3
    thickness = Data.from_zone(frac_faces)
    thickness[...] = 0.01
    solve_Darcy(
        model,
        mesh,
        frac_faces,
        thickness,
        True,
        xmax_boundary_nodes(mesh),
        assert_hydro_results,
    )


@pytest.mark.slow
def test_x_hydro_pressure():

    N = [20, 4, 4]
    L = [1000, 150, 200]
    model = Coats("diphasic")
    for g in range(0, 10, 3):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([g, 0.0, 0.0])
        all_solve_Darcy(
            N,
            L,
            model,
            xmax_boundary_nodes,
            assert_hydro_results,
        )


@pytest.mark.slow
def test_y_hydro_pressure():

    N = [5, 21, 4]
    L = [140, 1100, 190]
    model = Coats("diphasic")
    for g in range(0, 3):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([0.0, g, 0.0])
        # the plane is orthogonal to the gravity, then the result must be cst in space
        all_solve_Darcy(
            N,
            L,
            model,
            ymax_boundary_nodes,
            assert_ctx_results,
        )


@pytest.mark.slow
def test_z_hydro_pressure():

    N = [3, 4, 19]
    L = [150, 200, 900]
    model = Coats("diphasic")
    for g in range(0, 11, 2):
        # gravity is a vector, by default [0, 0, 9.81]
        model.gravity = np.array([0.0, 0.0, g])
        all_solve_Darcy(
            N,
            L,
            model,
            top_boundary_nodes,
            assert_hydro_results,
        )
