import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

from compass_coats.models import Coats
from compass_coats.schemes import TPFA
from compass_utils.filenames import output_directory
from icmesh import regular_mesh, top_boundary_faces
from compass_utils.units import bar, degC, year, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Zone, Data, Record


def test_diphasic_Dirichlet():
    # init the mesh and the geometry utilities
    Nx = Ny = 4
    Nz = 30
    Lx = Ly = 20.0  # cell lengths (in m)
    Lz = 200.0  # cell lengths (in m)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # init the scheme
    scheme_def = TPFA()
    # init the model with the chosen physics, optional precision: no fracture
    model = Coats("diphasic", with_fractures=False)

    # init petrophysics and rocktypes
    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.25
    data.matrix.petrophysics.permeability[...] = 1e-11  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 2.5  # W/m/K
    data.matrix.rocktypes[...] = 0  # cells values, no pc with rkt = 0

    # initial state and BC: top Dirichlet, hydrostatic pressure in reservoir
    # create equilibrate ref_state
    ptop = 10 * bar
    ref_context, ref_state = model.utils.equilibriate_state(
        context="liquid",
        p=ptop,
        T=15 * degC,
    )
    # Set Dirichlet values at the top (faces because tpfa scheme)
    top = top_boundary_faces(mesh)
    data.boundary_conditions.Dirichlet_states[top] = ref_state
    data.boundary_conditions.Dirichlet_contexts[top] = ref_context

    # Compute hydrostatic pressure to init the cells
    cells = Zone(mesh.cells)
    cell_centers = Data(mesh.isobarycenters(cells.iset))
    z_top = mesh.origin[2] + mesh.extent[2]
    # gravity is a vector, by default [0, 0, 9.81]
    rho_g = 1000 * model.gravity[-1]
    hydro_pressures = ptop + rho_g * (z_top - cell_centers[2])

    state_record = Record(data.initial_states.dtype)
    state_record[:] = ref_state
    # must init the cells with hydro pressure
    state = state_record[cells]
    # no pc, all phase pressures are equal
    state["pressure", model.phases["liquid"]] = hydro_pressures
    state["pressure", model.phases["gas"]] = hydro_pressures
    # Init data with state value and context
    data.initial_contexts[:] = ref_context
    data.initial_states[:] = state

    # Create and run time loop
    visu_dir = output_directory(__file__)
    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        output_dir=visu_dir,
    )

    solution, tick = time_loop.run(
        initial_step=1 * day,
        final_time=1 * year,
        nb_output=10,
    )
    # postprocess to prepare for visualization
    postprocess(visu_dir, time_unit="day")
