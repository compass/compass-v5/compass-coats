import sys
import pytest

import loaf

if loaf.with_petsc:
    import petsc4py

    petsc4py.init(sys.argv)
    from loaf.petsc_solver import PetscSolver
from loaf.eigen_solver import EigenSolver

from numpy.linalg import norm
from globalgo.core import Timeloop, Timestepper, Newton
from globalgo.timestep import FixedStep

from compass_coats.newton_convergence import Coats_norm
from icmesh import (
    regular_mesh,
    top_boundary_faces,
    bottom_boundary_faces,
)
from compass_coats.models import Coats
from compass_coats.schemes import TPFA
from compass_utils.units import bar, degC
from physics.physical_state import States_with_contexts


Nx = 1
Ny = 1
Nz = 20
Lx = 1.0
Ly = 1.0
Lz = 20.0

# init the model with the chosen physics,
# there is no fracture, optional to precise it
model = Coats("diphasic", with_fractures=False)
scheme_def = TPFA()
mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))

data = model.new_data(mesh.dimension)

data.matrix.petrophysics.porosity[...] = 0.15
data.matrix.petrophysics.permeability[...] = 1e-12
data.matrix.petrophysics.thermal_conductivity[...] = 3.0

# init and BC values
top_ctx, Xtop = model.utils.equilibriate_state(
    context="diphasic",
    p=11 * bar,
    T=35 * degC,
    Sg=0.1,
)
bot_ctx, Xbottom = model.utils.equilibriate_state(
    context="liquid",
    p=10 * bar,
    T=15 * degC,
    Cal=1e-4,
)

# init all states with bottom values
data.initial_states[...] = Xbottom
data.initial_contexts[...] = bot_ctx
# init Dirichlet values (top and bottom faces)
top_faces = top_boundary_faces(mesh)
data.boundary_conditions.Dirichlet_states[top_faces] = Xtop
data.boundary_conditions.Dirichlet_contexts[top_faces] = top_ctx
bottom_faces = bottom_boundary_faces(mesh)
data.boundary_conditions.Dirichlet_states[bottom_faces] = Xbottom
data.boundary_conditions.Dirichlet_contexts[bottom_faces] = bot_ctx

# build the linearized problem
linearized_problem = model.set_up(scheme_def, mesh, data)

scheme = linearized_problem.scheme
sites = scheme.sites
n_unknowns, block_size = (
    len(sites),
    model.physics.nb_dof,
)
size = n_unknowns * block_size

linearized_problem.__residual_norm__ = Coats_norm()
assembler = linearized_problem.__assembler__
dt = 1000
# contains states and contexts
solution = States_with_contexts(
    # states
    linearized_problem.sites_data.initial_states[sites].field,
    # contexts
    linearized_problem.sites_data.initial_contexts[sites].field,
)


def run_one_timestep(lsolver):
    newton = Newton(
        tolerance=1e-5,
        maxiter=8,
        assembler=assembler,
        linear_solver=lsolver,
        residual_norm=linearized_problem.__residual_norm__,
    )
    timeloop = Timeloop(Timestepper(newton), FixedStep(dt))
    timeloop.run(solution, dt)


def test_eigen_solve():
    run_one_timestep(EigenSolver())


@pytest.mark.skipif(sys.platform == "win32", reason="does not run on windows")
def test_solver_equivalence():

    solver = EigenSolver()
    assembler.set_up(solution, dt=dt, time=0.0)
    RHS, Jac = assembler.evaluate(solution)
    solver.set_up(Jac)
    eigen_x, _ = solver.solve(RHS)

    solver = PetscSolver((size, size, block_size))
    assembler.set_up(solution, dt=dt, time=0.0)
    RHS, Jac = assembler.evaluate(solution)
    solver.set_up(Jac)
    petsc_x, _ = solver.solve(RHS)

    diff = norm(petsc_x.as_array() - eigen_x.as_array())
    assert diff < 1.0e-6


if __name__ == "__main__":
    test_eigen_solve()
    if loaf.with_petsc:
        test_solver_equivalence()
