import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import regular_mesh, top_boundary
from compass_utils.units import bar, degC, year, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data


@pytest.mark.slow
def test_molar_heat_source(nitermax=None):
    # init the mesh
    Nx = Ny = Nz = 15
    L = Lx = Ly = Lz = 100.0  # cell lengths (in m)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # init the scheme
    scheme_def = TPFA()  # or VAG()
    # init the model with the chosen physics, optional precision: no fracture
    model = Coats("diphasic", with_fractures=False)

    # init petrophysics and rocktypes
    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.2
    data.matrix.petrophysics.permeability[...] = 1e-10  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 2.0  # W/m/K
    data.matrix.rocktypes[...] = 0  # cells values

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xinit
    data.boundary_conditions.Dirichlet_contexts[top] = init_ctx

    # set heat source, will be multiplied by the cell measure
    xyz = Data(mesh.cell_isobarycenters)
    source_cells = (xyz[0] < L / 3) & (xyz[1] < L / 2) & (xyz[2] < L / 4)
    vol_sources = data.matrix.volumetric_sources.dtype()
    vol_sources.molar[...] = 0.0
    vol_sources.molar[model.components["air"]] = 1e-7  # mol/m^3/s
    vol_sources.energy = 2.0  # W/m^3 = J/m^3/s
    data.matrix.volumetric_sources[source_cells] = vol_sources

    # # to modify the tabular after its initialization:
    # request = data.matrix.volumetric_sources[source_cells]
    # request.modify()
    # data.matrix.volumetric_sources[...] = request

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
    )
    # # if necessary, adapt the Newton or timestepper coefficients
    # time_loop.loop.timestepper.step_solver.maxiter = 15
    time_loop.loop.timestep_manager.decrease_factor = 0.2

    final_time = 20 * day
    solution, tick = time_loop.run(
        initial_step=1 * day,
        final_time=final_time,
        nitermax=nitermax,
        nb_output=3,
    )
    # postprocess to prepare for visualization
    postprocess(time_unit="day")


def test_molar_heat_source_fast():
    test_molar_heat_source(1)
