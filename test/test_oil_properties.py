import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.models import Coats
from compass_coats.schemes import TPFA
from icmesh import (
    regular_mesh,
    top_boundary_faces,
    bottom_boundary_faces,
)
from compass_utils.units import bar, degC, day
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from physicalprop.set_densities import (
    set_components_molar_mass,
    set_molar_density_functions,
)
from physicalprop.set_rock_heat_capacity import set_rock_volumetric_heat_capacity
from physicalprop.set_viscosity import set_viscosity_functions
from physicalprop.utils import constant_fluid_physical_property


@pytest.mark.slow
def test_oil_prop():

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0

    model = Coats(
        "diphasic",
        # change name of components (only the name, no modif of physical prop!)
        components=["water", "alkane"],
        # change name of phases (only the name, no modif of physical prop!)
        phases=["oil", "liquid"],
        # change name of contexts
        contexts=["oil", "diphasic", "liquid"],
        with_fractures=False,
    )
    # modifies the rock heat capacity
    set_rock_volumetric_heat_capacity(model.properties.rock, 2.0e6)

    # molar mass C_{n}H_{2n+2} = n * M_C + (2n+2) * M_H
    Malkane = lambda n: (n * 12 + 2 * n + 2) * 1e-3  # g/mol -> kg/mol
    molar_masses = [0] * model.nb_components
    Ma = molar_masses[model.components["alkane"]] = Malkane(1)
    Mw = molar_masses[model.components["water"]] = 0.018016
    # http://www.atomer.fr/1/1_alcanes-lineaires.html
    rho_water = 1000.0  # water density approximation at 1 bar, 20°C
    mol_dens = [0] * model.nb_phases
    mol_dens[model.phases["oil"]] = constant_fluid_physical_property(3.0 / Ma)
    mol_dens[model.phases["liquid"]] = constant_fluid_physical_property(rho_water / Mw)
    # set molar density only, volumetric mass density will be set with
    # modification of components molar mass
    set_molar_density_functions(
        model.properties.fluid,
        mol_dens,
        _update_volumetric_mass_density_functions=False,
    )
    set_components_molar_mass(model.properties.fluid, molar_masses)
    # https://fr.wikipedia.org/wiki/Ordres_de_grandeur_de_viscosit%C3%A9
    viscosities = [0] * model.nb_phases
    viscosities[model.phases["oil"]] = constant_fluid_physical_property(1e-5)
    viscosities[model.phases["liquid"]] = constant_fluid_physical_property(1e-3)
    set_viscosity_functions(model.properties.fluid, viscosities)

    # # overrides viscosity (was constant, depends now on the temperature)
    # import prop_library.viscosities as lib  # default library of properties
    # viscosities = [0] * model.nb_phases
    # viscosities[model.phases["oil"]] = lib.gas_water2ph_viscosities
    # viscosities[model.phases["liquid"]] = lib.liquid_water2ph_viscosities
    # set_viscosity_functions(model.properties.fluid, viscosities)

    print("-----------------------------------------------------------------")
    print("Solve coats - energy with Xcoats structure over a 3D regular grid")
    scheme_def = TPFA()
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))

    data = model.new_data(mesh.dimension)

    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0

    # init and BC values
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    bot_ctx, Xbottom = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with bottom values
    data.initial_states[...] = Xbottom
    data.initial_contexts[...] = bot_ctx
    # init Dirichlet values (top and bottom faces)
    top_faces = top_boundary_faces(mesh)
    data.boundary_conditions.Dirichlet_states[top_faces] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top_faces] = top_ctx
    bottom_faces = bottom_boundary_faces(mesh)
    data.boundary_conditions.Dirichlet_states[bottom_faces] = Xbottom
    data.boundary_conditions.Dirichlet_contexts[bottom_faces] = bot_ctx

    dt = 1000
    final_time = 10 * day
    snapshot_period = final_time / 10

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
    )

    time_loop.run(
        initial_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
    )
    postprocess()

    print("end of test")


if __name__ == "__main__":
    test_oil_prop()
