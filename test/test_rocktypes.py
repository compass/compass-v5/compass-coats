import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from icmesh import regular_mesh, top_boundary
from compass_utils.units import bar, degC
from icus.fig import Data, Zone


def setup_simu():
    # init the mesh and the geometry utilities
    # if you modify Nx, Ny or Nz, change test result
    Nx = 1
    Ny = 5
    Nz = 64  # number of cells
    Lx = 1.0
    Ly = 3.0
    Lz = 320.0  # cell lengths (in m)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # choose a scheme
    scheme_def = VAG()
    # init the model with the chosen physics with optional precision: no fracture
    model = Coats("diphasic", with_fractures=False)

    # init petrophysics
    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0  # W/m/K

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh obj at the top, no distinction between VAG and TPFA
    # to optimize, you can use top_boundary_faces for TPFA
    # and top_boundary_nodes for VAG
    top = top_boundary(mesh)
    data.boundary_conditions.Dirichlet_states[top] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top] = top_ctx

    return scheme_def, mesh, model, data


def test_uniform_rocktypes():

    scheme_def, mesh, model, data = setup_simu()
    # init rocktypes to test the distribution over the sites
    data.matrix.rocktypes[...] = 0
    z_cell = Zone(mesh.cells)
    data.matrix.rocktypes[z_cell] = 1  # modify all cells value

    linearized_problem = model.set_up(scheme_def, mesh, data)
    # assert nodes have been updated (all sites rkt=1)
    scheme = linearized_problem.scheme
    assert len(linearized_problem.sites_data.rocktypes[scheme.sites] == 1) == len(
        scheme.sites
    )


def test_depth_rocktypes():

    scheme_def, mesh, model, data = setup_simu()
    # init rocktypes to test the distribution over the sites
    data.matrix.rocktypes[...] = 0
    z_cell = Data(mesh.cell_isobarycenters)[:, -1]
    top_cells = z_cell >= 100
    bott_cells = z_cell < 100
    data.matrix.rocktypes[top_cells] = 1  # modify some cells values
    data.matrix.rocktypes[bott_cells] = 2  # modify others cells values

    linearized_problem = model.set_up(scheme_def, mesh, data)
    scheme = linearized_problem.scheme
    bott_cells_sites = Zone(scheme.sites.iset.from_location(bott_cells.iset))
    rkt = linearized_problem.sites_data.rocktypes
    # assert nodes have been updated (all sites rkt=1 or 2)
    assert rkt[scheme.sites] in [1, 2]
    assert len(rkt[bott_cells_sites] == 2) == len(bott_cells_sites)
    nb_sites_1_max = len(top_cells) + (len(top_cells) / 5 + 1) * 2 * 6
    nb_sites_1_min = nb_sites_1_max - 2 * 6
    assert len(rkt[scheme.sites] == 1) >= nb_sites_1_min
    assert len(rkt[scheme.sites] == 1) <= nb_sites_1_max


def test_depth_perm_rkt():

    scheme_def, mesh, model, data = setup_simu()
    # init rocktypes to test the distribution over the sites
    data.matrix.rocktypes[...] = 0
    z_cell = Data(mesh.cell_isobarycenters)[:, -1]
    top_cells = z_cell >= 100
    bott_cells = z_cell < 100
    data.matrix.rocktypes[top_cells] = 1  # modify some cells values
    data.matrix.rocktypes[bott_cells] = 2  # modify others cells values
    data.matrix.petrophysics.permeability[top_cells] = 1e-10  # m^2

    linearized_problem = model.set_up(scheme_def, mesh, data)
    scheme = linearized_problem.scheme
    # assert nodes have been updated (all sites rkt=1 or 2)
    rkt = linearized_problem.sites_data.rocktypes
    assert rkt[scheme.sites] in [1, 2]
    # perm of rkt 1 is higher thus all nodes touching top_cells must be 1
    nb_nodes_1 = (len(top_cells) / 5 + 1) * 2 * 6
    nb_sites_1 = len(top_cells) + nb_nodes_1
    assert len(rkt[scheme.sites] == 1) == nb_sites_1


def test_depth_perm_rkt_inverse():

    scheme_def, mesh, model, data = setup_simu()
    # init rocktypes to test the distribution over the sites
    data.matrix.rocktypes[...] = 0
    z_cell = Data(mesh.cell_isobarycenters)[:, -1]
    top_cells = z_cell >= 100
    bott_cells = z_cell < 100
    data.matrix.rocktypes[top_cells] = 1  # modify some cells values
    data.matrix.rocktypes[bott_cells] = 2  # modify others cells values
    data.matrix.petrophysics.permeability[top_cells] = 1e-15  # m^2

    linearized_problem = model.set_up(scheme_def, mesh, data)
    scheme = linearized_problem.scheme
    # assert nodes have been updated (all sites rkt=1 or 2)
    rkt = linearized_problem.sites_data.rocktypes
    assert rkt[scheme.sites] in [1, 2]
    # perm of rkt 1 is smaller thus all nodes touching top_cells must be 2
    nb_nodes_1 = len(top_cells) / 5 * 2 * 6
    nb_sites_1 = len(top_cells) + nb_nodes_1
    assert len(rkt[scheme.sites] == 1) == nb_sites_1


def test_tpfa_rkt():
    _, mesh, model, data = setup_simu()
    # top is Dirichlet
    scheme_def = TPFA()
    data.rocktypes[...] = 0
    cell = Zone(mesh.cells)
    data.matrix.rocktypes[cell] = 1  # modify all cells value

    linearized_problem = model.set_up(scheme_def, mesh, data)
    scheme = linearized_problem.scheme
    # assert faces have been updated (all sites rkt=1)
    assert len(linearized_problem.sites_data.rocktypes[scheme.sites] == 1) == len(
        scheme.sites
    )


def test_tpfa_rkt():
    _, mesh, model, data = setup_simu()
    # top is Dirichlet
    scheme_def = TPFA()
    data.matrix.rocktypes[...] = 0
    z_cell = Data(mesh.cell_isobarycenters)[:, -1]
    top_cells = z_cell >= 150
    bott_cells = z_cell < 150
    data.matrix.rocktypes[top_cells] = 1  # modify some cells values
    data.matrix.rocktypes[bott_cells] = 2  # modify others cells values

    linearized_problem = model.set_up(scheme_def, mesh, data)
    scheme = linearized_problem.scheme
    rkt = linearized_problem.sites_data.rocktypes
    assert rkt[scheme.sites] in [1, 2]
    assert len(rkt[scheme.sites] == 1) == len(top_cells) + 5
    assert rkt[scheme.sites] in [1, 2]
    dir_sites = Zone(scheme.sites.iset.from_location(scheme.dirichlet_mesh_zone.iset))
    assert len(rkt[dir_sites] == 1) == len(dir_sites)
