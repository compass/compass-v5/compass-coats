import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np

from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from compass_utils.filenames import output_directory
from icmesh import (
    regular_mesh,
    xmin_boundary,
    xmax_boundary,
)
from compass_utils.units import bar, degC, day, year
from compass_coats.postprocess import postprocess
from compass_coats.standard_time_loop import Standard_time_loop
from icus.fig import Data, Zone
from icus import field, tensor, join


def space_dependant_mu(mesh):
    cells = Zone(mesh.cells)
    x_cell = Data(mesh.cell_isobarycenters)[:, 0]
    Lx = mesh.extent[0] - mesh.origin[0]

    theta = x_cell / Lx * 4 * np.pi
    cells_thermal_cond = np.empty([len(cells), mesh.dimension, mesh.dimension])
    k = 0
    for cos, sin in zip(
        np.abs(np.cos(theta)).field.as_array(), np.abs(np.sin(theta)).field.as_array()
    ):
        cells_thermal_cond[k] = np.diag((5 * cos, 1, 5 * sin))
        k += 1

    mu = Data.from_zone(cells, tensor((mesh.dimension, mesh.dimension)))
    mu[...] = cells_thermal_cond

    return mu


def test_thermal_anistropy_fast():
    test_thermal_anistropy(nitermax=1)


@pytest.mark.slow
def test_thermal_anistropy(nitermax=None):
    # init the mesh and the geometry utilities
    Nx = 30
    Ny = 2
    Nz = 20  # number of cells
    Lx = 300.0
    Ly = 10.0
    Lz = 200.0  # cell lengths (in m)
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    # set the scheme definition
    scheme_def = TPFA()  # or VAG()
    # init the model with the chosen physics, optional precision: no fracture
    model = Coats("diphasic", with_fractures=False)
    model.gravity = np.zeros(mesh.dimension)

    # init petrophysics and rocktypes
    data = model.new_data(mesh.dimension)
    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12  # m^2
    data.matrix.rocktypes[...] = 0  # cells values

    # init cell thermal cond depending on the cell position
    # data.matrix.petrophysics.thermal_conductivity[...] = np.diag((5, 1, 0.1))  # W/m/K
    data.matrix.petrophysics.thermal_conductivity[...] = space_dependant_mu(mesh)

    # initial state and BC
    init_ctx, Xinit = model.utils.equilibriate_state(
        context="liquid",
        p=20 * bar,
        T=15 * degC,
    )
    BC_ctx, BC_X = model.utils.equilibriate_state(
        context="liquid",
        p=20 * bar,
        T=35 * degC,
    )
    # init all states with initial values
    data.initial_states[...] = Xinit
    data.initial_contexts[...] = init_ctx

    # get all mesh objects at the left and right,
    # no distinction between VAG and TPFA
    # to optimize, you can use *_boundary_faces for TPFA
    # and *_boundary_nodes for VAG
    xmin = xmin_boundary(mesh)
    xmax = xmax_boundary(mesh)
    znf = Data(mesh.isobarycenters(join(mesh.faces, mesh.nodes)))[:, -1]
    z_sup = znf > Lz * 3.0 / 4.0
    z_inf = znf < Lz / 4.0
    Dirichlet_zone = (xmin & z_sup) | (xmax & z_inf)
    data.boundary_conditions.Dirichlet_states[Dirichlet_zone] = BC_X
    data.boundary_conditions.Dirichlet_contexts[Dirichlet_zone] = BC_ctx

    visu_dir = output_directory(__file__)
    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        output_dir=visu_dir,
    )
    # # if necessary, adapt the Newton or timestepper coefficients
    # time_loop.loop.timestepper.step_solver.maxiter = 25
    # time_loop.loop.timestep_manager.increase_factor = 1.5

    solution, tick = time_loop.run(
        initial_step=200 * day,
        final_time=1000 * year,
        output_period=30 * year,
        nitermax=nitermax,
    )
    # postprocess to prepare for visualization
    postprocess(visu_dir, time_unit="day")


if __name__ == "__main__":
    test_thermal_anistropy()
