#%% Documentation : https://compass.gitlab.io/compass-v5/training/
"""
                fixed state
           ┌──────────────────┐
           │                  │
           │                  │
 no flux   │                  │ no flux
           │                  │
           │                  │
           │                  │
           │                  │
air       >│                  │
injection >│                  │
          >│                  │
           │                  │
           │                  │
           │                  │
           │                  │
 no flux   │                  │
           │                  │
           │                  │
           └──────────────────┘
                fixed state
"""
import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

#%% Import the ComPASS library and some utilities
import numpy as np
from physicalprop.set_van_Genuchten_rock_properties import (
    add_van_Genuchten_rock_properties,
)
from compass_coats.models import Coats
from compass_coats.schemes import TPFA, VAG
from compass_coats.postprocess import postprocess, postprocess_residuals
from compass_coats.standard_time_loop import Standard_time_loop
from icmesh import (
    regular_mesh,
    xmin_boundary_faces,
    top_boundary_faces,
    bottom_boundary_faces,
    top_boundary_nodes,
    bottom_boundary_nodes,
)
from icus.fig import Zone, Data, Record
from compass_utils.units import *
from globalgo.core import EventPool


def air_injection(scheme_def, with_vanG_pc=False, nitermax=None):
    #%% Define simulation parameters
    total_depth = 3 * km
    Hinjection = 0.1 * total_depth

    temperature = 300  # K
    pressure_top = 1e6  # Pa

    molar_injection = 1e-1  # mol/m^2/s
    energy_injection = 1e3  # W/m^2 = J/m^2/s
    final_molar_injection = 2 * molar_injection  # mol/m^2/s
    final_energy_injection = 2 * energy_injection  # W/m^2 = J/m^2/s

    Nx = 31
    Nz = Nx * 2
    Ny = 1
    dx = dy = dz = total_depth / Nz

    final_time = 1 * day

    initial_step = 60  # s
    nb_output = 3

    ################################################################
    #%% Set the model, the geometry
    # init the model with the chosen physics,
    # there is no fracture, optional to precise it
    model = Coats("diphasic", with_fractures=False)
    if with_vanG_pc:
        # with van Genuchten, some failures in the time step
        add_van_Genuchten_rock_properties(
            model.properties.rock, 0, 2.0e6, 0.01, 0, 1.54
        )

    mesh = regular_mesh(
        (Nx, Ny, Nz),
        extent=(Nx * dx, Ny * dy, Nz * dz),
        origin=(0.0, 0.0, -total_depth / 2),
    )

    data = model.new_data(mesh.dimension)

    #%% Initialize the petrophysics
    data.matrix.petrophysics.permeability[:] = 1e-12
    data.matrix.petrophysics.porosity[:] = 0.15
    data.matrix.petrophysics.thermal_conductivity[:] = 2

    #%% Get mesh information to define injection, bottom and top domain
    # define a Zone with all cells, faces, nodes
    domain = Zone() | mesh.cells | mesh.faces | mesh.nodes
    # get the coordinates of the domain
    xyz = Data(mesh.isobarycenters(domain.iset))
    # Identify the faces on the xmin boundary
    # which are located in the injection zone
    injection = xmin_boundary_faces(mesh) & (abs(xyz[2]) <= Hinjection / 2)
    # get faces and vertices at an extremity of the mesh
    # faces for TPFA, vertices for VAG
    bottom = bottom_boundary_faces(mesh) | bottom_boundary_nodes(mesh)
    top = top_boundary_faces(mesh) | top_boundary_nodes(mesh)

    #%% Set the initial and BC states (with hydrostatic pressure)
    # Compute hydrostatic pressure
    z_top = mesh.origin[2] + mesh.extent[2]
    rho = 1000
    # gravity is a vector, by default [0, 0, 9.81]
    rho_g = rho * model.gravity[-1]
    hydro_pressures = pressure_top + rho_g * (z_top - xyz[2])

    # Creates equilibrate ref_state then modify the pressures
    ref_context, ref_state = model.utils.equilibriate_state(
        context="liquid",
        T=temperature,
        p=pressure_top,
    )
    state_record = Record(data.initial_states.dtype)
    state_record[:] = ref_state
    state = state_record[domain]
    # no pc, all phase pressures are equal
    state["pressure", model.phases["liquid"]] = hydro_pressures
    state["pressure", model.phases["gas"]] = hydro_pressures
    # Init data with state value and context
    data.initial_contexts[:] = ref_context
    data.initial_states[:] = state
    # Init top Dirichlet BC with the initial state value
    data.boundary_conditions.Dirichlet_contexts[top | bottom] = ref_context
    data.boundary_conditions.Dirichlet_states[:] = state[top | bottom]

    #%% Set air injection, value is in mol/m^2/s,
    # is later multiplied by the face surface and distributed over the sites
    flux = data.boundary_conditions.Neumann_flux.dtype()
    flux.molar[model.components["air"]] = molar_injection
    flux.energy = energy_injection
    data.boundary_conditions.Neumann_flux[injection] = flux

    #%% Create the standard time loop
    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        # verbosity=0,
    )
    scheme = time_loop.linearized_problem.scheme

    #%% Change the time step manager and Newton max nb of iteration
    time_loop.loop.timestep_manager.increase_factor = 5
    time_loop.loop.timestepper.step_solver.maxiter = 12

    #%% Modification of the Neumann flux during the time loop using events
    flux_record_type = data.boundary_conditions.Neumann_flux.dtype
    neumann = (
        time_loop.linearized_problem.sites_data.boundary_conditions.Neumann_flux.field.as_array()
    )
    neumann_flux_in_time = []

    def time_dependent_Neumann_flux(solution, tick):
        # create a Record which will contain the Neumann flux
        faces_flux = Record(flux_record_type)
        time_part = tick.time / final_time  # initial time is 0
        # save the SURFACE neumann flux imposed at each time step
        neumann_flux_in_time.append(
            [
                tick.time,
                time_part * (final_molar_injection - molar_injection),
                time_part * (final_energy_injection - energy_injection),
            ]
        )
        # flux is an Accumulation object defined previously, I overwritte it
        flux.molar[model.components["air"]] = neumann_flux_in_time[-1][1]
        flux.energy = neumann_flux_in_time[-1][2]
        faces_flux[injection] = flux
        neumann[...] = scheme.build_sites_neumann_BC(mesh, faces_flux).field.as_array()

    events = EventPool()
    events.add_recurring_iteration(period=1, action=time_dependent_Neumann_flux)

    #%% Run the standard time loop
    solution, tick = time_loop.run(
        initial_step=initial_step,
        final_time=final_time,
        nb_output=nb_output,
        events=events,
        nitermax=nitermax,
    )

    #%% Some postprocesses, it allows to visualize with Paraview
    postprocess(time_unit="day")


@pytest.mark.slow
def test_air_injection_VAG():
    air_injection(VAG())


@pytest.mark.slow
def test_air_injection_VAG_with_vanG_pc():
    air_injection(TPFA(), with_vanG_pc=True)


@pytest.mark.slow
def test_air_injection_TPFA():
    air_injection(TPFA())


def test_air_injection_TPFA():
    air_injection(TPFA(), nitermax=5)
