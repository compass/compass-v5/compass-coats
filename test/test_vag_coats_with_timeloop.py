import pytest

import numpy as np
from compass_coats.old_assembler import LS_Assembler
from loaf.eigen_solver import EigenSolver
from compass_coats.newton_convergence import Coats_norm
from compass_coats.output_visu import Snapshooter, make_sites_dumper
from compass_coats.postprocess import postprocess
from compass_coats.output_callbacks import CoatsOutputCallbacks
from compass_coats import (
    VAG,
)
from icus import field, join, intersect, labels, tensor
from compass_coats.models import Coats
from physics.physical_state import States_with_contexts
from physicalprop.properties import Properties
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import (
    init_default_rock_physical_properties,
)
from icmesh import (
    regular_mesh,
    top_boundary_nodes,
    bottom_boundary_nodes,
)
from loaf import make_block_linear_system

from globalgo.core import Newton, Timeloop, EventPool, Timestepper, TimeTick
from globalgo.timestep import DynamicStep
from globalgo.callbacks import with_callbacks
from globalgo.core.utils import F

from compass_utils.units import *


def assemble_Darcy(
    mesh,
    the_physics,
    coats_properties,
    Schur_elimination,
):
    top_nodes_among_nodes = top_boundary_nodes(mesh).iset
    bottom_nodes_among_nodes = bottom_boundary_nodes(mesh).iset
    dirichlet_nodes = join(top_nodes_among_nodes, bottom_nodes_among_nodes)
    vag = VAG(mesh, mesh.cells)

    rocktypes = labels(vag.sites)
    rocktypes[...] = 0
    porosity = field(mesh.cells, dtype=np.float64)
    porosity[...] = 0.15
    # compute porosity * cell volume
    cell_measures = mesh.cell_measures.as_array()
    poro_vol = field(mesh.cells, dtype=np.float64)
    poro_vol.as_array()[...] = porosity.as_array() * cell_measures
    # compute (1 - porosity) * cell volume (for Fourier eq)
    poro_1vol = field(mesh.cells, dtype=np.float64)
    poro_1vol.as_array()[...] = (1.0 - porosity.as_array()) * cell_measures

    K = field(mesh.cells, dtype=tensor((3, 3), "d"))
    K[...] = np.diag((1e-12, 1e-12, 1e-12))
    Darcy_trans = vag.build_transmissivities(K)
    # reservoir thermal conductivity
    thermal_conductivity = field(mesh.cells, dtype=tensor((3, 3), "d"))
    thermal_conductivity[...] = np.diag((3.0, 3.0, 3.0))
    Fourier_trans = vag.build_transmissivities(thermal_conductivity)

    # init porous volumes, and first object to distrube the volume to the sites
    # is_volume_nodes: carried by mesh.nodes, true if not Dirichlet
    is_volume_nodes = field(vag.mesh.nodes, dtype=bool)
    is_volume_nodes[...] = True
    if dirichlet_nodes.size() > 0:
        dir_nodes_rebased = dirichlet_nodes.rebase(vag.mesh.nodes)
        for i in dir_nodes_rebased.mapping():
            is_volume_nodes[i] = False

    poro_darcy_vol = vag.distribute_volumes(
        poro_vol,
        rocktypes,
        is_volume_nodes,
        0.075,
    )
    poro_fourier_vol = vag.distribute_volumes(
        poro_vol,
        rocktypes,
        is_volume_nodes,
        0.075,
    )
    rock_fourier_vol = vag.distribute_volumes(
        poro_1vol,
        rocktypes,
        is_volume_nodes,
        0.075,
    )

    coats_states = field(vag.sites, dtype=the_physics.State)
    coats_contexts = field(vag.sites, dtype=the_physics.Context)
    coats_solution = States_with_contexts(coats_states, coats_contexts)

    # BC
    top_nodes = intersect(top_nodes_among_nodes, vag.sites.location, vag.sites.location)
    top_ctx, Xtop = coats_properties.build_state(
        the_physics.Context.diphasic,
        p=1.1e6,
        T=310,
        Sg=0.1,
    )
    bot_ctx, Xbottom = coats_properties.build_state(
        the_physics.Context.liquid,
        p=1.0e6,
        T=300,
        Cal=1.0e6 / 7.14e9 / 2.0,
    )

    # init states
    coats_solution.states[...] = Xbottom
    coats_solution.contexts[...] = bot_ctx
    # modify BC states
    for i in top_nodes.mapping():
        coats_solution.states[i] = Xtop
        coats_solution.contexts[i] = top_ctx
    if Schur_elimination:
        Jac, RHS = make_block_linear_system(vag.sites, the_physics.nb_dof)
    else:
        Jac, RHS = make_block_linear_system(vag.sites, the_physics.nb_natural_variables)

    assembler = LS_Assembler(
        the_physics,
        mesh,
        vag,
        coats_properties,
        rocktypes,
        poro_darcy_vol,
        poro_fourier_vol,
        rock_fourier_vol,
        Darcy_trans,
        Fourier_trans,
        dirichlet_nodes,
        # source_term,
        Jac,
        RHS,
        Schur_elimination=Schur_elimination,
        gravity=np.array([0.0, 0.0, 0.0]),
    )
    assembler.residual_norm = Coats_norm()
    return assembler, coats_solution, vag.sites


def test_coats_energy_fast():
    test_coats_energy(nitermax=5)


@pytest.mark.slow
def test_coats_energy(nitermax=None):

    Nx = 2
    Ny = 3
    Nz = 5
    Lz = 1000
    Lx = 1.0 / 2.0 * Nx / Nz * Lz  # grid size gz = 2*gx
    Ly = 1.0 / 4.0 * Ny / Nz * Lz  # grid size gz = 4*gy

    final_time = 1.0e4 * day

    start_dt = 1e3

    snapshot_period = final_time / 10

    Schur_elimination = True

    if nitermax is not None:
        final_time = TimeTick(final_time, nitermax)

    # load the physics
    model = Coats("diphasic")
    diphasic_physics = model.physics
    # load the default physical properties of the diphasic physics
    fluid_properties = init_default_fluid_physical_properties(diphasic_physics)
    rock_properties = init_default_rock_physical_properties(diphasic_physics)
    coats_prop = Properties(fluid_properties, rock_properties)

    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    #
    cb_log = CoatsOutputCallbacks()
    #
    assembler, initial_solution, sites = assemble_Darcy(
        mesh, diphasic_physics, coats_prop, Schur_elimination
    )
    with_callbacks(cb_log.timeloop_run)("run")(
        Timeloop(
            F(
                with_callbacks(cb_log.timestepper_make_step)("make_step"),
                with_callbacks(cb_log.timestepper_try_step)("try_step"),
            )(
                Timestepper(
                    with_callbacks(cb_log.newton_solve)("solve")(
                        Newton(
                            tolerance=1e-8,
                            maxiter=10,
                            assembler=F(
                                with_callbacks(cb_log.assembler_evaluate)("evaluate"),
                                with_callbacks(cb_log.assembler_set_up)("set_up"),
                                with_callbacks(cb_log.assembler_update_solution)(
                                    "update_solution"
                                ),
                            )(assembler),
                            residual_norm=F(
                                with_callbacks(cb_log.residual_norm_norm)("norm"),
                                with_callbacks(cb_log.residual_norm_set_up)("set_up"),
                            )(assembler.residual_norm),
                            linear_solver=F(
                                with_callbacks(cb_log.linear_solver_solve)("solve"),
                                with_callbacks(cb_log.linear_solver_set_up)("set_up"),
                            )(EigenSolver()),
                        ),
                    ),
                ),
            ),
            DynamicStep(start_dt),
        ),
    ).run(
        solution=initial_solution,
        stop=final_time,
        events=EventPool().add_recurring_time(
            period=snapshot_period,
            action=Snapshooter(make_sites_dumper(mesh, sites, model)),
        ),
    )
    #
    postprocess()


if __name__ == "__main__":
    test_coats_energy()
