import platform
import pytest

pytestmark = pytest.mark.skipif(
    platform.system() == "Windows", reason="does not run on windows"
)

import numpy as np
from compass_coats.models import Coats
from compass_coats.schemes import TPFA
from icmesh import (
    regular_mesh,
    top_boundary_faces,
    bottom_boundary_faces,
)
from compass_utils.units import bar, degC, year
from compass_coats.postprocess import postprocess, postprocess_residuals
from compass_coats.standard_time_loop import Standard_time_loop


@pytest.mark.slow
def test_van_genuchten_coats_energy():

    Nx = 1
    Ny = 1
    Nz = 20
    Lx = 1.0
    Ly = 1.0
    Lz = 20.0

    print("---------------------------------------------------------")
    print("       Solve coats - energy over a 3D regular grid       ")
    print("               With Van Genuchten kr and Pc              ")
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    scheme_def = TPFA()

    model = Coats("diphasic", with_fractures=False)
    model.gravity = np.array([0.0, 0.0, 0.0])
    # overrides default capillary pressure and relative permeability
    # with Van Genuchten formula
    from physicalprop.set_van_Genuchten_rock_properties import (
        add_van_Genuchten_rock_properties,
    )

    add_van_Genuchten_rock_properties(
        model.properties.rock, rocktype=0, Pr=15.0e6, Slr=0.4, Sgr=0, n=1.49
    )

    data = model.new_data(mesh.dimension)

    data.matrix.petrophysics.porosity[...] = 0.15
    data.matrix.petrophysics.permeability[...] = 1e-12
    data.matrix.petrophysics.thermal_conductivity[...] = 3.0

    # init and BC values
    top_ctx, Xtop = model.utils.equilibriate_state(
        context="diphasic",
        p=11 * bar,
        T=35 * degC,
        Sg=0.1,
        rocktype=0,  # then pressures will be at equilibrium (pc != 0)
    )
    bot_ctx, Xbottom = model.utils.equilibriate_state(
        context="liquid",
        p=10 * bar,
        T=15 * degC,
        Cal=1e-4,
    )

    # init all states with bottom values
    data.initial_states[...] = Xbottom
    data.initial_contexts[...] = bot_ctx
    # init Dirichlet values (top and bottom faces)
    top_faces = top_boundary_faces(mesh)
    data.boundary_conditions.Dirichlet_states[top_faces] = Xtop
    data.boundary_conditions.Dirichlet_contexts[top_faces] = top_ctx
    bottom_faces = bottom_boundary_faces(mesh)
    data.boundary_conditions.Dirichlet_states[bottom_faces] = Xbottom
    data.boundary_conditions.Dirichlet_contexts[bottom_faces] = bot_ctx

    dt = 1000
    final_time = 1 * year
    snapshot_period = final_time / 10

    time_loop = Standard_time_loop(
        mesh=mesh,
        model=model,
        scheme=scheme_def,
        data=data,
        newton_res_output=True,
    )
    # # change Newton max number of iteration and tolerance
    # time_loop.loop.timestepper.step_solver.maxiter = 25
    # time_loop.loop.timestepper.step_solver.tolerance = 1e-7

    time_loop.run(
        initial_step=dt,
        final_time=final_time,
        output_period=snapshot_period,
    )

    postprocess()
    postprocess_residuals()

    print("end of test")


if __name__ == "__main__":
    test_van_genuchten_coats_energy()
